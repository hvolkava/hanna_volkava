package by.training.volkava.homework21.controller;

import by.training.volkava.homework21.model.CustomUserDetails;
import by.training.volkava.homework21.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class.getName());
    private UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping({"/login"})
    public String getGoodPage(@RequestParam(value = "isAccept", required = false,
            defaultValue = "") String isAccept,
                              HttpServletRequest request,
                              @AuthenticationPrincipal CustomUserDetails userDetails) {
        HttpSession session = request.getSession(true);
        if (isAccept.equals("on")) {
            session.setAttribute("isAccept", true);
            LOG.debug(userDetails.getUser().toString() + " successfully logged in");
        }
        return "redirect:/goods";
    }

    @GetMapping({"/", "/login"})
    public String getLoginPage() {
        return "login";
    }
}
