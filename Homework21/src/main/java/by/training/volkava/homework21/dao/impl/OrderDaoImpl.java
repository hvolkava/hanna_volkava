package by.training.volkava.homework21.dao.impl;

import by.training.volkava.homework21.dao.OrderDao;
import by.training.volkava.homework21.model.Good;
import by.training.volkava.homework21.model.Order;
import by.training.volkava.homework21.model.User;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

@Repository
public class OrderDaoImpl implements OrderDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderDaoImpl.class.getName());
    private BasicDataSource dataSource;
    private static final String INSERT_ORDER_SQL_STATEMENT =
            "INSERT INTO Orders (user_id, total_price) values (?, ?)";

    public OrderDaoImpl(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Order saveOrder(User user, Map<Good, Integer> goodsMap) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(INSERT_ORDER_SQL_STATEMENT,
                     Statement.RETURN_GENERATED_KEYS)) {
            st.setInt(1, user.getId());
            BigDecimal totalSum = goodsMap.entrySet().stream()
                    .map(entry -> entry.getKey().getPrice()
                            .multiply(BigDecimal.valueOf(entry.getValue())))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            st.setBigDecimal(2, totalSum);
            st.executeUpdate();
            ResultSet keys = st.getGeneratedKeys();
            if (keys.next()) {
                int generatedId = keys.getInt(1);
                keys.close();
                return new Order(generatedId, user.getId(), totalSum);
            }
        } catch (SQLException exception) {
            LOG.error("Problem with executing INSERT", exception);
        }
        return null;
    }
}
