package by.training.volkava.homework21.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Arrays;

@Configuration
@ComponentScan({"by.training.volkava.homework21.service", "by.training.volkava.homework21.dao"})
@PropertySource("classpath:db/db.properties")
public class DataServiceConfig {

    @Bean(destroyMethod = "close")
    public BasicDataSource dataSource(@Value("${db.driver}") String driver,
                @Value("${db.url}") String url,
                @Value("${db.user}") String user,
                @Value("${db.password}") String password,
                @Value("${db.create}") String create,
                @Value("${db.fill}") String fill) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        dataSource.setConnectionInitSqls(Arrays.asList(create, fill));
        return dataSource;
    }
}
