package by.training.volkava.homework21.controller;

import by.training.volkava.homework21.model.CustomUserDetails;
import by.training.volkava.homework21.model.Good;
import by.training.volkava.homework21.model.Order;
import by.training.volkava.homework21.model.User;
import by.training.volkava.homework21.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;
import javax.servlet.http.HttpServlet;

@Controller
public class CheckoutController extends HttpServlet {
    private OrderService orderService;

    public CheckoutController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/checkout")
    public String getCheckoutPage(Model model, Authentication authentication) {
        User user = ((CustomUserDetails) authentication.getPrincipal()).getUser();
        Map<Good, Integer> goodsMap = orderService.getOrderByUser(user);
        if (goodsMap.isEmpty()) {
            return "redirect:/goods";
        }
        Order order = orderService.saveOrderInDd(user, goodsMap);
        model.addAttribute("order", order);
        model.addAttribute("goodsMap", goodsMap);
        return "checkout";
    }
}
