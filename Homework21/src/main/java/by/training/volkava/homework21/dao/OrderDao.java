package by.training.volkava.homework21.dao;

import by.training.volkava.homework21.model.Good;
import by.training.volkava.homework21.model.Order;
import by.training.volkava.homework21.model.User;

import java.util.Map;

public interface OrderDao {
    Order saveOrder(User user, Map<Good, Integer> goodsMap);
}
