package by.training.volkava.homework21.dao.impl;

import by.training.volkava.homework21.dao.OrderGoodDao;
import by.training.volkava.homework21.model.Good;
import by.training.volkava.homework21.model.Order;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

@Repository
public class OrderGoodsDaoImpl implements OrderGoodDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderGoodsDaoImpl.class.getName());
    private BasicDataSource dataSource;
    private static final String INSERT_ORDERGOOD_SQL_STATEMENT =
            "INSERT INTO Order_Good (order_id, good_id, count) values (?, ?, ?)";

    public OrderGoodsDaoImpl(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insertOrderGood(Order order, Map<Good, Integer> goodsMap) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(INSERT_ORDERGOOD_SQL_STATEMENT)) {
            for (Map.Entry<Good, Integer> entry : goodsMap.entrySet()) {
                st.setInt(1, order.getId());
                st.setInt(2, entry.getKey().getId());
                st.setInt(3, entry.getValue());
                st.addBatch();
            }
            st.execute();
        } catch (SQLException exception) {
            LOG.error("Problem with executing INSERT", exception);
        }
    }
}
