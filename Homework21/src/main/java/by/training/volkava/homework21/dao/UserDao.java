package by.training.volkava.homework21.dao;

import by.training.volkava.homework21.model.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> getUserByLogin(String login);
}
