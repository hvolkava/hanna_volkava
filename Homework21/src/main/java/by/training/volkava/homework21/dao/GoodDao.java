package by.training.volkava.homework21.dao;

import by.training.volkava.homework21.model.Good;

import java.util.Set;

public interface GoodDao {
    Set<Good> getAllGood();

    Good getGoodById(int id);
}
