package by.training.volkava.homework20.dao;

import by.training.volkava.homework20.model.Good;
import by.training.volkava.homework20.model.Order;
import by.training.volkava.homework20.model.User;

import java.util.Map;

public interface OrderDao {
    Order saveOrder(User user, Map<Good, Integer> goodsMap);
}
