package by.training.volkava.homework20.controller;

import by.training.volkava.homework20.model.User;
import by.training.volkava.homework20.service.GoodService;
import by.training.volkava.homework20.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServlet;

@Controller
@SessionAttributes("user")
public class GoodsController extends HttpServlet {
    private OrderService orderService;
    private GoodService goodService;

    public GoodsController(OrderService orderService, GoodService goodService) {
        this.orderService = orderService;
        this.goodService = goodService;
    }

    @PostMapping("/goods")
    public String getGoodsPageWhenAddGood(Model model, @RequestParam(value = "goods",
            required = false) String ...goodIds) {
        User user = (User) model.asMap().get("user");
        orderService.addGoods(user, goodIds);
        model.addAttribute("order", orderService.getOrderByUser(user));
        model.addAttribute("goods", goodService.getAllGoods());
        return "goods";
    }

    @GetMapping("/goods")
    public String getGoodsPage(Model model) {
        User user = (User) model.asMap().get("user");
        model.addAttribute("order", orderService.getOrderByUser(user));
        model.addAttribute("goods", goodService.getAllGoods());
        return "goods";
    }


}
