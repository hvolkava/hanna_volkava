package by.training.volkava.homework20.dao;

import by.training.volkava.homework20.model.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> getUserByLogin(String login);
}
