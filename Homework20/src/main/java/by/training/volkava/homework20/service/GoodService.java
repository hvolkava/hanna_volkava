package by.training.volkava.homework20.service;

import by.training.volkava.homework20.dao.GoodDao;
import by.training.volkava.homework20.model.Good;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Service
public class GoodService {
    private GoodDao goodDao;

    public GoodService(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    public Set<Good> getAllGoods() {
        return goodDao.getAllGood();
    }

    /**
     * Method converts the id array into set items.
     *
     * @param itemIds array of id
     * @return set items
     */
    public Map<Good, Integer> convertStringIdsToObjects(String... itemIds) {
        Map<Good, Integer> itemsMap = new HashMap<>();
        if (Objects.isNull(itemIds)) {
            return itemsMap;
        }
        for (String id : itemIds) {
            Good good = getGoodById(Integer.parseInt(id));
            itemsMap.merge(good, 1, Integer::sum);
        }
        return itemsMap;
    }

    /**
     * Get item by id.
     *
     * @param id id of item
     * @return item
     */
    private Good getGoodById(int id) {
        return goodDao.getGoodById(id);
    }
}
