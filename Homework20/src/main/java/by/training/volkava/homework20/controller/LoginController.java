package by.training.volkava.homework20.controller;

import by.training.volkava.homework20.model.User;
import by.training.volkava.homework20.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class.getName());
    private UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping({"/login"})
    public String getGoodPage(@RequestParam("username") String username,
                              @RequestParam(value = "isAccept", required = false,
                                      defaultValue = "") String isAccept,
                              HttpServletRequest request) {
        Optional<User> user = userService.getUserByLogin(username);
        HttpSession session = request.getSession(true);
        if (isAccept.equals("on")) {
            session.setAttribute("isAccept", true);
            session.setAttribute("user", user.get());
            LOG.debug(user.get().toString() + " successfully logged in");
            return "redirect:/goods";
        } else {
            return "acceptError";
        }
    }

    @GetMapping({"/", "/login"})
    public String getLoginPage() {
        return "login";
    }
}
