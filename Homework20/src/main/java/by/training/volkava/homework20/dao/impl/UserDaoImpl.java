package by.training.volkava.homework20.dao.impl;

import by.training.volkava.homework20.dao.UserDao;
import by.training.volkava.homework20.model.User;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class.getName());
    private BasicDataSource dataSource;

    private static final String SELECT_BY_LOGIN_SQL_STATEMENT =
            "SELECT * FROM User WHERE login = ?";

    public UserDaoImpl(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Method for find user in DB by his login.
     *
     * @param login login
     * @return user
     */
    public Optional<User> getUserByLogin(String login) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement(SELECT_BY_LOGIN_SQL_STATEMENT)) {
            st.setString(1, login);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("id"),
                        rs.getString("login"), rs.getString("password"));
                rs.close();
                return Optional.of(user);
            }
        } catch (SQLException exception) {
            LOG.error("Problem with executing SELECT", exception);
        }
        return Optional.empty();
    }
}
