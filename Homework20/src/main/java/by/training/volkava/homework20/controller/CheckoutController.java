package by.training.volkava.homework20.controller;

import by.training.volkava.homework20.model.Good;
import by.training.volkava.homework20.model.Order;
import by.training.volkava.homework20.model.User;
import by.training.volkava.homework20.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.Map;
import javax.servlet.http.HttpServlet;

@Controller
@SessionAttributes("user")
public class CheckoutController extends HttpServlet {
    private OrderService orderService;

    public CheckoutController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/checkout")
    public String getCheckoutPage(Model model) {
        User user = (User) model.asMap().get("user");
        Map<Good, Integer> goodsMap = orderService.getOrderByUser(user);
        if (goodsMap.isEmpty()) {
            return "redirect:/goods";
        }
        Order order = orderService.saveOrderInDd(user, goodsMap);
        model.addAttribute("order", order);
        model.addAttribute("goodsMap", goodsMap);
        return "checkout";
    }
}
