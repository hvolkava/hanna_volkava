package by.training.volkava.homework04.task1;

/**
 * Atm class is designed to withdraw the amount from the card andreplenish it.
 *
 * @author Hanna Volkava
 */
public class Atm {
    /**
     * Method of adding amount to card.
     *
     * @param card   card on which we will put money
     * @param amount rechargeable amount
     * @return true if transaction is successful, false otherwise
     */
    boolean replenishAccount(Card card, double amount) {
        try {
            card.replenishAccountBalance(amount);
        } catch (CardException exception) {
            return false;
        }
        return true;
    }

    /**
     * Method withdrawal amount from the account.
     *
     * @param card   card with which we will withdraw
     * @param amount amount that will be removed
     * @return true if transaction is successful, false otherwise
     */

    boolean withdrawalFromAccount(Card card, double amount) {
        try {
            card.withdrawAccountBalance(amount);
        } catch (CardException exception) {
            return false;
        }
        return true;
    }
}
