package by.training.volkava.homework04.task2;

import java.util.Objects;

/**
 * Class SelectionSort implementation selection sorting strategy.
 *
 * @author Hanna Volkava
 */
public class SelectionSort implements Sorter {
    @Override
    public void sort(int[] array) {
        if (Objects.isNull(array)) {
            throw new SorterException("Array must be not null");
        }
        for (int i = 0; i < array.length - 1; i++) {
            int indexMinElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[indexMinElement]) {
                    indexMinElement = j;
                }
            }
            if (indexMinElement != i) {
                swap(array, i, indexMinElement);
            }
        }
    }

    private void swap(int[] array, int i, int j) {
        int temporaryVariable = array[i];
        array[i] = array[j];
        array[j] = temporaryVariable;
    }
}