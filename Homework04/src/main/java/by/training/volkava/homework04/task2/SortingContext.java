package by.training.volkava.homework04.task2;

/**
 * Class SortingContext used chosen sorting strategy.
 *
 * @author Hanna Volkava
 */
public class SortingContext {
    private Sorter sortStrategy;

    public SortingContext(Sorter sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void execute(int[] array) {
        sortStrategy.sort(array);
    }
}
