package by.training.volkava.homework04.task2;

/**
 * Interface Sorter designed to sort an array.
 * Used Strategy pattern.
 *
 * @author Hanna Volkava
 */
public interface Sorter {
    void sort(int[] array);
}
