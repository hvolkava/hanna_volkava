package by.training.volkava.homework04.task1;

/**
 * The CreditCard class is used to store information about the account balance
 * and the owner and performs actions on the  balance.
 *
 * @author Hanna Volkava
 * @see class CardException.
 */
public class CreditCard extends Card {
    public CreditCard(String nameCardHolder) throws CardException {
        super(nameCardHolder);
    }

    public CreditCard(String nameCardHolder, double accountBalance) throws CardException {
        super(nameCardHolder, accountBalance);
    }
}
