package by.training.volkava.homework04.task1;

import java.util.Objects;

/**
 * The card class is used to store information about the account balance
 * and the owner and performs actions on the  balance.
 *
 * @author Hanna Volkava
 * @see class CardException.
 */

public abstract class Card {
    private String nameCardHolder;
    private double accountBalance;

    public Card(String nameCardHolder) throws CardException {
        setNameCardHolder(nameCardHolder);
    }

    public Card(String nameCardHolder, double accountBalance) throws CardException {
        setNameCardHolder(nameCardHolder);
        setAccountBalance(accountBalance);
    }

    /**
     * Method for getting account balance.
     *
     * @return account balance
     */
    public double getAccountBalance() {
        return accountBalance;
    }

    /**
     * Method of adding amount to the account.
     *
     * @param amount rechargeable amount
     * @throws CardException occurs when trying add negative amount
     */
    public void replenishAccountBalance(double amount) throws CardException {
        if (amount > 0) {
            accountBalance += amount;
        } else {
            throw new CardException("Unable to recharge a negative amount");
        }
    }

    /**
     * Method withdrawal amount from the account.
     *
     * @param amount amount we want to withdraw
     * @throws CardException occurs when trying withdraw negative amount
     */
    public void withdrawAccountBalance(double amount) throws CardException {
        if (amount > 0) {
            accountBalance -= amount;
        } else {
            throw new CardException("Cannot withdraw negative amount from account");
        }
    }

    /**
     * Method to convert account balance in another current.
     *
     * @param exchangeRate exchange rate
     * @return account balance in another currency
     * @throws CardException occurs when trying convert with not positive exchange rate
     */
    public double convertToAnotherCurrency(double exchangeRate) throws CardException {
        double accountBalanceInAnotherCurrency;
        if (exchangeRate > 0) {
            accountBalanceInAnotherCurrency = accountBalance * exchangeRate;
        } else {
            throw new CardException("Check exchange rate. It must be is greater than zero");
        }
        return accountBalanceInAnotherCurrency;
    }

    private void setNameCardHolder(String nameCardHolder) throws CardException {
        if (!Objects.isNull(nameCardHolder) && !nameCardHolder.isEmpty()) {
            this.nameCardHolder = nameCardHolder;
        } else {
            throw new CardException("Check card holder name. It should not be empty");
        }

    }

    private void setAccountBalance(double accountBalance) throws CardException {
        if (accountBalance > 0) {
            this.accountBalance = accountBalance;
        } else {
            throw new CardException("Cannot set a negative balance on the card");
        }
    }
}