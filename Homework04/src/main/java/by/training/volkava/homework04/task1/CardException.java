package by.training.volkava.homework04.task1;

/**
 * Class exception for Card class.
 *
 * @author Hanna Volkava
 */
public class CardException extends RuntimeException {
    public CardException(String message) {
        super(message);
    }
}
