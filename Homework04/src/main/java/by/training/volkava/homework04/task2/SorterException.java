package by.training.volkava.homework04.task2;

/**
 * Class exception for Sorter implementations.
 *
 * @author Hanna Volkava
 */
public class SorterException extends RuntimeException {
    public SorterException(String message) {
        super(message);
    }

}
