package by.training.volkava.homework04.task2;

import java.util.Objects;

/**
 * Class BubbleSort implementation bubble sorting strategy.
 *
 * @author Hanna Volkava
 */
public class BubbleSort implements Sorter {

    @Override
    public void sort(int[] array) {
        if (Objects.isNull(array)) {
            throw new SorterException("Array must be not null");
        }
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                }
            }
        }
    }

    private void swap(int[] array, int i, int j) {
        int temporaryVariable = array[i];
        array[i] = array[j];
        array[j] = temporaryVariable;
    }
}
