package by.training.volkava.homework04.task2;

import java.util.Arrays;

/**
 * Class Main is designed to sort an array using
 * Strategy pattern for selecting the sorting method.
 *
 * @author Hanna Volkava
 */
public class Main {
    public static void main(String[] args) {
        final int REQUIRED_NUMBER_OF_PARAMETERS = 1;
        if (args.length != REQUIRED_NUMBER_OF_PARAMETERS) {
            throw new IllegalArgumentException("This program requires 1 parameter "
                    + "in command line.");
        }
        int[] array = {34, 25, 39, 1, 14, 31};
        System.out.println(Arrays.toString(array));
        Sorter sortStrategy = chooseStrategy(args);
        SortingContext context = new SortingContext(sortStrategy);
        context.execute(array);
        System.out.println(Arrays.toString(array));
    }

    /**
     * Method for chosen sorting strategy.
     *
     * @return sorting strategy
     */
    private static Sorter chooseStrategy(String[] args) {
        Sorter sortStrategy;
        byte caseSort = (byte) parseByte(args[0]);
        switch (caseSort) {
            case 1:
                sortStrategy = new BubbleSort();
                System.out.println("choose bubble sort strategy");
                break;
            case 2:
                sortStrategy = new SelectionSort();
                System.out.println("choose selection sort strategy");
                break;
            default:
                throw new IllegalArgumentException("caseSort must be equals 1 or 2");
        }
        return sortStrategy;
    }

    /**
     * Method for parse byte argument.
     *
     * @param arg actual argument
     * @return parsed byte argument
     * @throws IllegalArgumentException occurs when String arg cannot convert to byte
     */
    private static int parseByte(String arg) {
        try {
            return Byte.parseByte(arg);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException("Incorrect argument " + arg, exception);
        }
    }
}
