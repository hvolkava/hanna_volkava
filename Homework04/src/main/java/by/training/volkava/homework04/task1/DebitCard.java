package by.training.volkava.homework04.task1;

/**
 * The DebitCard class is used to store information about the account balance
 * and the owner and performs actions on the  balance.
 *
 * @author Hanna Volkava
 * @see class CardException.
 */
public class DebitCard extends Card {

    public DebitCard(String nameCardHolder) throws CardException {
        super(nameCardHolder);
    }

    public DebitCard(String nameCardHolder, double accountBalance) throws CardException {
        super(nameCardHolder, accountBalance);
    }

    /**
     * Method withdrawal amount from the account.
     *
     * @param amount amount we want to withdraw
     * @throws CardException occurs when trying withdraw more amount than account balance
     */
    public void withdrawAccountBalance(double amount) throws CardException {
        if (getAccountBalance() >= amount) {
            super.withdrawAccountBalance(amount);
        } else {
            throw new CardException("Cannot withdraw amount from debit card account");
        }
    }
}
