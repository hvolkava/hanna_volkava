package by.training.volkava.homework04.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SortingContextTest {
    private int[] actualArray;
    private int[] expectedArray;

    @Before
    public void setUp() {
        actualArray = new int[]{34, 25, 39, 1, 14, 31, 37, 5, 12, 20, 10, 41, 36, 46};
        expectedArray = new int[]{1, 5, 10, 12, 14, 20, 25, 31, 34, 36, 37, 39, 41, 46};
    }

    @Test
    public void testExecute_WithBubbleSort() {
        SortingContext context = new SortingContext(new BubbleSort());
        context.execute(actualArray);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void testExecute_WithSelectionSort() {
        SortingContext context = new SortingContext(new SelectionSort());
        context.execute(actualArray);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }
}