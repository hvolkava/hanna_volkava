package by.training.volkava.homework04.task1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test suite for DebitCard class.
 */
public class DebitCardTest {

    @Test
    public void testWithdrawAccountBalance_WithCorrectAmount() throws CardException {
        Card card = new DebitCard("John Test", 500);
        card.withdrawAccountBalance(500);
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 0;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test(expected = CardException.class)
    public void testWithdrawAccountBalance_WithMoreAmountThanAccountBalance() throws CardException {
        Card card = new DebitCard("John Test");
        card.withdrawAccountBalance(1000);
    }
}