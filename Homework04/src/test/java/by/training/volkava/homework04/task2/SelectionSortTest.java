package by.training.volkava.homework04.task2;

import org.junit.Assert;
import org.junit.Test;

public class SelectionSortTest {

    @Test
    public void testSort() {
        int[] actualArray = {34, 25, 39, 1, 14, 31, 37, 5, 12, 20, 10, 41, 36, 46};
        int[] expectedArray = {1, 5, 10, 12, 14, 20, 25, 31, 34, 36, 37, 39, 41, 46};
        Sorter selectionSort = new SelectionSort();
        selectionSort.sort(actualArray);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }

    @Test(expected = SorterException.class)
    public void testSort_WithNull() {
        int[] actualArray = null;
        Sorter selectionSort = new SelectionSort();
        selectionSort.sort(actualArray);
    }
}