package by.training.volkava.homework04.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test suite for Atm class.
 */
public class AtmTest {
    private Atm atm;

    @Before
    public void setUp() throws Exception {
        atm = new Atm();
    }

    @Test
    public void testReplenishAccount_WithCorrectAmount() throws CardException {
        Card card = new CreditCard("John Test");
        boolean actual = atm.replenishAccount(card, 500);
        Assert.assertTrue(actual);
    }

    @Test
    public void testReplenishAccount_WithNegativeAmount() throws CardException {
        Card card = new CreditCard("John Test");
        boolean actual = atm.replenishAccount(card, -500);
        Assert.assertFalse(actual);
    }

    @Test
    public void testWithdrawalFromAccount_WithCorrectAmount() throws CardException {
        Card card = new CreditCard("John Test", 500);
        boolean actual = atm.withdrawalFromAccount(card, 500);
        Assert.assertTrue(actual);
    }

    @Test
    public void testWithdrawalFromAccount_WithMoreAmountThanAccountBalance() throws CardException {
        Card card = new DebitCard("John Test", 500);
        boolean actual = atm.withdrawalFromAccount(card, 1000);
        Assert.assertFalse(actual);
    }
}