package by.training.volkava.homework04.task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class MainTest {

    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(null);
    }

    @Test
    public void testMain_WithCorrectValuesBubbleSort() {
        String expected = "[34, 25, 39, 1, 14, 31]\r\n" +
                "choose bubble sort strategy\r\n" +
                "[1, 14, 25, 31, 34, 39]\r\n";
        String[] argumentsOfCommandLine = new String[]{"1"};
        Main.main(argumentsOfCommandLine);
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testMain_WithCorrectValuesSelectionSort() {
        String expected = "[34, 25, 39, 1, 14, 31]\r\n" +
                "choose selection sort strategy\r\n" +
                "[1, 14, 25, 31, 34, 39]\r\n";
        String[] argumentsOfCommandLine = new String[]{"2"};
        Main.main(argumentsOfCommandLine);
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_WithWrongCaseSort() {
        String[] argumentsOfCommandLine = new String[]{"3"};
        Main.main(argumentsOfCommandLine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_WithoutRequiredArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[0];
        Main.main(argumentsOfCommandLine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_WrongByteValuesArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[]{"1.2"};
        Main.main(argumentsOfCommandLine);
    }
}
