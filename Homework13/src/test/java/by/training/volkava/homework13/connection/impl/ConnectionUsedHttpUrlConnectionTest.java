package by.training.volkava.homework13.connection.impl;

import by.training.volkava.homework13.model.Article;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class ConnectionUsedHttpUrlConnectionTest {

    @Test
    public void testDoPostRequest_InCorrectCase() {
        Article article = new Article(101, 5, "title", "body");
        String stringUrl = "https://jsonplaceholder.typicode.com/posts";
        ConnectionUsedHttpUrlConnection connection = new ConnectionUsedHttpUrlConnection(stringUrl);
        Optional<Article> createdArticle = connection.doPostRequest(article);
        Assert.assertEquals(article, createdArticle.get());
    }

    @Test
    public void testDoPostRequest_WithNullArticle() {
        String stringUrl = "https://jsonplaceholder.typicode.com/posts";
        ConnectionUsedHttpUrlConnection connection = new ConnectionUsedHttpUrlConnection(stringUrl);
        Optional<Article> createdArticle = connection.doPostRequest(null);
        Assert.assertFalse(createdArticle.isPresent());
    }

    @Test
    public void testDoPostRequest_WithFailedCodeResponse() {
        Article article = new Article(101, 5, "title", "body");
        String stringUrl = "https://jsonplaceholder.typicode.com/";
        ConnectionUsedHttpUrlConnection connection = new ConnectionUsedHttpUrlConnection(stringUrl);
        Optional<Article> createdArticle = connection.doPostRequest(article);
        Assert.assertFalse(createdArticle.isPresent());
    }

    @Test
    public void testDoGetRequest_InCorrectCase() {
        String stringUrl = "https://jsonplaceholder.typicode.com/posts";
        ConnectionUsedHttpUrlConnection connection = new ConnectionUsedHttpUrlConnection(stringUrl);
        Article article = new Article(10, 1, "optio molestias id quia eum",
                "quo et expedita modi cum officia vel magni\n" +
                        "doloribus qui repudiandae\n" +
                        "vero nisi sit\n" +
                        "quos veniam quod sed accusamus veritatis error");
        Optional<Article> createdArticle = connection.doGetRequest(10);
        Assert.assertEquals(article, createdArticle.get());

    }

    @Test
    public void testDoGetRequest_WithFailedCodeResponse() {
        String stringUrl = "https://jsonplaceholder.typicode.com/";
        ConnectionUsedHttpUrlConnection connection = new ConnectionUsedHttpUrlConnection(stringUrl);
        Optional<Article> createdArticle = connection.doGetRequest(10);
        Assert.assertFalse(createdArticle.isPresent());
    }
}
