package by.training.volkava.homework13.utils;

import java.io.IOException;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import by.training.volkava.homework13.model.Article;

public class ArticleParserTest {
    @Test
    public void testConvertJsonToObject_WithCorrectData() throws IOException {
        String stringJson = "{\"userId\":5,\"id\":10,\"title\":\"sunt\",\"body\":\"quia et suscipit\"}";
        Article expected = new Article(10, 5, "sunt", "quia et suscipit");
        Article actual = ArticleParser.convertJsonToObject(stringJson);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertJsonToObject_WithNull() throws IOException {
        ArticleParser.convertJsonToObject(null);
    }

    @Test
    public void testConvertObjectToJson_WithCorrectData() throws IOException, JSONException {
        String expected = "{\n"
                + "\"userId\": 5,\n"
                + "\"id\": 10,\n"
                + "\"title\": \"title\",\n"
                + "\"body\": \"body\"\n"
                + "}";
        String actual = ArticleParser.convertObjectToJson(new Article(10, 5, "title", "body"));
        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertObjectToJson_WithNullObject() throws IOException {
        ArticleParser.convertObjectToJson(null);
    }

    @Test
    public void testConvertObjectToJson_WithEmptyObject() throws IOException, JSONException {
        String actual = ArticleParser.convertObjectToJson(new Article());
        JSONAssert.assertEquals("{\"id\":0,\"userId\":0,\"title\":null,\"body\":null}", actual, true);
    }
}
