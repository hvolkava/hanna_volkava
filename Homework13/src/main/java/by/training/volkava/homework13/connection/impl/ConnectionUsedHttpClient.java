package by.training.volkava.homework13.connection.impl;

import by.training.volkava.homework13.connection.HttpConnection;
import by.training.volkava.homework13.model.Article;
import by.training.volkava.homework13.utils.ArticleParser;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Optional;

public class ConnectionUsedHttpClient implements HttpConnection {
    private String urlService;
    private CloseableHttpClient httpClient = HttpClientBuilder.create().build();

    /**
     * Constructor creates instance of connection bound in a specific url service.
     *
     * @param urlService url service
     */
    public ConnectionUsedHttpClient(String urlService) {
        this.urlService = urlService;
    }

    /**
     * The method sends a GET request to REST service with the passed Id.
     *
     * @param id id article
     * @return response message if response code is 200 and null otherwise
     */
    public Optional<Article> doGetRequest(int id) {
        try {
            HttpUriRequest getRequest = new HttpGet(urlService + '/' + id);
            getRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
            CloseableHttpResponse httpResponse = httpClient.execute(getRequest);
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (responseCode == HttpStatus.SC_OK) {
                return Optional.ofNullable(getArticleFromResponse(httpResponse));
            } else {
                throw new IOException("Get Request failed with error code " + responseCode);
            }
        } catch (IOException exception) {
            return Optional.empty();
        }
    }

    /**
     * The method sends a POST request to url service with the passed article.
     *
     * @param article article
     * @return response message if response code is 201 and null otherwise
     */
    public Optional<Article> doPostRequest(Article article) {
        HttpPost postRequest = new HttpPost(urlService);
        try {
            String stringJson = ArticleParser.convertObjectToJson(article);
            postRequest.setEntity(new StringEntity(stringJson));
            postRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            CloseableHttpResponse httpResponse = httpClient.execute(postRequest);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_CREATED) {
                return Optional.ofNullable(getArticleFromResponse(httpResponse));
            } else {
                throw new IOException("Post Request failed with error code " + statusCode);
            }
        } catch (IOException | IllegalArgumentException exception) {
            exception.printStackTrace();
            return Optional.empty();
        }
    }

    private Article getArticleFromResponse(CloseableHttpResponse httpResponse) throws IOException {
        String responseJson = EntityUtils.toString(httpResponse.getEntity());
        return ArticleParser.convertJsonToObject(responseJson);
    }
}
