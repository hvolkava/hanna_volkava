package by.training.volkava.homework13.service;

import by.training.volkava.homework13.connection.HttpConnection;
import by.training.volkava.homework13.model.Article;

import java.util.Objects;
import java.util.Optional;

/**
 * The ArticleService allows you to work with articles.
 *
 * @author Hanna Volkava
 */
public class ArticleService {

    private HttpConnection connection;

    public ArticleService(HttpConnection connection) {
        this.connection = connection;
    }

    /**
     * Save information about the new article.
     *
     * @param article article what we want to save
     * @return saved article in wrapper Optional
     */
    public boolean createPost(Article article) {
        if (Objects.isNull(article)) {
            return false;
        }
        Optional<Article> createdArticle = connection.doPostRequest(article);
        createdArticle.ifPresent(art -> System.out.println((new StringBuilder(art.toString()))
                .insert(art.toString().indexOf(":"),
                        " has been created")));
        return createdArticle.isPresent();
    }

    /**
     * Get article by Id.
     *
     * @param id Id article
     * @return article with needed Id in wrapper Optional
     */
    public boolean getArticleById(int id) {
        Optional<Article> article = connection.doGetRequest(id);
        article.ifPresent(System.out::println);
        return article.isPresent();
    }
}
