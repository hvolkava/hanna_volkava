package by.training.volkava.homework13.connection;

import by.training.volkava.homework13.model.Article;

import java.util.Optional;

public interface HttpConnection {
    Optional<Article> doPostRequest(Article article);

    Optional<Article> doGetRequest(int id);
}
