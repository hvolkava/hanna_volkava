package by.training.volkava.homework13.connection.impl;

import by.training.volkava.homework13.connection.HttpConnection;
import by.training.volkava.homework13.model.Article;
import by.training.volkava.homework13.utils.ArticleParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

public class ConnectionUsedHttpUrlConnection implements HttpConnection {

    private String urlService;
    private HttpURLConnection connection;

    /**
     * Constructor creates instance of connection bound in a specific url service.
     *
     * @param urlService url service
     */
    public ConnectionUsedHttpUrlConnection(String urlService) {
        this.urlService = urlService;
    }

    /**
     * The method sends a POST request to url service with the passed article.
     *
     * @param article article
     * @return response message if response code is 201 and null otherwise
     */
    public Optional<Article> doPostRequest(Article article) {
        try {
            URL requestUrl = new URL(urlService);
            connection = (HttpURLConnection) requestUrl.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            try (OutputStream os = connection.getOutputStream()) {
                String stringJson = ArticleParser.convertObjectToJson(article);
                os.write(stringJson.getBytes());
            }
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_CREATED) {
                return Optional.ofNullable(getArticleFromResponse(connection));
            } else {
                throw new IOException("Post Request failed with error code " + responseCode);
            }
        } catch (IOException | IllegalArgumentException exception) {
            exception.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * The method sends a GET request to REST service with the passed Id.
     *
     * @param id id article
     * @return response message if response code is 200 and null otherwise
     */
    public Optional<Article> doGetRequest(int id) {
        URL urlForGetRequest;
        try {
            urlForGetRequest = new URL(urlService + '/' + id);
            connection = (HttpURLConnection) urlForGetRequest.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                return Optional.ofNullable(getArticleFromResponse(connection));
            } else {
                throw new IOException("Get Request failed with error code " + responseCode);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return Optional.empty();
    }

    private Article getArticleFromResponse(HttpURLConnection connection) throws IOException {
        String stringJson = convertStreamToString(connection.getInputStream());
        return ArticleParser.convertJsonToObject(stringJson);
    }

    private String convertStreamToString(InputStream is) {
        StringBuilder result = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line).append("\n");
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return result.toString();
    }
}
