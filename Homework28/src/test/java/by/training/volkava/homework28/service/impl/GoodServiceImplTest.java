package by.training.volkava.homework28.service.impl;

import by.training.volkava.homework28.dao.GoodDao;
import by.training.volkava.homework28.model.Good;
import by.training.volkava.homework28.service.GoodService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GoodServiceImplTest {
    @Mock
    private GoodDao goodDao;

    @InjectMocks
    private GoodServiceImpl service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllGoods_WhenReturnGoods() {
        List<Good> listGoods = new ArrayList<Good>();
        listGoods.add(new Good());
        Mockito.when(goodDao.getAllGood()).thenReturn(listGoods);
        Assert.assertEquals(1, service.getAllGoods().size());
    }

    @Test
    public void testGetGoodById_WhenGoodExist() {
        Good good = new Good();
        Mockito.when(goodDao.getGoodById(5L)).thenReturn(Optional.of(good));
        Assert.assertEquals(service.getGoodById(5L).get(), good);
    }

    @Test
    public void testGetGoodById_WhenGoodDoesNotExist() {
        Mockito.when(goodDao.getGoodById(5000L)).thenReturn(Optional.empty());
        Assert.assertFalse(service.getGoodById(5000L).isPresent());
    }
}