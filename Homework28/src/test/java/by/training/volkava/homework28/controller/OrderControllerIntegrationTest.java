package by.training.volkava.homework28.controller;

import by.training.volkava.homework28.config.*;
import by.training.volkava.homework28.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {MvcConfig.class, DataServiceConfig.class, HibernateConfig.class, SpringMailConfig.class, SecurityConfig.class})
public class OrderControllerIntegrationTest {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity());
        this.mockMvc = builder.build();
    }

    @Test
    public void testCreateOrder_WithValidUser() throws Exception {
        ResultMatcher created = MockMvcResultMatchers.status().isCreated();
        User user = new User();
        user.setLogin("John");
        user.setPassword("123456");
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.post("/orders")
                        .with(userHttpBasic(user))
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{\"idsGoodsCount\":{\"1\":1}}");
        this.mockMvc.perform(builder)
                .andExpect(created);

    }

    private RequestPostProcessor userHttpBasic(User user) {
        return SecurityMockMvcRequestPostProcessors
                .httpBasic(user.getLogin(), user.getPassword());
    }
}