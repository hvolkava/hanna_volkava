package by.training.volkava.homework28.service.impl;

import by.training.volkava.homework28.dao.GoodDao;
import by.training.volkava.homework28.dao.UserDao;
import by.training.volkava.homework28.model.Good;
import by.training.volkava.homework28.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class UserServiceImplTest {
    @Mock
    private UserDao userdDao;

    @InjectMocks
    private UserServiceImpl service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUserById_WhenUserExist() {
        User user = new User();
        Mockito.when(userdDao.getUserById(5L)).thenReturn(Optional.of(user));
        Assert.assertEquals(service.getUserById(5L).get(), user);
    }

    @Test
    public void testGetUserById_WhenUserDoesNotExist() {
        Mockito.when(userdDao.getUserById(5000L)).thenReturn(Optional.empty());
        Assert.assertFalse(service.getUserById(5000L).isPresent());
    }

    @Test
    public void testGetUserByLogin_WhenUserExist() {
        User user = new User();
        Mockito.when(userdDao.getUserByLogin("John")).thenReturn(Optional.of(user));
        Assert.assertEquals(service.getUserByLogin("John").get(), user);
    }

    @Test
    public void testGetUserByLogin_WhenUserDoesNotExist() {
        Mockito.when(userdDao.getUserByLogin("nothing")).thenReturn(Optional.empty());
        Assert.assertFalse(service.getUserByLogin("getUserByLogin").isPresent());
    }
}