var orderData;

var order = $('#order');

var renderOrder = function(jsonResponse) {
    order.empty();
    order.append('<table id="tableOrder"></table>');
    var table = jsonResponse['goodsMap'];
    let index = 1;
    $.each(table, function (key, good) {
        let tr = $('<tr/>');
        tr.append("<td> " + index + ")</td><td>" + good.title + "</td><td>" + good.price + "</td><td>" + jsonResponse['idsGoodsCount'][key] + "</td></tr>'");
        $('#tableOrder').append(tr);
        index++;
    });
};

var jsoncart;
if (localStorage["orderlocation"]) {
    orderlocation = localStorage.getItem("orderlocation");
    $.ajax({
        type: "GET",
        url: orderlocation,
        success: function (msg) {
            localStorage.setItem("username", msg["username"]);
            jsoncart = msg;
            renderOrder(jsoncart);
            document.getElementById("username").innerText = jsoncart["username"];
            document.getElementById("totalPrice").innerText = jsoncart["price"];
        }
    });


}




