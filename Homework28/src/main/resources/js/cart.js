const order = $('#order');
const renderOrder = function(jsonResponse) {
    order.empty();order.append('<p>You already chosen:</p>');
    order.append('<table id="tableOrder"></table>');
    let table = jsonResponse['goodsMap'];
    let index = 1;
    $.each(table, function (key, good) {
        let tr = $('<tr/>');
        tr.append("<td>" + index + ")</td><td>" + good.title + "</td><td>" + good.price + "</td><td>" + jsonResponse['idsGoodsCount'][key] + "</td></tr>'");
        $('#tableOrder').append(tr);
        index++;
    });
};
var jsoncart;
if (localStorage["jsoncart"]) {
    jsoncart = JSON.parse(localStorage.getItem("jsoncart"));
    document.getElementById("username").innerText = localStorage.getItem("username");
    renderOrder(jsoncart);
} else {
    $.ajax({
        type: "GET",
        url: "./cart/current",
        dataType: "json",
        success: function (msg) {
            document.getElementById("username").innerText = msg["username"];
            localStorage.setItem("username", msg["username"]);
            jsoncart = msg;
        }
    });
}
console.log(jsoncart);
let dropdown = $('#goods'); dropdown.empty();
$.getJSON('./goods', function (data) {
    $.each(data, function (key, entry) {
        dropdown.append($('<option></option>').attr('value', entry.id).text(entry.title + "  " + entry.price + "$"));
    });
});

$("#addgood").click(function (e) {
    let id = $("#goods").val();
    const jsontosend = JSON.stringify(jsoncart, null, 2);
    $.ajax(
        {
            url: "./cart/goods/" + id,
            type: "POST",
            contentType : 'application/json',
            data: jsontosend,
            success: function (msg) {
                jsoncart = msg;
                localStorage.setItem("jsoncart", JSON.stringify(jsoncart));
                console.log("jsoncart", jsoncart);
                renderOrder(jsoncart);
                $('#error').html("");
            }
        });
    e.preventDefault(); //STOP default action
});

$("#checkout").click(function (e) {
    const jsontosend = JSON.stringify(jsoncart, null, 2);
    $.ajax(
        {
            url: "./orders",
            type: "POST",
            contentType : 'application/json',
            data: jsontosend,
            success: function (msg, status, xhr) {
                console.log(xhr.getResponseHeader('Location'));
                localStorage.setItem("orderlocation", xhr.getResponseHeader('Location'));
                localStorage.setItem("orderData", JSON.stringify(msg));
                localStorage.removeItem("jsoncart");
                window.location.href = "order";
            },
            error: function (jqXHR, textStatus, error) {
                // Handle error here
                const jsonerror = JSON.parse(jqXHR.responseText);
                $('#error').html(jsonerror['errors']);
            }
        });
    e.preventDefault(); //STOP default action
});
