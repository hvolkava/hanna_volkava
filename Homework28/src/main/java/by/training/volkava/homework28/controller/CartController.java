package by.training.volkava.homework28.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import by.training.volkava.homework28.converter.OrderConverter;
import by.training.volkava.homework28.dto.OrderDto;
import by.training.volkava.homework28.model.Order;
import by.training.volkava.homework28.service.GoodService;
import by.training.volkava.homework28.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/cart")
public class CartController {

    private OrderService orderService;
    private OrderConverter orderConverter;

    public CartController(OrderService orderService,
                          OrderConverter orderConverter) {
        this.orderService = orderService;
        this.orderConverter = orderConverter;
    }

    @GetMapping(value = "/current", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDto> getCustomCart(Principal principal) {
        OrderDto orderDto = new OrderDto();
        orderDto.setUsername(principal.getName());
        orderDto.setIdsGoodsCount(new HashMap<>());
        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }

    @PostMapping(value = "/goods/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDto> addGoodInCart(@RequestBody OrderDto orderDto,
                                                   @PathVariable Long id) {
        Order order = orderConverter.getOrderFromOrderDto(orderDto);
        orderService.addGood(order, id.toString());
        OrderDto result = orderConverter.getOrderDtoFromOrder(order);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping
    public ModelAndView getPageCart() {
        return new ModelAndView();
    }

}
