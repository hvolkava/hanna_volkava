package by.training.volkava.homework28.service;

import by.training.volkava.homework28.model.Good;
import by.training.volkava.homework28.model.Order;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import javax.mail.MessagingException;

public interface OrderService {

    Order addGood(Order order, String goodId);

    void saveOrderInDd(Order order) throws MessagingException;

    Optional<Order> getOrderById(Long id);

    BigDecimal calculateTotalSum(Map<Good, Integer> goodsMap);

    void updateOrderInDd(Order order) throws MessagingException;
}
