package by.training.volkava.homework28.service.impl;

import by.training.volkava.homework28.model.Order;
import by.training.volkava.homework28.service.EmailService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {
    private static final String EMAIL_SIMPLE_TEMPLATE_NAME = "html/order";

    private ApplicationContext applicationContext;
    private JavaMailSender mailSender;
    private TemplateEngine htmlTemplateEngine;

    public EmailServiceImpl(ApplicationContext applicationContext, JavaMailSender mailSender,
                            @Qualifier("emailTemplateEngine") TemplateEngine htmlTemplateEngine) {
        this.applicationContext = applicationContext;
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
    }

    @Async
    public void sendSimpleMail(Order order) throws MessagingException {
        final Context ctx = new Context();
        ctx.setVariable("order", order);
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
        message.setSubject("Create order");
        message.setTo(order.getUser().getEmail());
        final String htmlContent = this.htmlTemplateEngine.process(EMAIL_SIMPLE_TEMPLATE_NAME, ctx);
        message.setText(htmlContent, true );
        this.mailSender.send(mimeMessage);
    }
}
