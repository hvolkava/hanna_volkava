package by.training.volkava.homework28.service;

import by.training.volkava.homework28.model.Order;

import javax.mail.MessagingException;

public interface EmailService {
    void sendSimpleMail(Order order) throws MessagingException;
}
