package by.training.volkava.homework28.dao.impl;

import by.training.volkava.homework28.dao.UserDao;
import by.training.volkava.homework28.model.User;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class.getName());
    private static final String SELECT_BY_LOGIN_HQL = "from User WHERE login = :login";
    private static final String SELECT_BY_ID_HQL = "from User WHERE  id = :id";
    private SessionFactory sessionFactory;

    public UserDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Optional<User> getUserByLogin(String login) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_BY_LOGIN_HQL)
                .setParameter("login", login)
                .uniqueResultOptional();
    }

    public Optional<User> getUserById(Long id) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_BY_ID_HQL)
                .setParameter("id", id)
                .uniqueResultOptional();
    }
}
