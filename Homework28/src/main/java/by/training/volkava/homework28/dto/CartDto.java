package by.training.volkava.homework28.dto;

import by.training.volkava.homework28.model.Good;

import java.util.Map;

public class CartDto {
    private String username;

    private Map<String, Integer> idsGoodsCount;

    private Map<String, Good> goodsMap;

    public Map<String, Integer> getIdsGoodsCount() {
        return idsGoodsCount;
    }

    public CartDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setIdsGoodsCount(Map<String, Integer> idsGoodsCount) {
        this.idsGoodsCount = idsGoodsCount;
    }

    public Map<String, Good> getGoodsMap() {
        return goodsMap;
    }

    public void setGoodsMap(Map<String, Good> goodsMap) {
        this.goodsMap = goodsMap;
    }
}
