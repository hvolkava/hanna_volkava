package by.training.volkava.homework28.service.impl;

import by.training.volkava.homework28.dao.GoodDao;
import by.training.volkava.homework28.model.Good;
import by.training.volkava.homework28.service.GoodService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class GoodServiceImpl implements GoodService {
    private static final Logger LOG = LoggerFactory.getLogger(GoodServiceImpl.class.getName());
    private GoodDao goodDao;

    public GoodServiceImpl(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<Good> getAllGoods() {
        return goodDao.getAllGood();
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Map<Good, Integer> getGoodsByIds(String... itemIds) {
        Map<Good, Integer> itemsMap = new HashMap<>();
        if (Objects.isNull(itemIds)) {
            return itemsMap;
        }
        for (String id: itemIds) {
            Optional<Good> optionalGoodgood = getGoodById(Long.parseLong(id));
            if (optionalGoodgood.isPresent()) {
                itemsMap.merge(optionalGoodgood.get(), 1, Integer::sum);
            } else {
                LOG.warn("Trying get GOOD by ID equals {} and not found", id);
            }
        }
        return itemsMap;
    }

    @Transactional(readOnly = true)
    public Optional<Good> getGoodById(Long id) {
        return goodDao.getGoodById(id);
    }

    @Transactional
    public void saveGood(Good good) {
        goodDao.saveGood(good);
    }
}
