package by.training.volkava.homework28.dto;

import by.training.volkava.homework28.model.Good;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import javax.validation.constraints.NotEmpty;

public class OrderDto implements Serializable {
    private String username;

    private BigDecimal price;

    @NotEmpty(message = "You can't create order without goods")
    private Map<String, Integer> idsGoodsCount;

    private Map<String, Good> goodsMap;

    public Map<String, Integer> getIdsGoodsCount() {
        return idsGoodsCount;
    }

    public OrderDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setIdsGoodsCount(Map<String, Integer> idsGoodsCount) {
        this.idsGoodsCount = idsGoodsCount;
    }

    public Map<String, Good> getGoodsMap() {
        return goodsMap;
    }

    public void setGoodsMap(Map<String, Good> goodsMap) {
        this.goodsMap = goodsMap;
    }
}
