package by.training.volkava.homework28.dao;

import by.training.volkava.homework28.model.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Long id);
}
