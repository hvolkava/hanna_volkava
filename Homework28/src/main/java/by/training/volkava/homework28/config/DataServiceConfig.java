package by.training.volkava.homework28.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"by.training.volkava.homework28.service", "by.training.volkava.homework28.dao",
        "by.training.volkava.homework28.converter"})
public class DataServiceConfig {

}
