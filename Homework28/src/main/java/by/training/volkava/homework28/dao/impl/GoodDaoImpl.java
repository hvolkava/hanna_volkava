package by.training.volkava.homework28.dao.impl;

import by.training.volkava.homework28.dao.GoodDao;
import by.training.volkava.homework28.model.Good;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class GoodDaoImpl implements GoodDao {
    private static final Logger LOG = LoggerFactory.getLogger(GoodDaoImpl.class.getName());
    private static final String SELECT_ALL_GOODS_HQL = "from Good";
    private SessionFactory sessionFactory;

    public GoodDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Good> getAllGood() {
        List<Good> result = new ArrayList<>();
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_GOODS_HQL).list();
    }

    @Override
    public Optional<Good> getGoodById(Long id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Good.class, id));
    }

    @Override
    public void saveGood(Good good) {
        sessionFactory.getCurrentSession().save(good);
    }
}
