package by.training.volkava.homework28.service;

import by.training.volkava.homework28.model.Good;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface GoodService {

    List<Good> getAllGoods();

    Map<Good, Integer> getGoodsByIds(String... itemIds);

    Optional<Good> getGoodById(Long id);

    void saveGood(Good good);
}
