package by.training.volkava.homework28.service.impl;

import by.training.volkava.homework28.dao.OrderDao;
import by.training.volkava.homework28.model.Good;
import by.training.volkava.homework28.model.Order;
import by.training.volkava.homework28.service.EmailService;
import by.training.volkava.homework28.service.GoodService;
import by.training.volkava.homework28.service.OrderService;
import by.training.volkava.homework28.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import javax.mail.MessagingException;

/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class.getName());
    private GoodService goodService;
    private UserService userService;
    private OrderDao orderDao;
    private EmailService emailService;

    public OrderServiceImpl(GoodService goodService, UserService userService,
                            OrderDao orderDao, EmailService emailService) {
        this.goodService = goodService;
        this.userService = userService;
        this.orderDao = orderDao;
        this.emailService = emailService;
    }

    public Order addGood(Order order, String goodId) {
        Map<Good, Integer> goodsMap = goodService.getGoodsByIds(goodId);
        Map<Good, Integer> goodMapsInOrder = order.getGoodsMap();
        goodsMap.forEach((key, value) -> goodMapsInOrder.merge(key, value, Integer::sum));
        order.setGoodsMap(goodMapsInOrder);
        order.setTotalPrice(calculateTotalSum(goodMapsInOrder));
        return order;
    }

    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    public void saveOrderInDd(Order order) throws MessagingException {
        orderDao.saveOrder(order);
        LOG.debug("{} saved {}",order.getUser(), order);
        emailService.sendSimpleMail(order);
    }

    public BigDecimal calculateTotalSum(Map<Good, Integer> goodsMap) {
        return goodsMap.entrySet().stream()
                .map(entry -> entry.getKey().getPrice()
                        .multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateOrderInDd(Order order) {
        orderDao.updateOrder(order);
        LOG.debug("{} update {}", order.getUser(), order);
    }

    @Transactional(readOnly = true)
    public Optional<Order> getOrderById(Long id) {
        return orderDao.getOrder(id);
    }
}
