package by.training.volkava.homework28.service;

import by.training.volkava.homework28.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Long id);
}
