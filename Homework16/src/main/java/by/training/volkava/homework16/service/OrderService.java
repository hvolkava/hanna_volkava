package by.training.volkava.homework16.service;

import by.training.volkava.homework16.model.Item;
import by.training.volkava.homework16.util.OrdersSingleton;

import java.util.Map;


/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
public class OrderService {
    /**
     * Add item in cart.
     *
     * @param username username
     * @param itemId   id item
     */
    public void addItem(String username, String... itemId) {
        ItemsService itemsService = new ItemsService();
        Map<Item, Integer> itemsMap = itemsService.convertStringIdsToObjects(itemId);
        Map<String, Map<Item, Integer>> ordersMap = OrdersSingleton.getInstance();
        if (OrdersSingleton.getInstance().containsKey(username)) {
            Map<Item, Integer> mapItems = ordersMap.get(username);
            itemsMap.forEach((key, value) -> mapItems.merge(key, value, Integer::sum));
        } else {
            ordersMap.put(username, itemsMap);
        }
    }
}
