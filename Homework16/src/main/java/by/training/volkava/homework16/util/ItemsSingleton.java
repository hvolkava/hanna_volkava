package by.training.volkava.homework16.util;

import by.training.volkava.homework16.model.Item;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * The class is an emitation of the connection with the database to get all items for sale.
 *
 * @author Hanna Volkava
 */
public class ItemsSingleton {

    private static Set<Item> setItems;

    private ItemsSingleton() {

    }

    /**
     * Get get all items fo online-shop.
     *
     * @return set items
     */
    public static Set<Item> getInstance() {
        if (Objects.isNull(setItems)) {
            setItems = new HashSet<>();
            setItems.add(new Item("Kindle", 19.99));
            setItems.add(new Item("Tamagochi", 4.99));
            setItems.add(new Item("Phone", 199.99));
        }
        return setItems;
    }
}
