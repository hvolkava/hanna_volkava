package by.training.volkava.homework16.util;

import by.training.volkava.homework16.model.Item;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class OrdersSingleton {
    private static Map<String, Map<Item, Integer>> ordersMap;

    private OrdersSingleton() {

    }

    /**
     * Get get all items fo online-shop.
     *
     * @return set items
     */
    public static Map<String, Map<Item, Integer>> getInstance() {
        if (Objects.isNull(ordersMap)) {
            ordersMap = new HashMap<>();
        }
        return ordersMap;
    }

}
