package by.training.volkava.homework16.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "login", urlPatterns = {"", "/login"})
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String username = request.getParameter("username");
        boolean isAccept = Objects.nonNull(request.getParameter("isAccept"))
                && request.getParameter("isAccept").equals("on");
        request.getSession(true).setAttribute("username", username);
        request.getSession(false).setAttribute("isAccept", isAccept);
        if (isAccept) {
            if (Objects.isNull(username) || username.trim().isEmpty()) {
                response.sendRedirect(request.getContextPath() + "/login");
            } else {
                request.getRequestDispatcher("/items").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("/error").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Login page</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("<body><div>");
            out.println("<h2 style='text-align: center;'>Welcome to Online Shop</h2>");
            out.println("<div>");
            out.println("<form action='login' method='POST'>");
            out.println("<input type='text' id='username' name='username'"
                    + "placeholder='Enter your name' >");
            out.println("<input type='checkbox' name='isAccept' id='isAccept'>"
                    + "<label for='isAccept'>I agree with the terms of service</label>");
            out.println("<input type='submit' value='Enter'>");
            out.println("</form>");
            out.println("</div></div></body>");
            out.println("</html>");
        }
    }
}
