package by.training.volkava.homework16.servlet;

import by.training.volkava.homework16.model.Item;
import by.training.volkava.homework16.util.OrdersSingleton;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "checkout", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String username = (String) request.getSession(false).getAttribute("username");

        if (OrdersSingleton.getInstance().get(username).isEmpty()) {
            response.sendRedirect(request.getContextPath() + "/items");
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Checkout</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("</head><body><div>");
            out.printf("<h2>Dear %s, your order:</h2>", username);
            out.println(getInfoByOrderInHtml(username));
            out.println("</div></body>");
            out.println("</html>");
        }
    }

    private String getInfoByOrderInHtml(String username) {
        StringBuilder sb = new StringBuilder();
        int counter = 1;
        Map<Item, Integer> mapItems = OrdersSingleton.getInstance().get(username);
        for (Map.Entry<Item, Integer> mapEntry : mapItems.entrySet()) {
            sb.append(String.format("<p>%d) %s  %6.2f $   %d</p>",
                    counter++, mapEntry.getKey().getName(),
                    mapEntry.getKey().getPrice(), mapEntry.getValue()));
        }
        double sum = mapItems.entrySet().stream()
                .mapToDouble(entry -> entry.getKey().getPrice() * entry.getValue()).sum();
        sb.append(String.format("<p> Total: $ %6.2f</p>", sum));
        return sb.toString();
    }
}
