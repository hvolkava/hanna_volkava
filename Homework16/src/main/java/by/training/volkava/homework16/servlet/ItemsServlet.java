package by.training.volkava.homework16.servlet;

import by.training.volkava.homework16.model.Item;
import by.training.volkava.homework16.service.OrderService;
import by.training.volkava.homework16.util.ItemsSingleton;
import by.training.volkava.homework16.util.OrdersSingleton;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "items", urlPatterns = {"/items"})
public class ItemsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String username = (String) request.getSession(false).getAttribute("username");
        String[] itemIds = request.getParameterValues("items");
        OrderService orderService = new OrderService();
        orderService.addItem(username, itemIds);

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Choose items</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("</head><body><div>");
            out.printf("<h2>Hello %s!</h2>", username);
            if (!OrdersSingleton.getInstance().get(username).isEmpty()) {
                out.println("<p>You have already chosen:</p>");
                out.println(getInfoByOrderInHtml(username));
            } else {
                out.println("<p>Make your order</p>");
            }
            out.println("<form id='form' action='items' method='POST'>");
            out.println("<select name='items' size='1' required>");
            Set<Item> setItems = ItemsSingleton.getInstance();
            for (Item item : setItems) {
                StringBuilder sb = new StringBuilder("<option value='");
                sb.append(item.getId()).append("'>").append(item.getName()).append("  (")
                        .append(item.getPrice()).append("$)</option>");
                out.println(sb);
            }
            out.println("</select><br>");
            out.println("<input type='submit' value='Add item'/>");
            out.println("<input type='submit' value='Submit' "
                    + "formaction='checkout' formmethod='POST'>");
            out.println("</form>");
            out.println("</div></body>");
            out.println("</html>");
        }
    }

    private String getInfoByOrderInHtml(String username) {
        StringBuilder sb = new StringBuilder();
        int counter = 1;
        Map<Item, Integer> mapItems = OrdersSingleton.getInstance().get(username);
        for (Map.Entry<Item, Integer> mapEntry : mapItems.entrySet()) {
            sb.append(String.format("<p>%d) %s  %6.2f $   %d</p>",
                    counter++, mapEntry.getKey().getName(),
                    mapEntry.getKey().getPrice(), mapEntry.getValue()));
        }
        return sb.toString();
    }
}
