package by.training.volkava.homework16.model;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Item {
    private static AtomicInteger counter =  new AtomicInteger();
    private int id = assignId();
    private String name;
    private double price;

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    private int assignId() {
        return counter.decrementAndGet();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Item)) {
            return false;
        }
        Item item = (Item) object;
        return getId() == item.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
