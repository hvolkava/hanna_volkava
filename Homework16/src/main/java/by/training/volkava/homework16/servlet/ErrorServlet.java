package by.training.volkava.homework16.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ErrorServlet", urlPatterns = {"/error"})
public class ErrorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Error page</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("<body><div>");
            out.println("<h2>Oops!</h2>");
            out.println("<p>You shouldn't be here</p>");
            out.println("<p>Please, agree with the terms of service first.</p>");
            out.println("<p><a href='" + request.getContextPath()
                    + "/login" + "'>Start page</a></p>");
            out.println("</form>");
            out.println("</div></body>");
            out.println("</html>");
        }
    }
}
