package by.training.volkava.homework02.task1;

/**
 * Main application class for Task1 of Homework02
 * Calculates G from incoming data (if possible).
 *
 * @author Hanna Volkava
 */
public class MainForTask1 {

    /**
     * Application start point that takes incoming data, calculates value G
     * and prints value G in the console.
     *
     * @param args required parameters for calculate G.
     * @throws IllegalArgumentException occurs when isn't requires 4 parameters in command line
     */
    public static void main(String[] args) {

        final int REQUIRED_NUMBER_OF_PARAMETERS = 4;

        if (args.length != REQUIRED_NUMBER_OF_PARAMETERS) {
            throw new IllegalArgumentException("Program requires 4 parameters in command line.");
        }

        int valueA = parseInt(args[0]);
        int valueP = parseInt(args[1]);
        double valueM1 = parseDouble(args[2]);
        double valueM2 = parseDouble(args[3]);
        double valueG = FormulaG.calculate(valueA, valueP, valueM1, valueM2);
        System.out.printf("Value G equals %10.5f", valueG);
    }

    /**
     * Method for parse int argument.
     *
     * @param arg actual argument
     * @return parsed int argument
     * @throws IllegalArgumentException occurs when String arg cannot convert to int
     */
    private static int parseInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException("Incorrect argument " + arg, exception);
        }
    }

    /**
     * Method for parse double argument.
     *
     * @param arg actual argument
     * @return parsed double argument
     * @throws IllegalArgumentException occurs when String arg cannot convert to double
     */
    private static double parseDouble(String arg) {
        try {
            return Double.parseDouble(arg);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException("Incorrect argument " + arg, exception);
        }
    }
}