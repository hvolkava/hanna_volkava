package by.training.volkava.homework02.task1;

/**
 * Util class for calculate value G.
 */
public class FormulaG {
    /**
     * Method for calculate G by the formula:
     * G = 4 * PI^2 * (a^3 / (p^2 * (m1 + m2))).
     *
     * @param valueA  int value "a" in the formula
     * @param valueP  int value "p" in the formula
     * @param valueM1 double value "m1" in the formula
     * @param valueM2 double value "m2" in the formula
     * @return valueG - double value "G" in the formula
     * @throws IllegalArgumentException occurs when are happened division by zero
     */
    public static double calculate(int valueA, int valueP, double valueM1, double valueM2) {
        double valueG;
        if (valueP != 0 && (valueM1 + valueM2) != 0) {
            valueG = 4 * Math.pow(Math.PI, 2) * Math.pow(valueA, 3)
                    / (Math.pow(valueP, 2) * (valueM1 + valueM2));
        } else {
            throw new IllegalArgumentException("Program can not calculate value G "
                    + "because with this source data are happened division by zero");
        }
        return valueG;
    }
}
