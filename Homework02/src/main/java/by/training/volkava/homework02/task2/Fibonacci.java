package by.training.volkava.homework02.task2;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Class Fibonacci calculates Fibonacci numbers of n.
 *
 * @author Hanna Volkava
 */
public class Fibonacci implements TaskForSolution {
    private BigInteger[] fibonacci;

    /**
     * Constructor class creates an instance of class fills the array fibonacci.
     * @param number first numbers of Fibonacci what we must find
     * @param loopType what kind loop we must fill fibonacci array
     */
    public Fibonacci(int number, byte loopType) {
        if (number < 0) {
            throw new IllegalArgumentException("For Fibonacci numbers n must be more zero");
        }
        this.fibonacci = new BigInteger[number];
        switch (loopType) {
            case 1:
                this.findWithLoopWhile();
                break;
            case 2:
                this.findWithLoopDoWhile();
                break;
            case 3:
                this.findWithLoopFor();
                break;
            default:
                System.out.println(" You choose invalid type loop and program use default loop");
                this.findWithLoopFor();
        }
    }

    /**
     * Method for find numbers of Fibonacci used loop 'For'.
     */
    @Override
    public void findWithLoopFor() {
        System.out.println(" You use loop 'For' for solution");
        for (int i = 0; i < fibonacci.length; i++) {
            fibonacci[i] = i > 1 ? fibonacci[i - 2].add(fibonacci[i - 1]) : BigInteger.valueOf(i);
        }
    }

    /**
     * Method for find numbers of Fibonacci used loop 'While'.
     */
    @Override
    public void findWithLoopWhile() {
        System.out.println(" You use loop 'While' for solution");
        int i = 0;
        while (i < fibonacci.length) {
            fibonacci[i] = i > 1 ? fibonacci[i - 2].add(fibonacci[i - 1]) : BigInteger.valueOf(i);
            i++;
        }
    }

    /**
     * Method for find numbers of Fibonacci used loop 'Do-While'.
     */
    @Override
    public void findWithLoopDoWhile() {
        System.out.println(" You use loop 'Do-While' for solution");
        int i = 0;
        do {
            fibonacci[i] = i > 1 ? fibonacci[i - 2].add(fibonacci[i - 1]) : BigInteger.valueOf(i);
            i++;
        } while (i < fibonacci.length);
    }

    /**
     * Method for print numbers of Fibonacci in the console .
     */
    @Override
    public void getResult() {
        System.out.println("First " + fibonacci.length + " numbers of Fibonacci "
                            + Arrays.toString(fibonacci));
    }
}
