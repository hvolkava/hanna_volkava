package by.training.volkava.homework02.task2;

/**
 * Interface TaskForSolution for Task2 of Homework02.
 * The necessary methods to implementation our task
 *
 * @author Hanna Volkava
 */
public interface TaskForSolution {
    /**
     * Method for used loop 'For'.
     */
    void findWithLoopFor();

    /**
     * Method for used loop 'While'.
     */
    void findWithLoopWhile();

    /**
     * Method for used loop 'Do-While'.
     */
    void findWithLoopDoWhile();

    /**
     * Method get result.
     */
    void getResult();
}
