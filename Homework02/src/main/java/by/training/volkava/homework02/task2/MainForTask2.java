package by.training.volkava.homework02.task2;

/**
 * Main application class for Task2 of Homework02.
 * It prints Fibonacci numbers or factorial for input number n,
 * calculated by chosen loop and prints result.
 *
 * @author Hanna Volkava
 */
public class MainForTask2 {
    /**
     * Application start point that takes incoming data
     * and prints value G in the console.
     *
     * @param args required parameters for solve our task.
     * @throws IllegalArgumentException occurs when isn't requires 3 parameters in command line
     */
    public static void main(String[] args) {
        final int REQUIRED_NUMBER_OF_PARAMETERS = 3;

        if (args.length != REQUIRED_NUMBER_OF_PARAMETERS) {
            throw new IllegalArgumentException("This program requires 3 parameters "
                                                + "in command line.");
        }
        byte algorithmId = (byte) parseByte(args[0]);
        byte loopType = (byte) parseByte(args[1]);
        int number = parseInt(args[2]);
        TaskForSolution task = createTask(algorithmId, number, loopType);
        task.getResult();
    }

    /**
     * Method for chose what task we want to find.
     *
     * @param algorithmId what task we want to solve
     *                    algorithmId = 1 - find Fibonacci numbers
     *                    algorithmId = 2 - find Factorial
     * @param number for algorithmId
     * @param loopType what kind loop we must use when find result
     * @return task what we want to find
     * @throws IllegalArgumentException occurs when algorithmId not equals 1 or 2
     */
    private static TaskForSolution createTask(byte algorithmId, int number, byte loopType) {
        TaskForSolution task;
        switch (algorithmId) {
            case 1:
                task = new Fibonacci(number, loopType);
                System.out.println(" You want to find first " + number + " Fibonacci numbers");
                break;
            case 2:
                task = new Factorial(number, loopType);
                System.out.println(" You want to find " + number + "!");
                break;
            default:
                throw new IllegalArgumentException("loopType must be equals 1 or 2");
        }
        return task;
    }

    /**
     * Method for parse byte argument.
     *
     * @param arg actual argument
     * @return parsed byte argument
     * @throws IllegalArgumentException occurs when String arg cannot convert to byte
     */
    private static int parseByte(String arg) {
        try {
            return Byte.parseByte(arg);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException("Incorrect argument " + arg, exception);
        }
    }

    /**
     * Method for parse int argument.
     *
     * @param arg actual argument
     * @return parsed int argument
     * @throws IllegalArgumentException occurs when String arg cannot convert to int
     */
    private static int parseInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException("Incorrect argument " + arg, exception);
        }
    }
}
