package by.training.volkava.homework02.task2;

import java.math.BigInteger;

/**
 * Class Factorial calculates factorial of n.
 *
 * @author Hanna Volkava
 */
public class Factorial implements TaskForSolution {
    private int number;
    private BigInteger value;

    /**
     * Constructor class creates an instance of class.
     *
     * @param number   factorial this number what we must find
     * @param loopType what kind loop we must use
     */
    public Factorial(int number, byte loopType) {
        if (number >= 0) {
            this.number = number;
        } else {
            throw new IllegalArgumentException("For factorial n must be more or equals zero");
        }
        switch (loopType) {
            case 1:
                this.findWithLoopWhile();
                break;
            case 2:
                this.findWithLoopDoWhile();
                break;
            case 3:
                this.findWithLoopFor();
                break;
            default:
                System.out.println(" You choose invalid type loop and program use default loop");
                this.findWithLoopFor();
        }
    }

    /**
     * Method for find value factorial n used loop 'For'.
     */
    @Override
    public void findWithLoopFor() {
        System.out.println(" You use loop 'For' for solution");
        value = BigInteger.ONE;
        for (int i = 0; i < number; i++) {
            value = value.multiply(BigInteger.valueOf(i + 1));
        }
    }

    /**
     * Method for find value factorial n used loop 'While'.
     */
    @Override
    public void findWithLoopWhile() {
        System.out.println(" You use loop 'While' for solution");
        value = BigInteger.ONE;
        int i = 0;
        while (i < number) {
            value = value.multiply(BigInteger.valueOf(i + 1));
            i++;
        }
    }

    /**
     * Method for find value factorial n used loop 'Do-While'.
     */
    @Override
    public void findWithLoopDoWhile() {
        System.out.println(" You use loop 'Do-While' for solution");
        value = BigInteger.ONE;
        int i = 0;
        do {
            value = value.multiply(BigInteger.valueOf(i + 1));
            i++;
        } while (i < number);
    }

    /**
     * Method for print value factorial n in the console .
     */
    @Override
    public void getResult() {
        System.out.println("Factorial " + number + " equals " + value);
    }
}
