package by.training.volkava.homework02.task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;

import static org.junit.Assert.assertArrayEquals;

public class FibonacciTest {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test(expected = IllegalArgumentException.class)
    public void testFibonacciCostructor_wrongValueNumber() {
        Fibonacci factorial = new Fibonacci(-1, (byte) 1);
    }
}