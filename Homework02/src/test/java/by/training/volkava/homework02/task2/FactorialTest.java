package by.training.volkava.homework02.task2;

import org.junit.Test;

import static org.junit.Assert.*;

public class FactorialTest {

    @Test(expected = IllegalArgumentException.class)
    public void testFactorialCostructor_wrongValueNumber() {
        Factorial factorial = new Factorial(-1, (byte) 1);
    }
}