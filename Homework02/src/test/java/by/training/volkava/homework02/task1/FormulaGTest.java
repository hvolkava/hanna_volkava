package by.training.volkava.homework02.task1;

import org.junit.Test;

import static org.junit.Assert.*;

public class FormulaGTest {

    @Test
    public void testcalculate_WithCorrectValues() {
        double expected = 27367.59054;
        double precision = 0.00001;
        int valueA = 55;
        int valueP = 2;
        double valueM1 = 10;
        double valueM2 = 50;
        double actual = FormulaG.calculate(valueA, valueP, valueM1, valueM2);
        assertEquals(expected, actual, precision);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testcalculate_DivisionByZero() {
        double g = FormulaG.calculate(1, 0, 8, 1);
    }
}