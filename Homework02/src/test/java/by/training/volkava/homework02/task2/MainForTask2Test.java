package by.training.volkava.homework02.task2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class MainForTask2Test {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(null);
    }

    @Test
    public void testMain_WithCorrectValues() {
        String expected = " You use loop 'Do-While' for solution\r\n" +
                " You want to find first 10 Fibonacci numbers\r\n" +
                "First 10 numbers of Fibonacci [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]\r\n";
        String[] argumentsOfCommandLine = new String[]{"1", "2", "10"};
        MainForTask2.main(argumentsOfCommandLine);
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_withoutRequiredArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[2];
        MainForTask2.main(argumentsOfCommandLine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_wrongIntValuesArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[]{"1", "1", "2.3"};
        MainForTask2.main(argumentsOfCommandLine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_wrongByteValuesArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[]{"1.2", "3", "200"};
        MainForTask2.main(argumentsOfCommandLine);
    }
}