package by.training.volkava.homework02.task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Test main application class for Task1 of Homework02.
 *
 * @author Hanna Volkava
 */
public class MainForTask1Test {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(null);
    }

    @Test
    public void testMain_WithCorrectValues() {
        String expected = "Value G equals   19,73921";
        String[] argumentsOfCommandLine = new String[]{"1", "1", "1", "1"};
        MainForTask1.main(argumentsOfCommandLine);
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_withoutRequiredArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[3];
        MainForTask1.main(argumentsOfCommandLine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_wrongIntValuesArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[4];
        MainForTask1.main(argumentsOfCommandLine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_wrongDoubleValuesArgumentsOfCommandLine() {
        String[] argumentsOfCommandLine = new String[]{"1.2", "3", "t", "5"};
        MainForTask1.main(argumentsOfCommandLine);
    }
}