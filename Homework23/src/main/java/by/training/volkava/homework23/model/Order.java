package by.training.volkava.homework23.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Map;
import java.util.StringJoiner;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Orders")
public class Order {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "Order_Good",
            joinColumns = @JoinColumn(name = "order_id"))
    @MapKeyJoinColumn(name = "good_id")
    @Column(name = "count")
    private Map<Good, Integer> goodsMap;

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Map<Good, Integer> getGoodsMap() {
        return goodsMap;
    }

    public void setGoodsMap(Map<Good, Integer> goodsMap) {
        this.goodsMap = goodsMap;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Order.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("userId=" + user.getId())
                .add("totalPrice=" + totalPrice)
                .add("goodsMap=" + goodsMap)
                .toString();
    }
}
