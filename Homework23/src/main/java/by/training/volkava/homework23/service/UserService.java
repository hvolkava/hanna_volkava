package by.training.volkava.homework23.service;

import by.training.volkava.homework23.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Long id);
}
