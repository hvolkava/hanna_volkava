package by.training.volkava.homework23.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"by.training.volkava.homework23.service", "by.training.volkava.homework23.dao"})
public class DataServiceConfig {

}
