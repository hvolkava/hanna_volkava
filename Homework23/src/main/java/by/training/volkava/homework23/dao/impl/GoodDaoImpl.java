package by.training.volkava.homework23.dao.impl;

import by.training.volkava.homework23.dao.GoodDao;
import by.training.volkava.homework23.model.Good;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class GoodDaoImpl implements GoodDao {
    private static final Logger LOG = LoggerFactory.getLogger(GoodDaoImpl.class.getName());
    private static final String SELECT_ALL_GOODS_HQL = "from Good";
    private static final String SELECT_BY_ID_HQL = "from Good WHERE id = :id";
    private SessionFactory sessionFactory;

    public GoodDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Good> getAllGood() {
        List<Good> result = new ArrayList<>();
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_GOODS_HQL).list();
    }

    @Override
    public Good getGoodById(Long id) {
        return (Good) sessionFactory.getCurrentSession()
                .createQuery(SELECT_BY_ID_HQL)
                .setParameter("id", id)
                .uniqueResult();
    }
}
