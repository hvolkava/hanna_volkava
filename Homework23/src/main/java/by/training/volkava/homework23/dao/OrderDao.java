package by.training.volkava.homework23.dao;

import by.training.volkava.homework23.model.Order;

import java.util.Optional;

public interface OrderDao {
    Order saveOrder(Order order);

    Optional<Order> getOrder(Long id);

    void deleteOrder(Order order);
}
