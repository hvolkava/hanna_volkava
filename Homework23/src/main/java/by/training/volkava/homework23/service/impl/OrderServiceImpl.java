package by.training.volkava.homework23.service.impl;

import by.training.volkava.homework23.dao.OrderDao;
import by.training.volkava.homework23.model.Good;
import by.training.volkava.homework23.model.Order;
import by.training.volkava.homework23.model.User;
import by.training.volkava.homework23.service.GoodService;
import by.training.volkava.homework23.service.OrderService;
import by.training.volkava.homework23.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class.getName());
    private static Map<User, Order> ordersMap = new HashMap<>();
    private GoodService goodService;
    private UserService userService;
    private OrderDao orderDao;

    public OrderServiceImpl(GoodService goodService, UserService userService, OrderDao orderDao) {
        this.goodService = goodService;
        this.userService = userService;
        this.orderDao = orderDao;
    }

    public Order getOrderByUser(User user) {
        if (ordersMap.containsKey(user)) {
            return ordersMap.get(user);
        } else {
            return createNewOrder(user);
        }
    }

    public void addGoods(Order order, User user, String... goodId) {
        Map<Good, Integer> goodsMap = goodService.getGoodsByIds(goodId);
        Map<Good, Integer> goodMapsInOrder = order.getGoodsMap();
        goodsMap.forEach((key, value) -> goodMapsInOrder.merge(key, value, Integer::sum));
        order.setGoodsMap(goodMapsInOrder);
        order.setTotalPrice(calculateTotalSum(goodMapsInOrder));
        ordersMap.put(user, order);
    }

    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    public Order saveOrderInDd(Order order, User user) {
        Order savedOrder = orderDao.saveOrder(order);
        ordersMap.remove(user);
        if (Objects.nonNull(savedOrder)) {
            LOG.debug(user.toString() + " saved in BD order " + savedOrder.toString());
        }
        return savedOrder;
    }

    private BigDecimal calculateTotalSum(Map<Good, Integer> goodsMap) {
        return goodsMap.entrySet().stream()
                .map(entry -> entry.getKey().getPrice()
                        .multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private Order createNewOrder(User user) {
        Order order = new Order();
        order.setUser(user);
        order.setGoodsMap(new HashMap<>());
        return order;
    }

    @Transactional(readOnly = true)
    public Order getOrderById(Long id) {
        Order ord = orderDao.getOrder(id).get();
        Order saveOrderInDd = orderDao.saveOrder(ord);
        orderDao.deleteOrder(saveOrderInDd);
        return ord;
    }
}
