package by.training.volkava.homework23.dao.impl;

import by.training.volkava.homework23.dao.OrderDao;
import by.training.volkava.homework23.model.Order;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class OrderDaoImpl implements OrderDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderDaoImpl.class.getName());
    private SessionFactory sessionFactory;
    private static final String SELECT_BY_ID_HQL = "from Order o left join fetch o.goodsMap "
            + "WHERE o.id = :id";

    public OrderDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Order saveOrder(Order order) {
        sessionFactory.getCurrentSession().save(order);
        return order;
    }

    public Optional<Order> getOrder(Long id) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_BY_ID_HQL)
                .setParameter("id", id)
                .uniqueResultOptional();
    }

    public void deleteOrder(Order order) {
        sessionFactory.getCurrentSession().delete(order);
    }
}
