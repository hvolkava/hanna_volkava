package by.training.volkava.homework23.controller;

import by.training.volkava.homework23.model.CustomUserDetails;
import by.training.volkava.homework23.model.Order;
import by.training.volkava.homework23.model.User;
import by.training.volkava.homework23.service.GoodService;
import by.training.volkava.homework23.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServlet;

@Controller
public class GoodsController extends HttpServlet {
    private OrderService orderService;
    private GoodService goodService;

    public GoodsController(OrderService orderService, GoodService goodService) {
        this.orderService = orderService;
        this.goodService = goodService;
    }

    @PostMapping("/goods")
    public String getGoodsPageWhenAddGood(Model model, Authentication authentication,
                                          @RequestParam(value = "goods",required = false)
                                                  String... goodIds) {
        User user = ((CustomUserDetails) authentication.getPrincipal()).getUser();
        Order order = orderService.getOrderByUser(user);
        orderService.addGoods(order, user, goodIds);
        model.addAttribute("order", order);
        model.addAttribute("goods", goodService.getAllGoods());
        return "goods";
    }

    @GetMapping("/goods")
    public String getGoodsPage(Model model, Authentication authentication) {
        User user = ((CustomUserDetails) authentication
                .getPrincipal())
                .getUser();
        model.addAttribute("order", orderService.getOrderByUser(user));
        model.addAttribute("goods", goodService.getAllGoods());
        return "goods";
    }
}
