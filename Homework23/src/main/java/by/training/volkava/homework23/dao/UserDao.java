package by.training.volkava.homework23.dao;

import by.training.volkava.homework23.model.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Long id);
}
