package by.training.volkava.homework23.service.impl;

import by.training.volkava.homework23.dao.GoodDao;
import by.training.volkava.homework23.model.Good;
import by.training.volkava.homework23.service.GoodService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class GoodServiceImpl implements GoodService {
    private GoodDao goodDao;

    public GoodServiceImpl(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<Good> getAllGoods() {
        return goodDao.getAllGood();
    }

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Map<Good, Integer> getGoodsByIds(String... itemIds) {
        Map<Good, Integer> itemsMap = new HashMap<>();
        if (Objects.isNull(itemIds)) {
            return itemsMap;
        }
        for (String id : itemIds) {
            Good good = getGoodById(Long.parseLong(id));
            itemsMap.merge(good, 1, Integer::sum);
        }
        return itemsMap;
    }

    private Good getGoodById(Long id) {
        return goodDao.getGoodById(id);
    }
}
