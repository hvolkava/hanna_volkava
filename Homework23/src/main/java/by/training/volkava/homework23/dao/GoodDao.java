package by.training.volkava.homework23.dao;

import by.training.volkava.homework23.model.Good;

import java.util.List;

public interface GoodDao {
    List<Good> getAllGood();

    Good getGoodById(Long id);
}
