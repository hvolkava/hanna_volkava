package by.training.volkava.homework23.service;

import by.training.volkava.homework23.model.Order;
import by.training.volkava.homework23.model.User;

public interface OrderService {
    Order getOrderByUser(User user);

    void addGoods(Order order, User user, String... goodId);

    Order saveOrderInDd(Order order, User user);

    Order getOrderById(Long id);
}
