package by.training.volkava.homework21.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"by.training.volkava.homework22.service", "by.training.volkava.homework22.dao"})
public class DataServiceConfig {

}
