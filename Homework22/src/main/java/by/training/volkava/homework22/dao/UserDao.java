package by.training.volkava.homework22.dao;

import by.training.volkava.homework22.model.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(int id);
}
