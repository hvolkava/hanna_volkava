package by.training.volkava.homework22.service;

import by.training.volkava.homework22.model.Order;
import by.training.volkava.homework22.model.User;

public interface OrderService {
    Order getOrderByUser(User user);

    void addGoods(Order order, User user, String... goodId);

    Order saveOrderInDd(Order order, User user);
}
