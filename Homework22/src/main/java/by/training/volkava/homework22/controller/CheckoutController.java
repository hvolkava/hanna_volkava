package by.training.volkava.homework22.controller;

import by.training.volkava.homework22.model.CustomUserDetails;
import by.training.volkava.homework22.model.Order;
import by.training.volkava.homework22.model.User;
import by.training.volkava.homework22.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServlet;

@Controller
public class CheckoutController extends HttpServlet {
    private OrderService orderService;

    public CheckoutController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/checkout")
    public String getCheckoutPage(Model model, Authentication authentication) {
        User user = ((CustomUserDetails) authentication.getPrincipal()).getUser();
        Order order = orderService.getOrderByUser(user);
        if (order.getGoodsMap().isEmpty()) {
            return "redirect:/goods";
        }
        orderService.saveOrderInDd(order, user);
        model.addAttribute("order", order);
        return "checkout";
    }
}
