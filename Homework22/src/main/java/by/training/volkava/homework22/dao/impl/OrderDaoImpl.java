package by.training.volkava.homework22.dao.impl;

import by.training.volkava.homework22.dao.OrderDao;
import by.training.volkava.homework22.model.Good;
import by.training.volkava.homework22.model.Order;
import by.training.volkava.homework22.model.OrderGood;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class OrderDaoImpl implements OrderDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderDaoImpl.class.getName());
    private SessionFactory sessionFactory;

    public OrderDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Order saveOrder(Order order) {
        Integer idSavedOrder = (Integer) sessionFactory.getCurrentSession().save(order);
        for (Map.Entry<Good, Integer> entry: order.getGoodsMap().entrySet()) {
            OrderGood orderGood = new OrderGood();
            orderGood.setOrderId(idSavedOrder);
            orderGood.setGoodId(entry.getKey().getId());
            orderGood.setCount(entry.getValue());
            sessionFactory.getCurrentSession().save(orderGood);
        }
        return order;
    }
}
