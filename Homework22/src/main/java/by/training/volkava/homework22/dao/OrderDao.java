package by.training.volkava.homework22.dao;

import by.training.volkava.homework22.model.Order;

public interface OrderDao {
    Order saveOrder(Order order);
}
