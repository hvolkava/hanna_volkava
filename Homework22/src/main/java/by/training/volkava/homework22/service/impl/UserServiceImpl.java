package by.training.volkava.homework22.service.impl;

import by.training.volkava.homework22.dao.UserDao;
import by.training.volkava.homework22.model.User;
import by.training.volkava.homework22.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service("userService")
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    public Optional<User> getUserByLogin(String login) {
        return userDao.getUserByLogin(login);
    }

    @Transactional
    public Optional<User> getUserById(int id) {
        return userDao.getUserById(id);
    }
}
