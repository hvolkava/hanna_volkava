package by.training.volkava.homework22.model;

import java.math.BigDecimal;
import java.util.StringJoiner;

public class Good {
    private int id;
    private String title;
    private BigDecimal price;

    public Good() {
    }

    /**
     * Create instance of good.
     *
     * @param id    id good
     * @param title title good
     * @param price price good
     */

    public Good(int id, String title, BigDecimal price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Good)) {
            return false;
        }
        Good good = (Good) object;
        if (getId() != good.getId()) {
            return false;
        }
        if (good.getPrice().compareTo(getPrice()) != 0) {
            return false;
        }
        return getTitle().equals(good.getTitle());
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getPrice().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Good.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("title='" + title + "'")
                .add("price=" + price)
                .toString();
    }
}
