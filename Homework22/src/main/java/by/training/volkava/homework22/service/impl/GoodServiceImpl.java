package by.training.volkava.homework22.service.impl;

import by.training.volkava.homework22.dao.GoodDao;
import by.training.volkava.homework22.model.Good;
import by.training.volkava.homework22.service.GoodService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
public class GoodServiceImpl implements GoodService {
    private GoodDao goodDao;

    public GoodServiceImpl(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    @Transactional
    public List<Good> getAllGoods() {
        return goodDao.getAllGood();
    }

    @Transactional
    public Map<Good, Integer> getGoodsByIds(String... itemIds) {
        Map<Good, Integer> itemsMap = new HashMap<>();
        if (Objects.isNull(itemIds)) {
            return itemsMap;
        }
        for (String id : itemIds) {
            Good good = getGoodById(Integer.parseInt(id));
            itemsMap.merge(good, 1, Integer::sum);
        }
        return itemsMap;
    }

    private Good getGoodById(int id) {
        return goodDao.getGoodById(id);
    }
}
