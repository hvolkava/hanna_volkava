package by.training.volkava.homework22.model;

import java.math.BigDecimal;
import java.util.Map;
import java.util.StringJoiner;

public class Order {
    private int id;
    private int userId;
    private BigDecimal totalPrice;
    private Map<Good, Integer> goodsMap;

    public Order() {
    }

    /**
     * Create instance of order.
     *
     * @param id         id order
     * @param userId     id user which make order
     * @param totalPrice total price order
     */
    public Order(int id, int userId, BigDecimal totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Map<Good, Integer> getGoodsMap() {
        return goodsMap;
    }

    public void setGoodsMap(Map<Good, Integer> goodsMap) {
        this.goodsMap = goodsMap;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Order.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("userId=" + userId)
                .add("totalPrice=" + totalPrice)
                .add("goodsMap=" + goodsMap)
                .toString();
    }
}
