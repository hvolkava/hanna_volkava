package by.training.volkava.homework22.dao;

import by.training.volkava.homework22.model.Good;

import java.util.List;

public interface GoodDao {
    List<Good> getAllGood();

    Good getGoodById(int id);
}
