package by.training.volkava.homework22.model;

public class User {
    private int id;
    private String login;
    private String password;

    public User() {
    }

    /**
     * Create instance of User.
     *
     * @param id       user id
     * @param login    user's login
     * @param password user's password
     */
    public User(int id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof User)) {
            return false;
        }
        User user = (User) object;
        if (getId() != user.getId()) {
            return false;
        }
        if (!getLogin().equals(user.getLogin())) {
            return false;
        }
        return getPassword() != null
                ? getPassword().equals(user.getPassword())
                : user.getPassword() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getLogin().hashCode();
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", login='").append(login).append('\'');
        sb.append(", password=[PROTECTED]");
        sb.append('}');
        return sb.toString();
    }
}
