package by.training.volkava.homework22.service;

import by.training.volkava.homework22.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(int id);
}
