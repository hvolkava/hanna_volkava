package by.training.volkava.homework22.service;

import by.training.volkava.homework22.model.Good;

import java.util.List;
import java.util.Map;

public interface GoodService {

    List<Good> getAllGoods();

    Map<Good, Integer> getGoodsByIds(String... itemIds);
}
