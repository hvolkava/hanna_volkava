package by.training.volkava.homework09.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class SetOperationsTest {
    private Set<String> setA;
    private Set<String> setB;

    @Before
    public void setUp() {
        setA = new HashSet<>();
        setA.add("A");
        setA.add("B");
        setB = new HashSet<>();
        setB.add("B");
        setB.add("C");
    }

    @Test
    public void testUnion() {
        Set<String> expected = new HashSet<>();
        expected.add("A");
        expected.add("B");
        expected.add("C");
        Assert.assertEquals(expected, SetOperations.union(setA, setB));
    }

    @Test
    public void testIntersection() {
        Set<String> expected = new HashSet<>();
        expected.add("B");
        Assert.assertEquals(expected, SetOperations.intersection(setA, setB));
    }

    @Test
    public void testMinus() {
        Set<String> expected = new HashSet<>();
        expected.add("A");
        Assert.assertEquals(expected, SetOperations.minus(setA, setB));
    }

    @Test
    public void testDifference() {
        Set<String> expected = new HashSet<>();
        expected.add("A");
        expected.add("C");
        Assert.assertEquals(expected, SetOperations.difference(setA, setB));
    }

    @Test(expected = NullPointerException.class)
    public void testUnion_WithNull() {
        SetOperations.union(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testIntersection_WithNull() {
        SetOperations.union(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testMinus_WithNull() {
        SetOperations.union(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void testDifference_WithNull() {
        SetOperations.union(null, null);
    }
}