package by.training.volkava.homework09.task1;

import org.junit.Assert;
import org.junit.Test;

public class SumOfDigitsTest {

    @Test
    public void testGetSumOfDigits_WithPositiveNumber() {
        int number = 12345;
        Assert.assertEquals(15, SumOfDigits.getSumOfDigits(number));
    }

    @Test
    public void testGetSumOfDigits_WithNegativeNumber() {
        int number = -12345;
        Assert.assertEquals(15, SumOfDigits.getSumOfDigits(number));
    }

    @Test(expected = NullPointerException.class)
    public void testGetSumOfDigits_WithNull() {
        Integer number = null;
        Assert.assertEquals(0, SumOfDigits.getSumOfDigits(number));
    }

    @Test
    public void testSort() {
        Integer[] array = {29, -15688, 81, 89, 10000};
        Integer[] expected = {10000, 81, 29, 89, -15688};
        SumOfDigits.sort(array);
        Assert.assertArrayEquals(expected, array);
    }

    @Test
    public void testSort_WithEmptyArray() {
        Integer[] array = {};
        Integer[] expected = {};
        SumOfDigits.sort(array);
        Assert.assertArrayEquals(expected, array);
    }

    @Test(expected = NullPointerException.class)
    public void testSort_WithNulArray() {
        Integer[] array = null;
        SumOfDigits.sort(array);
    }
}