package by.training.volkava.homework09.task2;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Class SetOperations helps to perform operations on sets.
 *
 * @author Hanna Volkava
 */
public class SetOperations {
    /**
     * The method performs the union of sets.
     *
     * @param firstSet  first set
     * @param secondSet second set
     * @return union of two sets
     */
    public static <E> Set<E> union(Set<E> firstSet, Set<E> secondSet) {
        checkOnNull(firstSet, secondSet);
        Set<E> resultSet = new HashSet<>(firstSet);
        resultSet.addAll(secondSet);
        return resultSet;
    }

    /**
     * The method performs the intersection of sets.
     *
     * @param firstSet  first set
     * @param secondSet second set
     * @return intersection of two sets
     */
    public static <E> Set<E> intersection(Set<E> firstSet, Set<E> secondSet) {
        checkOnNull(firstSet, secondSet);
        Set<E> resultSet = new HashSet<>();
        for (E object : firstSet) {
            if (secondSet.contains(object)) {
                resultSet.add(object);
            }
        }
        return resultSet;
    }

    /**
     * The method performs the complement of sets.
     *
     * @param firstSet  first set
     * @param secondSet second set
     * @return complement of two sets
     */
    public static <E> Set<E> minus(Set<E> firstSet, Set<E> secondSet) {
        checkOnNull(firstSet, secondSet);
        Set<E> resultSet = new HashSet<>();
        for (E object : firstSet) {
            if (!secondSet.contains(object)) {
                resultSet.add(object);
            }
        }
        return resultSet;
    }

    /**
     * The method performs the symmetric difference of sets.
     *
     * @param firstSet  first set
     * @param secondSet second set
     * @return symmetric difference of two sets
     */
    public static <E> Set<E> difference(Set<E> firstSet, Set<E> secondSet) {
        checkOnNull(firstSet, secondSet);
        Set<E> unionSet = union(firstSet, secondSet);
        Set<E> intersectionSet = intersection(firstSet, secondSet);
        return minus(unionSet, intersectionSet);
    }

    private static <E> void checkOnNull(Set<E> firstSet, Set<E> secondSet) {
        if (Objects.isNull(firstSet) || Objects.isNull(secondSet)) {
            throw new NullPointerException("Sets must be not null");
        }
    }
}
