package by.training.volkava.homework09.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class SumOfDigits helps to find sum of digits in number and
 * can sort integer array by sum of digits of numbers.
 *
 * @author Hanna Volkava
 */
public class SumOfDigits {
    /**
     * Method helps to find sum of digits in number.
     *
     * @param number number sum of digits we want to find
     * @return sum of digits in number
     */
    public static int getSumOfDigits(Integer number) {
        if (Objects.isNull(number)) {
            throw new NullPointerException("Number in getSumOfDigits() is null");
        }
        if (number < 0) {
            number = Math.abs(number);
        }
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    /**
     * Method sort integer array by sum of digits of numbers.
     *
     * @param array array we will be sort
     */
    public static void sort(Integer[] array) {
        if (Objects.isNull(array)) {
            throw new NullPointerException("Array is null");
        }
        ArrayList<Integer> list = Arrays.stream(array)
                .sorted(Comparator.comparing(SumOfDigits::getSumOfDigits))
                .collect(Collectors.toCollection(ArrayList::new));
        list.toArray(array);
    }
}
