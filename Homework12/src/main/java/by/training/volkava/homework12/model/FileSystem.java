package by.training.volkava.homework12.model;

import java.io.Serializable;

/**
 * Interface model is designed for build file system tree.
 * "Component" in pattern "Composite"
 *
 * @author Hanna Volkava
 */
public interface FileSystem extends Serializable {
    String getName();
}
