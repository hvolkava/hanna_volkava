package by.training.volkava.homework12;

import by.training.volkava.homework12.model.FileSystem;
import by.training.volkava.homework12.model.FileSystemException;
import by.training.volkava.homework12.model.Folder;
import by.training.volkava.homework12.util.FileSystemOut;
import by.training.volkava.homework12.util.FileSystemSerialize;
import by.training.volkava.homework12.util.FileSystemUtil;

import java.util.Optional;
import java.util.Scanner;

/**
 * Main application class for Homework11.
 * Program that emulates the file system model, its hierarchical structure.
 * It can also save the structure to a file and read it.
 *
 * @author Hanna Volkava
 */
public class Main {
    /**
     * Application start point. The program accepts as input a string
     * representing the path to the file / directory.
     * User can display the resulting directory structure at any time,
     * user can also save the structure to a file and read it.
     * User at any time can complete the work with the program.
     *
     * @param args command line arguments
     * @throws FileSystemException occurs when something wrong
     */
    public static void main(String[] args) throws FileSystemException {
        FileSystem myFileSystem = new Folder("MyFileSystem");
        printMenu();
        try (Scanner scan = new Scanner(System.in)) {
            boolean isExit = false;
            while (!isExit) {
                System.out.print("$>");
                String userString = scan.next().toLowerCase().trim();
                switch (userString) {
                    case "exit":
                        isExit = true;
                        break;
                    case "print":
                        System.out.print(FileSystemOut.printTree(myFileSystem));
                        break;
                    case "save":
                        FileSystemSerialize.write(myFileSystem);
                        break;
                    case "read":
                        Optional<FileSystem> fileSystem = FileSystemSerialize.read();
                        myFileSystem = fileSystem.orElse(myFileSystem);
                        break;
                    default:
                        FileSystemUtil.parseString(myFileSystem, userString);
                        break;
                }
            }
        } catch (FileSystemException exception) {
            System.out.println(exception);
        }
    }

    private static void printMenu() {
        System.out.println("You can enter a file system string.");
        System.out.println("Enter 'print' for print file system");
        System.out.println("Enter 'save' to save file system");
        System.out.println("Enter 'read' to restore file system from file");
        System.out.println("Enter 'exit' to exit the task");
    }
}
