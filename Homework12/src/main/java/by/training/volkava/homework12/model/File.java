package by.training.volkava.homework12.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class File represents an object file in model.
 * "Leaf" in pattern "Composite"
 *
 * @author Hanna Volkava
 */
public class File implements FileSystem {
    private String name;

    public File(String name) throws FileSystemException {
        setName(name);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) throws FileSystemException {
        if (Objects.nonNull(name) && !name.isEmpty()) {
            if (isFileName(name)) {
                this.name = name;
            } else {
                throw new FileSystemException("File must be have extension");
            }
        } else {
            throw new FileSystemException("File must be have name");
        }
    }

    /**
     * Method determines if the name has an extension.
     *
     * @param inputString name file
     * @return true if extension has, false if extension hasn't
     */
    public static boolean isFileName(String inputString) {
        Pattern pattern = Pattern.compile("\\w+[.{1}]\\w+$");
        Matcher matcher = pattern.matcher(inputString);
        return matcher.matches();
    }
}
