package by.training.volkava.homework12.model;

public class FileSystemException extends Exception {
    public FileSystemException(String message) {
        super(message);
    }
}
