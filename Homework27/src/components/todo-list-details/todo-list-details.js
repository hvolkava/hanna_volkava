import React from 'react';

const TodoListDetails = ({ item }) => {
    
    if (!item) {
        return <span>Sorry, this todo not found!</span>;
    };

    return (
        <>
            <h3>{item.title}</h3>
            <h4>{item.description}</h4>
        </>
    );
};

export default TodoListDetails;
