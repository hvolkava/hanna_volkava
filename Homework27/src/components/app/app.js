import React, { Component } from 'react'
import Container from "@material-ui/core/Container";
import { BrowserRouter as Router, Route } from 'react-router-dom'

import TodoList from '../todo-list'
import ItemAddForm from '../item-add-form';
import AppHeader from '../app-header';
import TodoListItem from '../todo-list-item/todo-list-item';
import TodoListDetails from '../todo-list-details';

export default class App extends Component {

    maxId=100;

    state = {
        items: [
            {id: 1, title: 'Check button "Save"', description: 'Check button "Save" description' },
            {id: 2, title: 'Check button "Update"', description: 'Check button "Update" description' },
            {id: 3, title: 'Check button "Delete"', description: 'Check button "Delete" description' },
            {id: 4, title: 'Check buttons "Extended" and "Collapse"', description: 'Check buttons "Extended" and "Collapse" description' }
        ],
        id: 0,
        title: '',
        description: '',
        create: true
    };

    createItem(title, description) {
        return {
            title,
            description,
            id: this.maxId++
        };
    };

    onItemAdded = () => {
        this.setState((state) => {
            console.log("in app onItemAdded", this.state.title, this.state.description);
            const item = this.createItem(this.state.title, this.state.description);
            return { items: [...state.items, item], create: true, title: '', description: '' };
        })
      };

    onDeleted = (id) => {
        this.setState((state) => {
            const idx = state.items.findIndex((item) => item.id === id);
            if (idx == -1) return;
            const items = [ ...state.items.slice(0, idx), ...state.items.slice(idx + 1) ];
            console.log("in app onDeleted()", id, this.state.title, this.state.description);
            return { items };
        });
    };

    onUpdated = (id) => {
        const item = this.state.items.find((it) => it.id === id);
        if (item == undefined) return;
        this.setState({
            title: item.title,
            description: item.description,
            id: item.id,
            create: false
        });
    };

    onItemEdited = () => {
        console.log("in app onItemEdited()", this.state.id, this.state.title, this.state.description);
        const itemupdated = { title: this.state.title, description: this.state.description, id: this.state.id };
        const idx = this.state.items.findIndex((item) => item.id === itemupdated.id);    
        const items = (idx != -1) ?
                [ ...this.state.items.slice(0, idx), itemupdated, ...this.state.items.slice(idx + 1) ] :
                this.state.items;
      
        this.setState(() => ({
            items: items,
            create: true,
            title: '',
            description: ''
        }));
    };

    onHandleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        const { items, create, title, description } = this.state;
        return (
            <Container maxWidth='md'>
                <Router>
                    <Route path="/" exact
                        render={() => {
                            return (<>
                                        <AppHeader />
                                        <ItemAddForm 
                                            onItemAdded={this.onItemAdded}
                                            onItemEdited={this.onItemEdited}
                                            onHandleChange={this.onHandleChange}
                                            create={create}
                                            title={title}
                                            description={description} />
                                        <TodoList 
                                            items={items}
                                            onDeleted={this.onDeleted }
                                            onUpdated={this.onUpdated} />
                                    </>);
                        }} />
                        
                    <Route path="/todoItem/:id" exact
                        render={({ match }) => {                            
                            const { id } = match.params;
                            console.log(id.value);
                            const idx = items.findIndex((item) => item.id == id);
                            const item = items.find((it) => it.id == id);
                            console.log(items, items[idx], idx, item);
                            return <TodoListDetails item={items[idx]} />
                        }} />
                </Router>
                
            </Container>
        );
    }; 
};