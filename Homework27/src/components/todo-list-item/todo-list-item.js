import React from 'react'
import Button from '@material-ui/core/Button';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';

const TodoListItem = ({ title, onDeleted, onUpdated, id }) => {

    return (
        <>
            <ListItemText>
                <Link to={`/todoItem/${id}`}>{title}</Link>
            </ListItemText>                
            <Button 
                variant="contained"
                color="default"
                onClick={onUpdated}>Update</Button>
            <Button
                variant="contained"
                color="secondary"
                onClick={onDeleted} >Delete</Button>
        </>
    );
};

export default TodoListItem;