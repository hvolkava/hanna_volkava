var orderData;

var order = $('#order');

if (localStorage["orderData"]) {
    orderData = JSON.parse(localStorage.getItem("orderData"));
}

var renderOrder = function(jsonResponse) {
    order.empty();
    order.append('<table id="tableOrder"></table>');
    var table = jsonResponse['goodsMap'];
    let index = 1;
    $.each(table, function (key, good) {
        let tr = $('<tr/>');
        tr.append("<td> " + index + ")</td><td>" + good.title + "</td><td>" + good.price + "</td><td>" + jsonResponse['idsGoodsCount'][key] + "</td></tr>'");
        $('#tableOrder').append(tr);
        index++;
    });
};
renderOrder(orderData);
document.getElementById("username").innerText = orderData["username"];
document.getElementById("totalPrice").innerText = orderData["price"];