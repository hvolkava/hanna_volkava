package by.training.volkava.homework24.dao;

import by.training.volkava.homework24.model.Order;

import java.util.Optional;

public interface OrderDao {
    void saveOrder(Order order);

    Optional<Order> getOrder(Long id);

    void updateOrder(Order order);
}
