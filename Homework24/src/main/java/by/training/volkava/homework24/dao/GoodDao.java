package by.training.volkava.homework24.dao;

import by.training.volkava.homework24.model.Good;

import java.util.List;
import java.util.Optional;

public interface GoodDao {
    List<Good> getAllGood();

    Optional<Good> getGoodById(Long id);

    void saveGood(Good good);
}
