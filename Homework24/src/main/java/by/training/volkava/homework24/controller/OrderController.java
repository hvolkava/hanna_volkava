package by.training.volkava.homework24.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import by.training.volkava.homework24.converter.OrderConverter;
import by.training.volkava.homework24.dto.OrderDto;
import by.training.volkava.homework24.model.CustomUserDetails;
import by.training.volkava.homework24.model.Order;
import by.training.volkava.homework24.model.User;
import by.training.volkava.homework24.service.OrderService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/orders")
public class OrderController {
    private OrderService orderService;
    private OrderConverter orderConverter;

    public OrderController(OrderService orderService, OrderConverter orderConverter) {
        this.orderService = orderService;
        this.orderConverter = orderConverter;
    }

    @PostMapping(produces = APPLICATION_JSON_UTF8_VALUE, consumes = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDto> createOrder(@RequestBody @Valid OrderDto orderDto,
                                                Authentication authentication,
                                                UriComponentsBuilder ucBuilder) {
        User user = ((CustomUserDetails) authentication.getPrincipal()).getUser();
        Order order = orderConverter.getOrderFromOrderDto(orderDto, user);
        orderService.saveOrderInDd(order);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/order/{id}").buildAndExpand(order.getId()).toUri());
        OrderDto result = orderConverter.getOrderDtoFromOrder(order);
        return new ResponseEntity<>(result, headers, HttpStatus.CREATED);

    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDto> getOrder(@PathVariable long id) {
        Optional<Order> optOrder = orderService.getOrderById(id);
        if (!optOrder.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        OrderDto orderDto = orderConverter.getOrderDtoFromOrder(optOrder.get());
        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDto> updateOrder(@PathVariable long id,
                                                @RequestBody OrderDto orderDto,
                                                Authentication authentication) {
        User user = ((CustomUserDetails) authentication.getPrincipal()).getUser();
        Optional<Order> optOrder = orderService.getOrderById(id);
        if (!optOrder.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Order order = optOrder.get();
        Order orderFromClient = orderConverter.getOrderFromOrderDto(orderDto, user);
        order.setGoodsMap(orderFromClient.getGoodsMap());
        order.setTotalPrice(orderFromClient.getTotalPrice());
        orderService.updateOrderInDd(order);
        OrderDto result = orderConverter.getOrderDtoFromOrder(order);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}

