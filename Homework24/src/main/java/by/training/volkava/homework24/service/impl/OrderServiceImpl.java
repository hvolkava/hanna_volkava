package by.training.volkava.homework24.service.impl;

import by.training.volkava.homework24.dao.OrderDao;
import by.training.volkava.homework24.model.Good;
import by.training.volkava.homework24.model.Order;
import by.training.volkava.homework24.service.GoodService;
import by.training.volkava.homework24.service.OrderService;
import by.training.volkava.homework24.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class.getName());
    private GoodService goodService;
    private UserService userService;
    private OrderDao orderDao;

    public OrderServiceImpl(GoodService goodService, UserService userService, OrderDao orderDao) {
        this.goodService = goodService;
        this.userService = userService;
        this.orderDao = orderDao;
    }

    public Order addGood(Order order, String goodId) {
        Map<Good, Integer> goodsMap = goodService.getGoodsByIds(goodId);
        Map<Good, Integer> goodMapsInOrder = order.getGoodsMap();
        goodsMap.forEach((key, value) -> goodMapsInOrder.merge(key, value, Integer::sum));
        order.setGoodsMap(goodMapsInOrder);
        order.setTotalPrice(calculateTotalSum(goodMapsInOrder));
        return order;
    }

    @Transactional(rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    public void saveOrderInDd(Order order) {
        orderDao.saveOrder(order);
        LOG.debug("{} saved {}",order.getUser(), order);
    }

    public BigDecimal calculateTotalSum(Map<Good, Integer> goodsMap) {
        return goodsMap.entrySet().stream()
                .map(entry -> entry.getKey().getPrice()
                        .multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateOrderInDd(Order order) {
        orderDao.updateOrder(order);
        LOG.debug("{} update {}", order.getUser(), order);
    }

    @Transactional(readOnly = true)
    public Optional<Order> getOrderById(Long id) {
        return orderDao.getOrder(id);
    }
}
