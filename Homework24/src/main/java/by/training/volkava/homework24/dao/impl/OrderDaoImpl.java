package by.training.volkava.homework24.dao.impl;

import by.training.volkava.homework24.dao.OrderDao;
import by.training.volkava.homework24.model.Order;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class OrderDaoImpl implements OrderDao {
    private SessionFactory sessionFactory;

    public OrderDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveOrder(Order order) {
        sessionFactory.getCurrentSession().save(order);
    }

    public Optional<Order> getOrder(Long id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Order.class, id));
    }

    @Override
    public void updateOrder(Order order) {
        sessionFactory.getCurrentSession().update(order);
    }
}
