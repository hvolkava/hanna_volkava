package by.training.volkava.homework24.dao;

import by.training.volkava.homework24.model.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Long id);
}
