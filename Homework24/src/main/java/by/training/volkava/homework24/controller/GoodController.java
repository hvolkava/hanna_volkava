package by.training.volkava.homework24.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import by.training.volkava.homework24.model.Good;
import by.training.volkava.homework24.service.GoodService;
import by.training.volkava.homework24.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "goods")
public class GoodController {
    private OrderService orderService;
    private GoodService goodService;

    public GoodController(OrderService orderService, GoodService goodService) {
        this.orderService = orderService;
        this.goodService = goodService;
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Good>> getListGoods() {
        List<Good> goodsList = goodService.getAllGoods();
        if (goodsList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(goodsList, HttpStatus.OK);
    }

    @PostMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Good> greateGood(@Valid @RequestBody Good good) {
        goodService.saveGood(good);
        return new ResponseEntity<>(good, HttpStatus.OK);
    }

}
