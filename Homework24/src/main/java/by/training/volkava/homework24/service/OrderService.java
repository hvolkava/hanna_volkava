package by.training.volkava.homework24.service;

import by.training.volkava.homework24.model.Good;
import by.training.volkava.homework24.model.Order;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

public interface OrderService {

    Order addGood(Order order, String goodId);

    void saveOrderInDd(Order order);

    Optional<Order> getOrderById(Long id);

    BigDecimal calculateTotalSum(Map<Good, Integer> goodsMap);

    void updateOrderInDd(Order order);
}
