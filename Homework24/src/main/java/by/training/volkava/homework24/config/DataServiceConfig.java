package by.training.volkava.homework24.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"by.training.volkava.homework24.service", "by.training.volkava.homework24.dao",
        "by.training.volkava.homework24.converter"})
public class DataServiceConfig {

}
