package by.training.volkava.homework24.service;

import by.training.volkava.homework24.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> getUserByLogin(String login);

    Optional<User> getUserById(Long id);
}
