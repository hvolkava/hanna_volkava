package by.training.volkava.homework24.converter;

import by.training.volkava.homework24.dto.OrderDto;
import by.training.volkava.homework24.model.Good;
import by.training.volkava.homework24.model.Order;
import by.training.volkava.homework24.model.User;
import by.training.volkava.homework24.service.GoodService;
import by.training.volkava.homework24.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class OrderConverter {
    private static final Logger LOG = LoggerFactory.getLogger(OrderConverter.class.getName());
    private GoodService goodService;
    private OrderService orderService;

    public OrderConverter(GoodService goodService, OrderService orderService) {
        this.goodService = goodService;
        this.orderService = orderService;
    }

    public Order getOrderFromOrderDto(OrderDto orderDto, User user) {
        Order order = new Order();
        order.setUser(user);
        Map<Good, Integer> goodsMap = new HashMap();
        Map<String, Integer> idsGoodsCount = orderDto.getIdsGoodsCount();
        idsGoodsCount.forEach(
                (idGood, count) -> {
                    Optional<Good> optionalGood = goodService.getGoodById(Long.parseLong(idGood));
                    if (optionalGood.isPresent()) {
                        goodsMap.put(optionalGood.get(), count);
                    } else {
                        LOG.warn("Trying get GOOD by ID equals {} and not found", idGood);
                    }
                });
        order.setGoodsMap(goodsMap);
        order.setTotalPrice(orderService.calculateTotalSum(goodsMap));
        return order;
    }

    public OrderDto getOrderDtoFromOrder(Order order) {
        OrderDto orderDto = new OrderDto();
        User user = order.getUser();
        orderDto.setUsername(user.getLogin());
        orderDto.setPrice(order.getTotalPrice());
        Map<String, Integer> idsGoodsCount = new HashMap<>();
        Map<String, Good> goodsMap = new HashMap<>();
        order.getGoodsMap().forEach((good, count) -> {
            idsGoodsCount.put(good.getId().toString(), count);
            goodsMap.put(good.getId().toString(), good);
        });
        orderDto.setIdsGoodsCount(idsGoodsCount);
        orderDto.setGoodsMap(goodsMap);
        return orderDto;
    }
}
