package by.training.volkava.homework03.task2;

import java.util.Arrays;

/**
 * Median utility class looks for the value of the middle element
 * for a sorted array with an odd number of elements, and the arithmetic average of
 * two central elements for a sorted array with an even number of elements.
 *
 * @author Hanna Volkava
 */
public class Median {
    /**
     * Looking for median value in int source array.
     *
     * @param arraySource source array
     * @return float median value
     */
    static float median(int[] arraySource) {
        int[] array = arraySource.clone();
        Arrays.sort(array);
        int medianIndex = array.length / 2;
        float medianValue;
        if (array.length % 2 == 1) {
            medianValue = array[medianIndex];
        } else {
            medianValue = (float) (array[medianIndex - 1] + array[medianIndex]) / 2;
        }
        return medianValue;
    }

    /**
     * Looking for median value in double source array.
     *
     * @param arraySource source array
     * @return double median value
     */
    static double median(double[] arraySource) {
        double[] array = arraySource.clone();
        Arrays.sort(array);
        int medianIndex = array.length / 2;
        double medianValue;
        if (array.length % 2 == 1) {
            medianValue = array[medianIndex];
        } else {
            medianValue = (array[medianIndex - 1] + array[medianIndex]) / 2;
        }
        return medianValue;
    }
}
