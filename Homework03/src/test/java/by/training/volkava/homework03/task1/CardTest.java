package by.training.volkava.homework03.task1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test suite for Card class.
 */
public class CardTest {

    @Test
    public void testGetAccountBalance() throws CardException {
        Card card = new Card("John Test", 100);
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 100;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test
    public void testReplenishAccountBalance_WithCorrectAmount() throws CardException {
        Card card = new Card("John Test");
        card.replenishAccountBalance(100);
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 100;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test
    public void testWithdrawAccountBalance_WithCorrectAmount() throws CardException {
        Card card = new Card("John Test", 500);
        card.withdrawAccountBalance(100);
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 400;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test
    public void testConvertToAnotherCurrency_WithCorrectExchangeRate() throws CardException {
        Card card = new Card("John Test", 100);
        double exchangeRate = 2.5;
        double actualBalance = card.convertToAnotherCurrency(exchangeRate);
        double expectedBalance = 250;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test(expected = CardException.class)
    public void testConvertToAnotherCurrency_WithNegativeExchangeRate() throws CardException {
        Card card = new Card("John Test", 100);
        double exchangeRate = -2.5;
        card.convertToAnotherCurrency(exchangeRate);
    }

    @Test(expected = CardException.class)
    public void testReplenishAccountBalance_WithNegativeAmount() throws CardException {
        Card card = new Card("John Test", 100);
        card.replenishAccountBalance(-100);
    }

    @Test(expected = CardException.class)
    public void testWithdrawAccountBalance_WithNegativeAmount() throws CardException {
        Card card = new Card("John Test", 500);
        card.withdrawAccountBalance(-100);
    }

    @Test(expected = CardException.class)
    public void testCardConstructor_WithoutNameHolder() throws CardException {
        Card card = new Card("", 500);
    }

    @Test(expected = CardException.class)
    public void testCardConstructor_WithNegativeAccountBalance() throws CardException {
        Card card = new Card("John Test", -500);
    }
}