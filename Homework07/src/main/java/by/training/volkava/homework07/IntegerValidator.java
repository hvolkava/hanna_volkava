package by.training.volkava.homework07;

/**
 * Class for validate Integer number.
 *
 * @author Hanna Volkava
 */
public class IntegerValidator implements Validator<Integer> {
    /**
     * Method for validate number.
     * Integer numbers must belong to the interval [1,10]
     *
     * @param variable number which we want validate
     * @throws ValidationFailedException if the data does not comply
     *                                   with the rules, throws an exception
     */
    public void validate(Integer variable) throws ValidationFailedException {
        int lowerBound = 1;
        int upperBound = 10;
        if ((variable < lowerBound) || (variable > upperBound)) {
            throw new ValidationFailedException("Integer validation failed: " + variable);
        }
    }
}