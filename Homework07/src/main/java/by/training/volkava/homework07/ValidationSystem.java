package by.training.volkava.homework07;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * ValidationSystem used for validate input variable.
 *
 * @author Hanna Volkava
 */
public class ValidationSystem {
    private static Map<String, Validator> validators = new HashMap<>();

    static {
        validators.put("Integer", new IntegerValidator());
        validators.put("String", new StringValidator());
    }

    /**
     * Validate input variable.
     *
     * @param variable value
     * @throws ValidationFailedException occurs if this type value we don't know
     */

    public static void validate(Object variable) throws ValidationFailedException {
        Validator validator = ValidationSystem
                .getValidator(variable.getClass().getSimpleName())
                .orElseThrow(() -> new ValidationFailedException("Unknown type for validate"));
        validator.validate(variable);
    }

    public static Optional<Validator> getValidator(String typeName) {
        return Optional.ofNullable(validators.get(typeName));
    }
}
