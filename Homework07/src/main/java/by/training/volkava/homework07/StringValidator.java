package by.training.volkava.homework07;

/**
 * Class for validate input string.
 *
 * @author Hanna Volkava
 */
public class StringValidator implements Validator<String> {
    /**
     * Method for validate input string.
     * The string must begin with a capital letter
     *
     * @param userString input string which we want validate
     * @throws ValidationFailedException if the data does not comply
     *                                   with the rules, throws an exception
     */
    public void validate(String userString) throws ValidationFailedException {
        if (!userString.matches("^[A-Z].*")) {
            throw new ValidationFailedException("String validation failed: " + userString);
        }
    }
}
