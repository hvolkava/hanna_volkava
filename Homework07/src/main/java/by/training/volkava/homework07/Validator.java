package by.training.volkava.homework07;

public interface Validator<T> {
    public void validate(T variable) throws ValidationFailedException;
}