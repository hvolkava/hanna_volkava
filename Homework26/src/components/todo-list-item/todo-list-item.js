import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import ListItemText from '@material-ui/core/ListItemText';

export default class TodoListItem extends Component {

    state = {
        isShow: false,
    };

    onDescriptionClick = () => {   
        this.setState(({isShow}) => {
            return { isShow: !isShow };
        });
    };

    render() {

        const { title, description, onDeleted, onUpdated } = this.props;

        const buttonName = !this.state.isShow ? "Extended" : "Collapse";

        return (
            <>
                <ListItemText>{title}{ this.state.isShow ? <p>{description}</p> : null }</ListItemText>                
                <Button
                    variant="contained"
                    color="default"
                    onClick={this.onDescriptionClick} >{buttonName}</Button>
                <Button 
                    variant="contained"
                    color="default"
                    onClick={onUpdated}>Update</Button>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={onDeleted} >Delete</Button>
            </>
        );
    };
};