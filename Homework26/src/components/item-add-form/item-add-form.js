import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';

export default class ItemAddForm extends Component { 

    onSubmit = (e) => {
        e.preventDefault();
        const cb = this.props.create ? this.props.onItemAdded || (() => {})
                                     : this.props.onItemEdited || (() => {});
        cb();        
    };

    render() {

        const buttonName = this.props.create ? "Save" : "Update";

        const inputIsEmpty = this.props.title === "" || this.props.description === "" ? true : false; 

        return (
            <FormControl fullWidth >
                <Input type="text"
                    value={this.props.title}
                    onChange={this.props.onHandleChange}
                    placeholder="Title"                
                    variant="outlined"
                    name="title"
                />
                <Input type="text"
                    value={this.props.description}
                    onChange={this.props.onHandleChange}
                    placeholder="Description"                
                    variant="outlined"
                    name="description"
                />
                <Button variant="contained" color="primary" disabled={inputIsEmpty} onClick={this.onSubmit}>
                    {buttonName}
                </Button>
            </FormControl>
        );
    };
};