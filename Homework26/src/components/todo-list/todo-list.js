import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import TodoListItem from '../todo-list-item/todo-list-item';
  
const TodoList = ({ items, onDeleted, onUpdated }) => {
    
    const elements = items.map((item) => {
        const { id, ...itemProps } = item;
        return (
            <ListItem key={id}>         
                <TodoListItem 
                        { ...itemProps }
                        onDeleted={ () => onDeleted(id)} 
                        onUpdated={ () => onUpdated(id)}
                />
            </ListItem>
        );
    });

    return (
        
        <List>
            { elements }
        </List>
       
    );
};

export default TodoList;