package by.training.volkava.homework15.service;

import java.util.Objects;

public class UserService {
    public boolean isUserIdentification(String username) {
        return (Objects.isNull(username) || username.trim().isEmpty());
    }
}
