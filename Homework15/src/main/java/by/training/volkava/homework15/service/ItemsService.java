package by.training.volkava.homework15.service;

import by.training.volkava.homework15.model.Item;
import by.training.volkava.homework15.util.ItemsSingleton;

import java.util.HashSet;
import java.util.Set;

public class ItemsService {
    /**
     * Method converts the id array into set items.
     *
     * @param itemIds array of id
     * @return set items
     */
    public Set<Item> convertStringIdsToObjects(String[] itemIds) {
        Set<Item> items = new HashSet<>();
        for (String id : itemIds) {
            Item item = getItemById(Integer.parseInt(id));
            items.add(item);
        }
        return items;
    }

    /**
     * Get item by id.
     *
     * @param id id of item
     * @return item
     */
    private Item getItemById(int id) {
        Set<Item> setItems = ItemsSingleton.getInstance();
        return setItems.stream().filter(item -> item.getId() == id).findAny()
                .orElseThrow(() -> new IllegalArgumentException("Couldn't find item "
                        + "with id equals" + id));
    }
}
