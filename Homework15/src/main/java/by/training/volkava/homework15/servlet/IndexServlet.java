package by.training.volkava.homework15.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "index", urlPatterns = {"", "/index"})
public class IndexServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Login page</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("<body><div>");
            out.println("<h2>Welcome to Online Shop</h2>");
            out.println("<form action='items' method='POST'>");
            out.println("<input type='text' id='username' name='username'"
                    + "placeholder='Enter your name' >");
            out.println("<input type='submit' value='Enter'>");
            out.println("</form>");
            out.println("</div></body>");
            out.println("</html>");
        }
    }
}
