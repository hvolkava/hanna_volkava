package by.training.volkava.homework15.servlet;

import by.training.volkava.homework15.model.Item;
import by.training.volkava.homework15.service.OrderService;
import by.training.volkava.homework15.service.UserService;
import by.training.volkava.homework15.util.OrdersSingleton;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "checkout", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        UserService userService = new UserService();
        String username = request.getParameter("username");
        if (userService.isUserIdentification(username)) {
            getServletContext().getRequestDispatcher("/index").forward(request, response);
        }
        String[] itemIds = request.getParameterValues("items");
        OrderService orderService = new OrderService();
        orderService.createOrder(username, itemIds);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Checkout</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("</head><body><div>");
            out.printf("<h2>Dear %s, your order:</h2>", username);
            out.println(getInfoByOrderInHtml(username));
            out.println("</div></body>");
            out.println("</html>");
        }
    }

    private String getInfoByOrderInHtml(String username) {
        StringBuilder sb = new StringBuilder();
        int counter = 1;
        Set<Item> setItems = OrdersSingleton.getInstance().get(username);
        for (Item item : setItems) {
            sb.append(String.format("<p>%d) %s  %6.2f $</p>",
                    counter++, item.getName(), item.getPrice()));
        }
        double sum = setItems.stream().mapToDouble(Item::getPrice).sum();
        sb.append(String.format("<p> Total: $ %6.2f</p>", sum));
        return sb.toString();
    }
}
