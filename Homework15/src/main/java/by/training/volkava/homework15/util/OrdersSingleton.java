package by.training.volkava.homework15.util;

import by.training.volkava.homework15.model.Item;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


public class OrdersSingleton {
    private static Map<String, Set<Item>> ordersMap;

    private OrdersSingleton() {

    }

    /**
     * Get get all items fo online-shop.
     *
     * @return set items
     */
    public static Map<String, Set<Item>> getInstance() {
        if (Objects.isNull(ordersMap)) {
            ordersMap = new HashMap<>();
        }
        return ordersMap;
    }

}
