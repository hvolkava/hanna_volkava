package by.training.volkava.homework15.servlet;

import by.training.volkava.homework15.model.Item;
import by.training.volkava.homework15.service.UserService;
import by.training.volkava.homework15.util.ItemsSingleton;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "items", urlPatterns = {"/items"})
public class ItemsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        UserService userService = new UserService();
        String username = request.getParameter("username");
        if (userService.isUserIdentification(username)) {
            getServletContext().getRequestDispatcher("/index").forward(request, response);
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Choose items</title>");
            out.println("<link href='css/style.css' rel='stylesheet' type='text/css'/>");
            out.println("</head><body><div>");
            out.printf("<h2>Hello %s</h2>", username);
            out.println("<p>Make your order</p>");
            out.println("<form id='form' action='checkout' method='POST'>");
            out.printf("<input name='username' value='%s' hidden='true'>", username);
            out.println("<select name='items' required multiple='multiple' width='200px'>");
            Set<Item> setItems = ItemsSingleton.getInstance();
            for (Item item : setItems) {
                StringBuilder sb = new StringBuilder("<option value='");
                sb.append(item.getId()).append("'>").append(item.getName()).append("  (")
                        .append(item.getPrice()).append("$)</option>");
                out.println(sb);
            }
            out.println("</select><br>");
            out.println("<input type='submit' width='200px' value='Submit'>");
            out.println("</form>");
            out.println("</div></body>");
            out.println("</html>");
        }
    }
}
