package by.training.volkava.homework15.service;

import by.training.volkava.homework15.model.Item;
import by.training.volkava.homework15.util.OrdersSingleton;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
public class OrderService {
    /**
     * Create order.
     *
     * @param username username
     * @param itemsId array of items id
     */
    public void createOrder(String username, String[] itemsId) {
        ItemsService itemsService = new ItemsService();
        Set<Item> itemsSet = itemsService.convertStringIdsToObjects(itemsId);
        Set<Item> cart = new HashSet<>(itemsSet);
        Map<String, Set<Item>> ordersMap = OrdersSingleton.getInstance();
        ordersMap.put(username, cart);
    }
}
