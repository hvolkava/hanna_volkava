package by.training.volkava.homework19.servlet;

import by.training.volkava.homework19.model.Good;
import by.training.volkava.homework19.model.Order;
import by.training.volkava.homework19.model.User;
import by.training.volkava.homework19.service.OrderService;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "checkout", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {
    private OrderService orderService;

    /**
     * Method is needed to injection the service in the servlet.
     */
    public void init() {
        ApplicationContext applicationContext = (ApplicationContext) getServletContext()
                .getAttribute("applicationContext");
        orderService = (OrderService) applicationContext.getBean("orderService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        User user = (User) request.getSession(false).getAttribute("user");
        Map<Good, Integer> goodsMap = orderService.getOrderByUser(user);
        if (goodsMap.isEmpty()) {

            response.sendRedirect(request.getContextPath() + "/goods");
        }
        Order order = orderService.saveOrderInDd(user, goodsMap);
        request.setAttribute("order", order);
        request.setAttribute("goodsMap", goodsMap);

        getServletContext().getRequestDispatcher("/WEB-INF/jsp/checkout.jsp")
                .forward(request, response);
    }
}
