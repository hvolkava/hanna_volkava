package by.training.volkava.homework19.model;

import java.util.StringJoiner;

public class Good {
    private int id;
    private String title;
    private double price;

    /**
     * Create instance of good.
     *
     * @param id    id good
     * @param title title good
     * @param price price good
     */
    public Good(int id, String title, double price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Good)) {
            return false;
        }
        Good good = (Good) object;
        if (getId() != good.getId()) {
            return false;
        }
        if (Double.compare(good.getPrice(), getPrice()) != 0) {
            return false;
        }
        return getTitle().equals(good.getTitle());
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId();
        result = 31 * result + getTitle().hashCode();
        temp = Double.doubleToLongBits(getPrice());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Good.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("title='" + title + "'")
                .add("price=" + price)
                .toString();
    }
}
