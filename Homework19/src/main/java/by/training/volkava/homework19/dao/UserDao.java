package by.training.volkava.homework19.dao;

import by.training.volkava.homework19.model.User;

public interface UserDao {
    User getUserByLogin(String login);
}
