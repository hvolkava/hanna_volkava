package by.training.volkava.homework19.dao;

import by.training.volkava.homework19.model.Good;

import java.util.Set;

public interface GoodDao {
    Set<Good> getAllGood();

    Good getGoodById(int id);
}
