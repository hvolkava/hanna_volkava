package by.training.volkava.homework19.dao;

import by.training.volkava.homework19.model.Good;
import by.training.volkava.homework19.model.Order;

import java.util.Map;

public interface OrderGoodDao {
    void insertOrderGood(Order order, Map<Good, Integer> goodsMap);
}
