package by.training.volkava.homework19.service;

import by.training.volkava.homework19.dao.OrderDao;
import by.training.volkava.homework19.dao.OrderGoodDao;
import by.training.volkava.homework19.model.Good;
import by.training.volkava.homework19.model.Order;
import by.training.volkava.homework19.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
public class OrderService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderService.class.getName());
    private static Map<User, Map<Good, Integer>> ordersMap;
    private GoodsService goodService;
    private OrderDao orderDao;
    private OrderGoodDao orderGoodDao;

    /**
     * Constructor to create a class instance.
     * @param goodService goods service
     * @param orderDao dao object for work with order
     * @param orderGoodDao dao object for work with orderGood
     */
    public OrderService(GoodsService goodService, OrderDao orderDao, OrderGoodDao orderGoodDao) {
        this.goodService = goodService;
        this.orderDao = orderDao;
        this.orderGoodDao = orderGoodDao;
    }

    /**
     * Get get all items fo online-shop.
     *
     * @return set items
     */
    private static Map<User, Map<Good, Integer>> getGoods() {
        if (Objects.isNull(ordersMap)) {
            ordersMap = new HashMap<>();
        }
        return ordersMap;
    }

    /**
     * Gets map order by username.
     *
     * @param user user
     * @return map order
     */
    public Map<Good, Integer> getOrderByUser(User user) {
        if (getGoods().containsKey(user)) {
            return ordersMap.get(user);
        } else {
            return new HashMap<>();
        }
    }

    /**
     * Add item in cart.
     *
     * @param user   user
     * @param goodId id item
     */
    public void addGoods(User user, String... goodId) {
        Map<Good, Integer> goodsMap = goodService.convertStringIdsToObjects(goodId);
        Map<User, Map<Good, Integer>> ordersMap = getGoods();
        if (getGoods().containsKey(user)) {
            Map<Good, Integer> mapGoods = ordersMap.get(user);
            goodsMap.forEach((key, value) -> mapGoods.merge(key, value, Integer::sum));
        } else {
            ordersMap.put(user, goodsMap);
        }
    }

    /**
     * Method for saving order in database.
     *
     * @param user     user
     * @param goodsMap map of goods
     * @return created order
     */
    public Order saveOrderInDd(User user, Map<Good, Integer> goodsMap) {
        Order createdOrder = orderDao.saveOrder(user, goodsMap);
        orderGoodDao.insertOrderGood(createdOrder, goodsMap);
        ordersMap.remove(user);
        if (Objects.nonNull(createdOrder)) {
            LOG.debug(user.toString() + " saved in BD order " + createdOrder.toString());
        }
        return createdOrder;
    }
}
