package by.training.volkava.homework19.service;

import by.training.volkava.homework19.dao.UserDao;
import by.training.volkava.homework19.model.User;

public class UserService {
    private UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * Get user by his login.
     *
     * @param login user login
     * @return user
     */
    public User getUserByLogin(String login) {
        return userDao.getUserByLogin(login);
    }
}
