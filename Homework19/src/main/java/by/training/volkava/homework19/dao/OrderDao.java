package by.training.volkava.homework19.dao;

import by.training.volkava.homework19.model.Good;
import by.training.volkava.homework19.model.Order;
import by.training.volkava.homework19.model.User;

import java.util.Map;

public interface OrderDao {
    Order saveOrder(User user, Map<Good, Integer> goodsMap);
}
