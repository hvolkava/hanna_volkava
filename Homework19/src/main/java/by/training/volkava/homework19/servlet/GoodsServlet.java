package by.training.volkava.homework19.servlet;

import by.training.volkava.homework19.model.User;
import by.training.volkava.homework19.service.GoodsService;
import by.training.volkava.homework19.service.OrderService;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "goods", urlPatterns = {"/goods"})
public class GoodsServlet extends HttpServlet {
    private OrderService orderService;
    private GoodsService goodService;

    /**
     * Method is needed to injection the service in the servlet.
     */
    public void init() {
        ApplicationContext applicationContext = (ApplicationContext) getServletContext()
                .getAttribute("applicationContext");
        orderService = (OrderService) applicationContext.getBean("orderService");
        goodService = (GoodsService) applicationContext.getBean("goodService");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        User user = (User) request.getSession(false).getAttribute("user");
        String[] goodIds = request.getParameterValues("goods");
        orderService.addGoods(user, goodIds);

        request.setAttribute("order", orderService.getOrderByUser(user));
        request.setAttribute("goods", goodService.getAllGoods());

        getServletContext().getRequestDispatcher("/WEB-INF/jsp/goods.jsp")
                .forward(request, response);
    }
}
