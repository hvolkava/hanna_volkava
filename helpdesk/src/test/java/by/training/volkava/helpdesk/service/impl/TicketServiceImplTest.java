package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.TicketConverter;
import by.training.volkava.helpdesk.converter.TicketListConverter;
import by.training.volkava.helpdesk.dao.TicketDao;
import by.training.volkava.helpdesk.dto.CategoryDto;
import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.dto.TicketDtoList;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.enums.Urgency;
import by.training.volkava.helpdesk.exception.TicketException;
import by.training.volkava.helpdesk.model.Category;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.EmailService;
import by.training.volkava.helpdesk.service.HistoryService;
import by.training.volkava.helpdesk.service.TicketService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.security.AccessControlException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class TicketServiceImplTest {

    @Mock
    private ApplicationContext applicationContext;
    @Mock
    private HistoryService historyService;
    @Mock
    private TicketDao ticketDao;
    @Mock
    private TicketConverter ticketConverter;
    @Mock
    private EmailService emailService;
    @Mock
    private TicketListConverter ticketListConverter;

    private User employee;
    private User manager;
    private User engineer;

    @InjectMocks
    private TicketServiceImpl ticketService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        employee = new User();
        employee.setEmail("employee@test");employee.setRole(Role.EMPLOYEE);
        manager = new User();
        manager.setEmail("manager@test");manager.setRole(Role.MANAGER);
        engineer = new User();
        engineer.setEmail("manager@test");engineer.setRole(Role.ENGINEER);
    }

    @Test
    public void testGetAllTickets_WhenDaoReturnEmptyTicketList() {
        List<Ticket> list = Arrays.asList();
        Mockito.when(ticketDao.getAllTickets(employee))
                .thenReturn(list);
        Mockito.when(ticketListConverter.convertToDto(list, employee))
                .thenReturn(new TicketDtoList(Arrays.asList()));
        TicketDtoList listDto = ticketService.getAllTickets(employee);
    }

    @Test
    public void testGetTicketById_WhenDaoReturnSomeResult() {
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        Assert.assertEquals(ticket, ticketService.getTicketById(10L, employee));
    }

    @Test(expected = TicketException.class)
    public void testGetTicketById_WhenDaoReturnNull() {
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.ofNullable(null));
        ticketService.getTicketById(10L, employee);
    }

    @Test(expected = AccessControlException.class)
    public void testGetTicketById_WhenDaoReturnSomeResultButTicketNotAvailableForUser() {
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setState(TicketState.DRAFT)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        ticketService.getTicketById(10L, manager);
    }

    @Test
    public void testCreateTicket_WhenTicketSavedWithStateNew() {
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setCategory(new CategoryDto())
                .setUrgency(Urgency.AVERAGE)
                .setDesiredDate(LocalDate.now())
                .setName("test")
                .setState(TicketState.NEW)
                .build();
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setCategory(new Category())
                .setUrgency(Urgency.AVERAGE)
                .setDesiredResolutionDate(LocalDate.now())
                .setName("test")
                .setState(TicketState.NEW)
                .build();
        Mockito.when(ticketConverter.convertToEntity(ticketDto)).thenReturn(ticket);
        Assert.assertEquals(ticket.getId(), ticketService.createTicket(ticketDto, employee));
        verify(emailService).notifyUsers(ticket, TicketState.NEW);
        verify(ticketDao, times(1)).saveTicket(ticket);
        verify(historyService).createHistory(employee, ticket, "Ticket is created", null);
    }

    @Test
    public void testCreateTicket_WhenTicketSavedWithStateDraft() {
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setCategory(new CategoryDto())
                .setUrgency(Urgency.AVERAGE)
                .setDesiredDate(LocalDate.now())
                .setName("test")
                .setState(TicketState.DRAFT)
                .build();
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setCategory(new Category())
                .setUrgency(Urgency.AVERAGE)
                .setDesiredResolutionDate(LocalDate.now())
                .setName("test")
                .setState(TicketState.DRAFT)
                .build();
        Mockito.when(ticketConverter.convertToEntity(ticketDto)).thenReturn(ticket);
        Assert.assertEquals(ticket.getId(), ticketService.createTicket(ticketDto, employee));
        verify(emailService, times(0)).notifyUsers(ticket, TicketState.NEW);
        verify(ticketDao, times(1)).saveTicket(ticket);
        verify(historyService).createHistory(employee, ticket, "Ticket is created", null);
    }

    @Test
    public void testChangeState_WhenTicketChangeFromDraftToNewByOwner() {
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setState(TicketState.DRAFT)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        ticketService.changeState(10L, employee, TicketState.NEW);
        verify(historyService).createHistory(employee, ticket,
                "Ticket Status is changed", " from DRAFT to NEW");
        verify(emailService, times(1)).notifyUsers(ticket, TicketState.NEW);
    }

    @Test
    public void testChangeState_WhenTicketChangeFromNewToDeclinedByApprover() {
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setState(TicketState.NEW)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        ticketService.changeState(10L, manager, TicketState.DECLINED);
        Assert.assertEquals(manager, ticket.getUserApprover());
        verify(historyService).createHistory(manager, ticket,
                "Ticket Status is changed", " from NEW to DECLINED");
        verify(emailService, times(1)).notifyUsers(ticket, TicketState.DECLINED);
    }

    @Test
    public void testChangeState_WhenTicketChangeFromNewToCancelledByApprover() {
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setState(TicketState.NEW)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        ticketService.changeState(10L, manager, TicketState.CANCELLED);
        Assert.assertEquals(manager, ticket.getUserApprover());
        verify(historyService).createHistory(manager, ticket,
                "Ticket Status is changed", " from NEW to CANCELLED");
        verify(emailService, times(1)).notifyUsers(ticket, TicketState.CANCELLED);
    }

    @Test
    public void testChangeState_WhenTicketChangeFromApprovedToCancelledByAssigner() {
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setState(TicketState.APPROVED)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        ticketService.changeState(10L, engineer, TicketState.CANCELLED);
        Assert.assertEquals(engineer, ticket.getUserAssignee());
        verify(historyService).createHistory(engineer, ticket,
                "Ticket Status is changed", " from APPROVED to CANCELLED");
        verify(emailService, times(1)).notifyUsers(ticket, TicketState.CANCELLED);
    }

    @Test(expected = AccessControlException.class)
    public void testChangeState_WhenTicketChangeFromCancelledToNewByOwner() {
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setState(TicketState.CANCELLED)
                .setUserOwner(employee)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        ticketService.changeState(10L, employee, TicketState.NEW);
    }

    @Test(expected = AccessControlException.class)
    public void testChangeState_WhenManagerCouldNotApprovedTicketsHimself() {
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setUserOwner(manager)
                .setName("test")
                .setState(TicketState.NEW)
                .setUrgency(Urgency.AVERAGE)
                .setDesiredResolutionDate(LocalDate.now())
                .build();

        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        ticketService.changeState(10L, manager, TicketState.APPROVED);
    }

    @Test
    public void testGetTicketDtoById() {
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setUserOwner(employee)
                .setName("test")
                .setState(TicketState.DRAFT)
                .setUrgency(Urgency.AVERAGE)
                .setDesiredResolutionDate(LocalDate.now())
                .build();
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setId(10L)
                .setState(TicketState.DRAFT)
                .setName("test")
                .setDesiredDate(LocalDate.now())
                .setUrgency(Urgency.AVERAGE)
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        Mockito.when(ticketConverter.convertToDto(ticket, employee))
                .thenReturn(ticketDto);
        ticketService.getTicketDtoById(10L, employee);
        verify(ticketDao).getTicketById(10L);
        verify(ticketConverter).convertToDto(ticket, employee);
    }

    @Test
    public void testUpdateTicketFromTicketDto_WhenTicketUpdated() {
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setUserOwner(employee)
                .setName("test")
                .setState(TicketState.DRAFT)
                .setUrgency(Urgency.AVERAGE)
                .setDesiredResolutionDate(LocalDate.now())
                .build();
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setId(10L)
                .setState(TicketState.NEW)
                .setName("test new")
                .setDesiredDate(LocalDate.now())
                .setUrgency(Urgency.HIGH)
                .build();
        Ticket ticketFromDto = new Ticket.TicketBuilder()
                .setId(10L)
                .setName("test new")
                .setState(TicketState.NEW)
                .setUrgency(Urgency.HIGH)
                .setDesiredResolutionDate(LocalDate.now())
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Mockito.when(ticketConverter.convertToEntity(ticketDto)).thenReturn(ticketFromDto);
        ticketService.updateTicketFromTicketDto(ticketDto, employee, 10L);
        verify(ticketDao).getTicketById(10L);
        verify(historyService, times(2)).createHistory(eq(employee), any(Ticket.class),
                anyString(), anyString());
    }

    @Test(expected = TicketException.class)
    public void testUpdateTicketFromTicketDto_WhenTicketCouldNotUpdated() {
        Ticket ticket = new Ticket.TicketBuilder()
                .setId(10L)
                .setUserOwner(employee)
                .setName("test")
                .setState(TicketState.NEW)
                .setUrgency(Urgency.AVERAGE)
                .setDesiredResolutionDate(LocalDate.now())
                .build();
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setId(10L)
                .setState(TicketState.APPROVED)
                .setName("test new")
                .setDesiredDate(LocalDate.now())
                .setUrgency(Urgency.HIGH)
                .build();
        Ticket ticketFromDto = new Ticket.TicketBuilder()
                .setId(10L)
                .setName("test new")
                .setState(TicketState.APPROVED)
                .setUrgency(Urgency.HIGH)
                .setDesiredResolutionDate(LocalDate.now())
                .build();
        Mockito.when(ticketDao.getTicketById(10L))
                .thenReturn(Optional.of(ticket));
        Mockito.when(applicationContext.getBean(TicketService.class)).thenReturn(ticketService);
        Mockito.when(ticketConverter.convertToEntity(ticketDto)).thenReturn(ticketFromDto);
        ticketService.updateTicketFromTicketDto(ticketDto, employee, 10L);
    }
}