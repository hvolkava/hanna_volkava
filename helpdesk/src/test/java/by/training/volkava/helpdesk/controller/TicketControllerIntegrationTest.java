package by.training.volkava.helpdesk.controller;

import by.training.volkava.helpdesk.config.DataServiceConfig;
import by.training.volkava.helpdesk.config.HibernateConfig;
import by.training.volkava.helpdesk.config.MvcConfig;
import by.training.volkava.helpdesk.config.SecurityConfig;
import by.training.volkava.helpdesk.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {MvcConfig.class, DataServiceConfig.class,
        HibernateConfig.class, SecurityConfig.class})
public class TicketControllerIntegrationTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    private User user;
    private User manager;
    private User engineer;

    @Before
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity());
        this.mockMvc = builder.build();
        user = new User();
        user.setEmail("user1_mogilev@yopmail.com");
        user.setPassword("P@ssword1");
        manager = new User();
        manager.setEmail("manager1_mogilev@yopmail.com");
        manager.setPassword("P@ssword1");
        engineer = new User();
        engineer.setEmail("engineer1_mogilev@yopmail.com");
        engineer.setPassword("P@ssword1");
    }

    @Test
    public void testGetListTickets_ForUser1() throws Exception {

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/tickets")
                        .with(userHttpBasic(user));
        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print());
    }

    @Test
    public void testGetListTickets_ForManager1() throws Exception {
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/tickets")
                        .with(userHttpBasic(manager));
        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print());
    }

    @Test
    public void testGetListTickets_ForEngineer1() throws Exception {
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/tickets")
                        .with(userHttpBasic(engineer));
        this.mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print());
    }

    private RequestPostProcessor userHttpBasic(User user) {
        return SecurityMockMvcRequestPostProcessors
                .httpBasic(user.getEmail(), user.getPassword());
    }
}