package by.training.volkava.helpdesk.controller;

import by.training.volkava.helpdesk.config.DataServiceConfig;
import by.training.volkava.helpdesk.config.HibernateConfig;
import by.training.volkava.helpdesk.config.MvcConfig;
import by.training.volkava.helpdesk.config.SecurityConfig;
import by.training.volkava.helpdesk.config.SecurityInit;
import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.dto.TicketDtoList;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.exception.TicketException;
import by.training.volkava.helpdesk.handler.CustomGlobalExceptionHandler;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.TicketService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.AccessControlException;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MvcConfig.class, DataServiceConfig.class,
        HibernateConfig.class, SecurityConfig.class, SecurityInit.class})
@WebAppConfiguration
public class TicketControllerTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    FilterChainProxy springSecurityFilterChain;

    @Mock
    private TicketService ticketService;

    @InjectMocks
    private TicketController ticketController;

    private MockMvc mockMvc;
    private User employee;
    private User manager;
    private User engineer;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(ticketController)
                .setControllerAdvice(new CustomGlobalExceptionHandler())
                .apply(springSecurity(springSecurityFilterChain)).build();
        employee = new User();
        employee.setEmail("employee@test");employee.setRole(Role.EMPLOYEE);
        manager = new User();
        manager.setEmail("manager@test");manager.setRole(Role.MANAGER);
        engineer = new User();
        engineer.setEmail("manager@test");engineer.setRole(Role.ENGINEER);
    }

    @Test
    public void testGetListTickets_WhenServiceReturnTicketListDto() throws Exception {
        Mockito.when(ticketService.getAllTickets(employee)).thenReturn(new TicketDtoList(new ArrayList<>()));
        mockMvc.perform(get("/tickets").with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTicketById_WhenServiceReturnTicketDto() throws Exception {
        Mockito.when(ticketService.getTicketDtoById(1L, employee)).thenReturn(new TicketDto());
        mockMvc.perform(get("/tickets/{id}", 1L).with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTicketById_WhenServiceReturnAccessControlException() throws Exception {
        Mockito.when(ticketService.getTicketDtoById(100L, employee)).thenThrow(new AccessControlException("not allowed"));
        mockMvc.perform(get("/tickets/{id}", 100L).with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testGetTicketById_WhenServiceReturnTicketException() throws Exception {
        Mockito.when(ticketService.getTicketDtoById(100L, employee)).thenThrow(new TicketException("Ticket not found"));
        mockMvc.perform(get("/tickets/{id}", 100L).with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateTicket_WhenTicketDtoIsValid() throws Exception {
        Mockito.when(ticketService.createTicket(any(TicketDto.class), eq(employee))).thenReturn(10L);
        String ticketDtoJson = "{\"category\":{\"id\":1},\"name\":\"test\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(post("/tickets")
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isCreated());
    }

    @Test
    public void testCreateTicket_WhenTicketDtoIsNotValidNullCategory() throws Exception {
        Mockito.when(ticketService.createTicket(any(TicketDto.class), eq(employee))).thenReturn(10L);
        String ticketDtoJson = "{\"name\":\"test\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(post("/tickets")
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateTicket_WhenTicketDtoIsNotValidNameCirillic() throws Exception {
        Mockito.when(ticketService.createTicket(any(TicketDto.class), eq(employee))).thenReturn(10L);
        String ticketDtoJson = "{\"name\":\"ТЕСТЫ\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(post("/tickets")
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateTicket_WhenTicketDtoIsNotValidEmptyName() throws Exception {
        Mockito.when(ticketService.createTicket(any(TicketDto.class), eq(employee))).thenReturn(10L);
        String ticketDtoJson = "{\"name\":\"\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(post("/tickets")
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateTicket_WhenTicketDtoIsNotValidStateNotNewOrDraft() throws Exception {
        Mockito.when(ticketService.createTicket(any(TicketDto.class), eq(employee))).thenReturn(10L);
        String ticketDtoJson = "{\"name\":\"test\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(post("/tickets")
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTicket_WhenServiceReturnTicketException() throws Exception {
        Mockito.doThrow(new TicketException(""))
                .when(ticketService).updateTicketFromTicketDto(any(TicketDto.class), eq(employee), anyLong());
        String ticketDtoJson = "{\"category\":{\"id\":1},\"name\":\"test\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(put("/tickets/{id}", 100L)
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateTicket_WhenServiceUpdatedTicket() throws Exception {
        Mockito.doNothing()
                .when(ticketService).updateTicketFromTicketDto(any(TicketDto.class), eq(employee), anyLong());
        String ticketDtoJson = "{\"category\":{\"id\":1},\"name\":\"test\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Draft\"}";
        mockMvc.perform(put("/tickets/{id}", 100L)
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateTicket_WhenTicketDtoIsNotValidStateNotNewOrDraft() throws Exception {
        String ticketDtoJson = "{\"name\":\"test\",\"urgency\":\"CRITICAL\",\"desiredDate\":\"25/07/2025\",\"state\":\"Done\"}";
        mockMvc.perform(put("/tickets/{id}", 100L)
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testChangeStateInTicket_WhenTicketStateChanged() throws Exception {
        String ticketDtoJson = "{\"state\":\"Done\"}";
        Mockito.doNothing()
                .when(ticketService).changeState(anyLong(), eq(employee), eq(TicketState.DONE));
        mockMvc.perform(patch("/tickets/{id}", 100L)
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isOk());
    }

    @Test
    public void testChangeStateInTicket_WhenServiceThrowTicketException() throws Exception {
        String ticketDtoJson = "{\"state\":\"Done\"}";
        Mockito.doThrow(new TicketException(""))
                .when(ticketService).changeState(anyLong(), eq(employee), eq(TicketState.DONE));
        mockMvc.perform(patch("/tickets/{id}", 100L)
                .content(ticketDtoJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetListTicketsUrgencies() throws Exception {
        mockMvc.perform(get("/tickets/urgencies")
                .with(user(new CustomUserPrincipal(employee))))
                .andExpect(status().isOk());
    }
}