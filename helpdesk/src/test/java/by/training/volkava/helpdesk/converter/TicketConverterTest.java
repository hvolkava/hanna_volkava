package by.training.volkava.helpdesk.converter;

import by.training.volkava.helpdesk.dto.CategoryDto;
import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.enums.Urgency;
import by.training.volkava.helpdesk.model.Category;
import by.training.volkava.helpdesk.model.Feedback;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class TicketConverterTest {

    @Mock
    private CategoryConverter categoryConverter;

    @InjectMocks
    private TicketConverter ticketConverter;

    private User employee;
    private User manager;
    private User engineer;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        employee = new User();
        employee.setId(1L);
        employee.setEmail("employee@test");employee.setRole(Role.EMPLOYEE);
        manager = new User();
        manager.setId(2L);
        manager.setEmail("manager@test");manager.setRole(Role.MANAGER);
        engineer = new User();
        employee.setId(3L);
        engineer.setEmail("manager@test");engineer.setRole(Role.ENGINEER);
    }

    @Test
    public void testConvertToDto_WhenTicketInStateDraftForEmployee() {
        Category category = new Category();
        Ticket ticket = new Ticket.TicketBuilder()
                .setUserOwner(employee)
                .setState(TicketState.DRAFT)
                .setCategory(category)
                .setId(1L)
                .build();
        Mockito.when(categoryConverter.convertToDto(category)).thenReturn(new CategoryDto());
        TicketDto ticketDto = ticketConverter.convertToDto(ticket, employee);
        Assert.assertTrue(ticketDto.getIsMineTicket());
        Assert.assertTrue(ticketDto.hasLink("editTicket"));
        Assert.assertFalse(ticketDto.hasLink("leaveFeedback"));
        Assert.assertFalse(ticketDto.hasLink("viewFeedback"));
    }

    @Test
    public void testConvertToDto_WhenTicketInStateDoneAndHasNotFeedbackForEmployee() {
        Category category = new Category();
        Ticket ticket = new Ticket.TicketBuilder()
                .setUserOwner(employee)
                .setState(TicketState.DONE)
                .setCategory(category)
                .setId(1L)
                .build();
        Mockito.when(categoryConverter.convertToDto(category)).thenReturn(new CategoryDto());
        TicketDto ticketDto = ticketConverter.convertToDto(ticket, employee);
        Assert.assertTrue(ticketDto.getIsMineTicket());
        Assert.assertFalse(ticketDto.hasLink("editTicket"));
        Assert.assertTrue(ticketDto.hasLink("leaveFeedback"));
        Assert.assertFalse(ticketDto.hasLink("viewFeedback"));
    }

    @Test
    public void testConvertToDto_WhenTicketInStateNewAndHasNotUserApproverForManager() {
        Category category = new Category();
        Ticket ticket = new Ticket.TicketBuilder()
                .setUserOwner(employee)
                .setState(TicketState.NEW)
                .setCategory(category)
                .setId(1L)
                .build();
        Mockito.when(categoryConverter.convertToDto(category)).thenReturn(new CategoryDto());
        TicketDto ticketDto = ticketConverter.convertToDto(ticket, manager);
        Assert.assertFalse(ticketDto.getIsMineTicket());
        Assert.assertFalse(ticketDto.hasLink("editTicket"));
        Assert.assertFalse(ticketDto.hasLink("leaveFeedback"));
        Assert.assertFalse(ticketDto.hasLink("viewFeedback"));
    }

    @Test
    public void testConvertToDto_WhenTicketInStateApprovedAndHasUserApproverForManager() {
        Category category = new Category();
        Ticket ticket = new Ticket.TicketBuilder()
                .setUserOwner(employee)
                .setState(TicketState.APPROVED)
                .setCategory(category)
                .setUserApprover(manager)
                .setId(1L)
                .build();
        Mockito.when(categoryConverter.convertToDto(category)).thenReturn(new CategoryDto());
        TicketDto ticketDto = ticketConverter.convertToDto(ticket, manager);
        Assert.assertTrue(ticketDto.getIsMineTicket());
        Assert.assertFalse(ticketDto.hasLink("editTicket"));
        Assert.assertFalse(ticketDto.hasLink("leaveFeedback"));
        Assert.assertFalse(ticketDto.hasLink("viewFeedback"));
    }

    @Test
    public void testConvertToDto_WhenTicketInStateDoneAndHasFeedbackForEngineer() {
        Category category = new Category();
        Ticket ticket = new Ticket.TicketBuilder()
                .setUserOwner(employee)
                .setState(TicketState.DONE)
                .setCategory(category)
                .setId(1L)
                .setFeedback(new Feedback())
                .setUserAssignee(engineer)
                .build();
        Mockito.when(categoryConverter.convertToDto(category)).thenReturn(new CategoryDto());
        TicketDto ticketDto = ticketConverter.convertToDto(ticket, engineer);
        Assert.assertTrue(ticketDto.getIsMineTicket());
        Assert.assertFalse(ticketDto.hasLink("editTicket"));
        Assert.assertFalse(ticketDto.hasLink("leaveFeedback"));
        Assert.assertTrue(ticketDto.hasLink("viewFeedback"));
    }

    @Test
    public void testConvertToDto_WhenTicketInStateDoneAndHasNotFeedbackForEngineer() {
        Category category = new Category();
        Ticket ticket = new Ticket.TicketBuilder()
                .setUserOwner(employee)
                .setState(TicketState.DONE)
                .setCategory(category)
                .setId(1L)
                .setUserAssignee(engineer)
                .build();
        Mockito.when(categoryConverter.convertToDto(category)).thenReturn(new CategoryDto());
        TicketDto ticketDto = ticketConverter.convertToDto(ticket, engineer);
        Assert.assertTrue(ticketDto.getIsMineTicket());
        Assert.assertFalse(ticketDto.hasLink("editTicket"));
        Assert.assertFalse(ticketDto.hasLink("leaveFeedback"));
        Assert.assertFalse(ticketDto.hasLink("viewFeedback"));
    }

    @Test
    public void testConvertToEntity() {
        CategoryDto categoryDto = new CategoryDto(1, "test category");
        Category category = new Category(1, "test category");
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setState(TicketState.DRAFT)
                .setName("test")
                .setDesiredDate(LocalDate.now())
                .setUrgency(Urgency.AVERAGE)
                .setCategory(categoryDto)
                .build();
        Ticket expected = new Ticket.TicketBuilder()
                .setState(TicketState.DRAFT)
                .setName("test")
                .setDesiredResolutionDate(LocalDate.now())
                .setUrgency(Urgency.AVERAGE)
                .setCategory(category)
                .build();
        Mockito.when(categoryConverter.convertToEntity(categoryDto)).thenReturn(category);
        Assert.assertEquals(expected, ticketConverter.convertToEntity(ticketDto));
    }
}