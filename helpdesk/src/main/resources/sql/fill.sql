INSERT INTO User(first_name, last_name, role_id, email, password) VALUES('Employee 1 first name', 'Employee 1 last name', 'EMPLOYEE', 'user1_mogilev@yopmail.com', '$2a$10$o9QtR3nz8t6c7xrS/lBaF.WvG1v0U3d7wb0zupBX.3t4t6PeH6v3e');
INSERT INTO User(first_name, last_name, role_id, email, password) VALUES('Employee 2 first name', 'Employee 2 last name', 'EMPLOYEE', 'user2_mogilev@yopmail.com', '$2a$10$o9QtR3nz8t6c7xrS/lBaF.WvG1v0U3d7wb0zupBX.3t4t6PeH6v3e');
INSERT INTO User(first_name, last_name, role_id, email, password) VALUES('Manager 1 first name', 'Manager 1 last name', 'MANAGER', 'manager1_mogilev@yopmail.com', '$2a$10$o9QtR3nz8t6c7xrS/lBaF.WvG1v0U3d7wb0zupBX.3t4t6PeH6v3e');
INSERT INTO User(first_name, last_name, role_id, email, password) VALUES('Manager 2 first name', 'Manager 2 last name', 'MANAGER', 'manager2_mogilev@yopmail.com', '$2a$10$o9QtR3nz8t6c7xrS/lBaF.WvG1v0U3d7wb0zupBX.3t4t6PeH6v3e');
INSERT INTO User(first_name, last_name, role_id, email, password) VALUES('Enginer 1 first name', 'Enginer 1 last name','ENGINEER', 'engineer1_mogilev@yopmail.com', '$2a$10$o9QtR3nz8t6c7xrS/lBaF.WvG1v0U3d7wb0zupBX.3t4t6PeH6v3e');
INSERT INTO User(first_name, last_name, role_id, email, password) VALUES('Enginer 2 first name', 'Enginer 2 last name', 'ENGINEER', 'engineer2_mogilev@yopmail.com', '$2a$10$o9QtR3nz8t6c7xrS/lBaF.WvG1v0U3d7wb0zupBX.3t4t6PeH6v3e');

INSERT INTO Category(name) VALUES ('Application & Services');
INSERT INTO Category(name) VALUES ('Benefits & Paper Work');
INSERT INTO Category(name) VALUES ('Hardware & Software');
INSERT INTO Category(name) VALUES ('People Management');
INSERT INTO Category(name) VALUES ('Security & Access');
INSERT INTO Category(name) VALUES ('Workplaces & Facilities');

INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t1 u1', '', '2019-06-10', '2019-06-14', null, 1, 'DRAFT', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t2 u1', '', '2019-06-10', '2019-06-14', null, 1, 'NEW', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t3 u1', '', '2019-06-10', '2019-06-14', null, 1, 'APPROVED', 1, 2, 3 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t4 u1', '', '2019-06-10', '2019-06-14', null, 1, 'DECLINED', 1, 1, 3 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t5 u1', '', '2019-06-10', '2019-06-14', 5, 1, 'IN_PROGRESS', 1, 1, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t6 u1', '', '2019-06-10', '2019-06-14', 5, 1, 'DONE', 1, 3, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t7 u1', '', '2019-06-10', '2019-06-14', null, 1, 'CANCELLED', 1, 0, 3 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t8 u2', '', '2019-06-10', '2019-06-14', null, 2, 'DRAFT', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t9 u2', '', '2019-06-10', '2019-06-14', null, 2, 'NEW', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t10 u2', '', '2019-06-10', '2019-06-14', null, 2, 'APPROVED', 1, 0, 4 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t11 u2', '', '2019-06-10', '2019-06-14', null, 2, 'DECLINED', 1, 0, 4 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t12 u2', '', '2019-06-10', '2019-06-14', 5, 2, 'IN_PROGRESS', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t13 u2', '', '2019-06-10', '2019-06-14', 5, 2, 'DONE', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t14 u2', '', '2019-06-10', '2019-06-14', 5, 2, 'CANCELLED', 1, 0, 4 );

INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t15 m1', '', '2019-06-10', '2019-06-14', null, 3, 'DRAFT', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t16 m1', '', '2019-06-10', '2019-06-14', null, 3, 'NEW', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t17 m1', '', '2019-06-10', '2019-06-14', null, 3, 'APPROVED', 1, 2, 4 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t18 m1', '', '2019-06-10', '2019-06-14', null, 3, 'DECLINED', 1, 1, 4 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t19 m1', '', '2019-06-10', '2019-06-14', 5, 3, 'IN_PROGRESS', 1, 1, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t20 m1', '', '2019-06-10', '2019-06-14', 5, 3, 'DONE', 1, 3, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t21 m1', '', '2019-06-10', '2019-06-14', 5, 3, 'CANCELLED', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t22 m2', '', '2019-06-10', '2019-06-14', null, 4, 'DRAFT', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t23 m2', '', '2019-06-10', '2019-06-14', null, 4, 'NEW', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t24 m2', '', '2019-06-10', '2019-06-14', null, 4, 'APPROVED', 1, 0, 3 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t25 m2', '', '2019-06-10', '2019-06-14', null, 4, 'DECLINED', 1, 0, 3 );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t26 m2', '', '2019-06-10', '2019-06-14', 6, 4, 'IN_PROGRESS', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t27 m2', '', '2019-06-10', '2019-06-14', 6, 3, 'DONE', 1, 0, null );
INSERT INTO Ticket(name, description, created_on, desire_resolution_date, assignee_id, owner_id , state_id, category_id, urgency_id, approver_id) VALUES ('test t28 m2', '', '2019-06-10', '2019-06-14', 6, 4, 'CANCELLED', 1, 0, 3 );

INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (4, 'Ticket created', '2019-07-26 11:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (4, 'Ticket edited', '2019-07-26 12:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (3, 'Ticket approved', '2019-08-11 13:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (5, 'Ticket in process', '2019-07-26 14:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (4, 'Ticket created', '2019-07-26 15:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (4, 'Ticket edited', '2019-07-26 6:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (3, 'Ticket approved', '2019-07-26 17:16:57', 2);
INSERT INTO Comment (user_id, text, date, ticket_id) VALUES (5, 'Ticket in process', '2019-07-26 18:16:57', 2);


INSERT INTO History (ticket_id, date, action, user_id, description) VALUES (2, '2019-07-26 11:16:57', 'Action 1', 1, 'Description 1');
INSERT INTO History (ticket_id, date, action, user_id, description) VALUES (2, '2019-07-26 11:16:57', 'Action 2', 2, 'Description 2');
INSERT INTO History (ticket_id, date, action, user_id, description) VALUES (2, '2019-07-26 11:16:57', 'Action 3', 3, 'Description 3');
INSERT INTO History (ticket_id, date, action, user_id, description) VALUES (2, '2019-07-26 11:16:57', 'Action 1', 4, 'Description 4');


