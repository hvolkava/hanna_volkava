package by.training.volkava.helpdesk.exception;

public class TicketException extends RuntimeException {
    public TicketException(String message) {
        super(message);
    }
}
