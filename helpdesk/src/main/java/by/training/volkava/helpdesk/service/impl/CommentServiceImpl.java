package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.CommentConverter;
import by.training.volkava.helpdesk.dao.CommentDao;
import by.training.volkava.helpdesk.dto.CommentDto;
import by.training.volkava.helpdesk.model.Comment;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.CommentService;
import by.training.volkava.helpdesk.service.TicketService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

@Service
public class CommentServiceImpl implements CommentService {
    private CommentDao commentDao;
    private CommentConverter commentConverter;
    private TicketService ticketService;

    public CommentServiceImpl(CommentDao commentDao,
                              CommentConverter commentConverter,
                              TicketService ticketService) {
        this.commentDao = commentDao;
        this.commentConverter = commentConverter;
        this.ticketService = ticketService;
    }

    @Transactional
    @Override
    public Long createComment(Long ticketId, CommentDto commentDto, User currentUser) {
        Comment comment = commentConverter.convertToEntity(commentDto);
        comment.setUser(currentUser);
        LocalDateTime date = LocalDateTime.now();
        comment.setDate(date);
        comment.setTicket(ticketService.getTicketById(ticketId, currentUser));
        commentDao.save(comment);
        return comment.getId();
    }

    @Transactional
    public List<CommentDto> getAllCommentsByTicketId(Long ticketId, User currentUser) {
        Ticket ticket = ticketService.getTicketById(ticketId, currentUser);
        return commentDao.getAllCommentsByTicket(ticket)
                .stream()
                .map(comment -> commentConverter.convertToDto(comment))
                .collect(Collectors.toList());
    }
}
