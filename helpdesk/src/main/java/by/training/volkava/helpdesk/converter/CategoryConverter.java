package by.training.volkava.helpdesk.converter;

import by.training.volkava.helpdesk.dto.CategoryDto;
import by.training.volkava.helpdesk.model.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter {
    public CategoryDto convertToDto(Category category) {
        return new CategoryDto(category.getId(), category.getName());
    }

    public Category convertToEntity(CategoryDto categoryDto) {
        return new Category(categoryDto.getId(), categoryDto.getName());
    }
}
