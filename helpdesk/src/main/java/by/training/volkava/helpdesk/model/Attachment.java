package by.training.volkava.helpdesk.model;

import java.util.StringJoiner;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Lob
    @Column
    private byte[] blob;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    public Attachment() {
    }

    public Long getId() {
        return id;
    }

    public Attachment setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Attachment setName(String name) {
        this.name = name;
        return this;
    }

    public byte[] getBlob() {
        return blob;
    }

    public Attachment setBlob(byte[] blob) {
        this.blob = blob;
        return this;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Attachment setTicket(Ticket ticket) {
        this.ticket = ticket;
        return this;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ",
                Attachment.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("blob={Blob}")
                .add("ticket=" + ticket)
                .toString();
    }
}
