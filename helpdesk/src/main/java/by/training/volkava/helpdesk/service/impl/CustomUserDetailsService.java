package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.dao.UserDao;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;
import by.training.volkava.helpdesk.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    private UserDao userDao;

    public CustomUserDetailsService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDao.getUserByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(email));
        return new CustomUserPrincipal(user);
    }
}