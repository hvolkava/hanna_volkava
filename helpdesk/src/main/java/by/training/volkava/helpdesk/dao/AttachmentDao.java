package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.model.Attachment;
import by.training.volkava.helpdesk.model.Ticket;
import org.springframework.core.io.Resource;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public interface AttachmentDao {
    void createAttachment(Attachment attachment);

    List<Attachment> getAllAttachmentByTicket(Ticket ticket);

    Optional<Attachment> getAttachmentByAttachId(Long attachmentId);

    void deleteAttachment(Attachment attachment);
}
