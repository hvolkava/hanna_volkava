package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.TicketDao;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class TicketDaoImpl implements TicketDao {
    private static final Logger LOG = LoggerFactory.getLogger(TicketDaoImpl.class.getName());
    private static final String SELECT_ALL_TICKETS_HQL_FOR_EMPLOYEE =
            "from Ticket where userOwner = :user";
    private static final String SELECT_ALL_TICKETS_HQL_FOR_MANAGER =
            "from Ticket t where t.userOwner = :user or "
            + "(userOwner.role = 'EMPLOYEE' and state = 'NEW') or "
            + "(userOwner.role = 'MANAGER' and state = 'NEW' and userOwner != :user) or "
            + "(t.userApprover = :user and t.state in ( 'APPROVED', 'CANCELLED', 'DECLINED', "
            + "'DONE', 'IN_PROGRESS'))";

    private static final String SELECT_ALL_TICKETS_HQL_FOR_ENGINEER =
            "from Ticket where "
            + "(userOwner.role in ('EMPLOYEE', 'MANAGER') and state = 'APPROVED') or "
            + "(userAssignee = :user and state in ('IN_PROGRESS', 'DONE'))";

    private Map<Role, String> getAllMap = new EnumMap<>(Role.class);

    {
        getAllMap.put(Role.EMPLOYEE, SELECT_ALL_TICKETS_HQL_FOR_EMPLOYEE);
        getAllMap.put(Role.MANAGER, SELECT_ALL_TICKETS_HQL_FOR_MANAGER);
        getAllMap.put(Role.ENGINEER, SELECT_ALL_TICKETS_HQL_FOR_ENGINEER);
    }

    private SessionFactory sessionFactory;

    public TicketDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Ticket> getAllTickets(User currentUser) {
        final String Select_All = getAllMap.get(currentUser.getRole());
        return sessionFactory.getCurrentSession()
                .createQuery(Select_All)
                .setParameter("user", currentUser)
                .list();
    }

    public Optional<Ticket> getTicketById(Long id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Ticket.class, id));
    }

    @Override
    public void saveTicket(Ticket ticket) {
        sessionFactory.getCurrentSession().save(ticket);
    }

    @Override
    public void updateTicket(Ticket ticket) {
        sessionFactory.getCurrentSession().update(ticket);
    }
}
