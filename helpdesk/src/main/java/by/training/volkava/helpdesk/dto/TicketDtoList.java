package by.training.volkava.helpdesk.dto;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class TicketDtoList extends ResourceSupport {
    private List<TicketDto> tickets;

    public TicketDtoList() {
    }

    public TicketDtoList(List<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public List<TicketDto> getTickets() {
        return tickets;
    }

    public TicketDtoList setTickets(List<TicketDto> tickets) {
        this.tickets = tickets;
        return this;
    }
}
