package by.training.volkava.helpdesk.model;

import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.enums.Urgency;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;


@Entity
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column(name = "created_on")
    private LocalDate createdOnDate;

    @Column(name = "desire_resolution_date")
    private LocalDate desiredResolutionDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "assignee_id")
    private User userAssignee;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private User userOwner;

    @Enumerated(EnumType.STRING)
    @Column(name = "state_id")
    private TicketState state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "urgency_id")
    private Urgency urgency;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "approver_id")
    private User userApprover;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "ticket")
    private Feedback feedback;

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public Ticket() {
    }

    public Ticket(TicketBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.createdOnDate = builder.createdOnDate;
        this.desiredResolutionDate = builder.desiredResolutionDate;
        this.userAssignee = builder.userAssignee;
        this.userOwner = builder.userOwner;
        this.state = builder.state;
        this.category = builder.category;
        this.urgency = builder.urgency;
        this.userApprover = builder.userApprover;
        this.feedback = builder.feedback;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreatedOnDate() {
        return createdOnDate;
    }

    public void setCreatedOnDate(LocalDate createdOn) {
        this.createdOnDate = createdOn;
    }

    public LocalDate getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(LocalDate desiredResclutionDate) {
        this.desiredResolutionDate = desiredResclutionDate;
    }

    public User getUserAssignee() {
        return userAssignee;
    }

    public void setUserAssignee(User userAssignee) {
        this.userAssignee = userAssignee;
    }

    public User getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;
    }

    public TicketState getState() {
        return state;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public User getUserApprover() {
        return userApprover;
    }

    public void setUserApprover(User userApprover) {
        this.userApprover = userApprover;
    }

    public static class TicketBuilder {

        private Long id;
        private String name;
        private String description;
        private LocalDate createdOnDate;
        private LocalDate desiredResolutionDate;
        private User userAssignee;
        private User userOwner;
        private TicketState state;
        private Category category;
        private Urgency urgency;
        private User userApprover;
        private Feedback feedback;

        public TicketBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public TicketBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public TicketBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public TicketBuilder setCreatedOnDate(LocalDate createdOnDate) {
            this.createdOnDate = createdOnDate;
            return this;
        }

        public TicketBuilder setDesiredResolutionDate(LocalDate desiredResolutionDate) {
            this.desiredResolutionDate = desiredResolutionDate;
            return this;
        }

        public TicketBuilder setUserAssignee(User userAssignee) {
            this.userAssignee = userAssignee;
            return this;
        }

        public TicketBuilder setUserOwner(User userOwner) {
            this.userOwner = userOwner;
            return this;
        }

        public TicketBuilder setState(TicketState state) {
            this.state = state;
            return this;
        }

        public TicketBuilder setCategory(Category category) {
            this.category = category;
            return this;
        }

        public TicketBuilder setUrgency(Urgency urgency) {
            this.urgency = urgency;
            return this;
        }

        public TicketBuilder setUserApprover(User userApprover) {
            this.userApprover = userApprover;
            return this;
        }

        public TicketBuilder setFeedback(Feedback feedback) {
            this.feedback = feedback;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ticket)) {
            return false;
        }

        Ticket ticket = (Ticket) obj;

        if (getId() != null ? !getId().equals(ticket.getId()) : ticket.getId() != null) return false;
        if (getName() != null ? !getName().equals(ticket.getName()) : ticket.getName() != null) return false;
        if (getDescription() != null ? !getDescription().equals(ticket.getDescription()) : ticket.getDescription() != null)
            return false;
        if (getCreatedOnDate() != null ? !getCreatedOnDate().equals(ticket.getCreatedOnDate()) : ticket.getCreatedOnDate() != null)
            return false;
        if (getDesiredResolutionDate() != null ? !getDesiredResolutionDate().equals(ticket.getDesiredResolutionDate()) : ticket.getDesiredResolutionDate() != null)
            return false;
        if (getUserAssignee() != null ? !getUserAssignee().equals(ticket.getUserAssignee()) : ticket.getUserAssignee() != null)
            return false;
        if (getUserOwner() != null ? !getUserOwner().equals(ticket.getUserOwner()) : ticket.getUserOwner() != null)
            return false;
        if (getState() != ticket.getState()) return false;
        if (getCategory() != null ? !getCategory().equals(ticket.getCategory()) : ticket.getCategory() != null)
            return false;
        if (getUrgency() != ticket.getUrgency()) return false;
        if (getUserApprover() != null ? !getUserApprover().equals(ticket.getUserApprover()) : ticket.getUserApprover() != null)
            return false;
        return getFeedback() != null ? getFeedback().equals(ticket.getFeedback()) : ticket.getFeedback() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getCreatedOnDate() != null ? getCreatedOnDate().hashCode() : 0);
        result = 31 * result + (getDesiredResolutionDate() != null ? getDesiredResolutionDate().hashCode() : 0);
        result = 31 * result + (getUserAssignee() != null ? getUserAssignee().hashCode() : 0);
        result = 31 * result + (getUserOwner() != null ? getUserOwner().hashCode() : 0);
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (getCategory() != null ? getCategory().hashCode() : 0);
        result = 31 * result + (getUrgency() != null ? getUrgency().hashCode() : 0);
        result = 31 * result + (getUserApprover() != null ? getUserApprover().hashCode() : 0);
        result = 31 * result + (getFeedback() != null ? getFeedback().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ticket.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("createdOn=" + createdOnDate)
                .add("desiredResolutionDate=" + desiredResolutionDate)
                .add("userAssignee=" + userAssignee)
                .add("userOwner=" + userOwner)
                .add("state=" + state)
                .add("category=" + category)
                .add("urgency=" + urgency)
                .add("userApprover=" + userApprover)
                .toString();
    }
}
