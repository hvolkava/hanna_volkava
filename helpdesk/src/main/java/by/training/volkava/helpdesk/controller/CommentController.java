package by.training.volkava.helpdesk.controller;

import by.training.volkava.helpdesk.dto.CommentDto;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/tickets/{ticketId}/comments")
public class CommentController {
    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping
    public ResponseEntity<List<CommentDto>> getAllCommentsByTicketId(
            @PathVariable(value = "ticketId") Long ticketId,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        return ResponseEntity.ok(commentService
                .getAllCommentsByTicketId(ticketId, currentUser));
    }

    @PostMapping
    public ResponseEntity<CommentDto> createComment(@PathVariable(value = "ticketId") Long ticketId,
                                                    @Valid @RequestBody CommentDto commentDto,
                                                    Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        final URI uri =
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(commentService
                                .createComment(ticketId, commentDto, currentUser))
                        .toUri();
        return ResponseEntity.created(uri).build();
    }
}
