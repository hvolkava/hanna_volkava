package by.training.volkava.helpdesk.converter;

import by.training.volkava.helpdesk.dto.CommentDto;
import by.training.volkava.helpdesk.model.Comment;
import org.springframework.stereotype.Component;

@Component
public class CommentConverter {
    public Comment convertToEntity(CommentDto ticketDto) {
        Comment comment = new Comment();
        comment.setText(ticketDto.getComment());
        return comment;
    }

    public CommentDto convertToDto(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setComment(comment.getText());
        commentDto.setDateTime(comment.getDate());
        commentDto.setUsername(comment.getUser().getFullName());
        return commentDto;
    }
}
