package by.training.volkava.helpdesk.dto;

public class FeedbackDto {
    private Byte rate;
    private String text;

    public FeedbackDto() {
    }

    public FeedbackDto(Byte rate, String text) {
        this.rate = rate;
        this.text = text;
    }

    public Byte getRate() {
        return rate;
    }

    public void setRate(Byte rate) {
        this.rate = rate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
