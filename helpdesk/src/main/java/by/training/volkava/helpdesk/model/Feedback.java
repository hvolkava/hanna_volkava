package by.training.volkava.helpdesk.model;

import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private Byte rate;

    @Basic
    private LocalDateTime date;

    @Column
    private String text;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    public Feedback() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Byte getRate() {
        return rate;
    }

    public void setRate(Byte rate) {
        this.rate = rate;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Feedback(FeedbackBuilder builder) {
        this.id = builder.id;
        this.user = builder.user;
        this.rate = builder.rate;
        this.date = builder.date;
        this.text = builder.text;
        this.ticket = builder.ticket;
    }

    public static class FeedbackBuilder {
        private Long id;
        private User user;
        private Byte rate;
        private LocalDateTime date;
        private String text;
        private Ticket ticket;

        public FeedbackBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public FeedbackBuilder setUser(User user) {
            this.user = user;
            return this;
        }

        public FeedbackBuilder setRate(Byte rate) {
            this.rate = rate;
            return this;
        }

        public FeedbackBuilder setDate(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public FeedbackBuilder setText(String text) {
            this.text = text;
            return this;
        }

        public FeedbackBuilder setTicket(Ticket ticket) {
            this.ticket = ticket;
            return this;
        }

        public Feedback build() {
            return new Feedback(this);
        }
    }
}
