package by.training.volkava.helpdesk.converter;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import by.training.volkava.helpdesk.controller.TicketController;
import by.training.volkava.helpdesk.dto.TicketDtoList;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TicketListConverter {

    private TicketConverter ticketConverter;

    public TicketListConverter(TicketConverter ticketConverter) {
        this.ticketConverter = ticketConverter;
    }

    public TicketDtoList convertToDto(List<Ticket> ticketList, User currentUser) {
        TicketDtoList ticketDtoList = new TicketDtoList(ticketList.stream()
                .map(ticket -> ticketConverter.convertToDto(ticket, currentUser))
                .collect(Collectors.toList()));
        addLinksForTicketDtoList(ticketDtoList, currentUser);
        return ticketDtoList;
    }

    private void addLinksForTicketDtoList(TicketDtoList ticketDtoList, User currentUser) {
        if (currentUser.getRole() == Role.MANAGER
                || currentUser.getRole() == Role.EMPLOYEE) {
            ticketDtoList.add(linkTo(TicketController.class).withRel("createTicket"));
        }
        ticketDtoList.getTickets().forEach(
                (ticketDto) -> ticketDto.add(linkTo(TicketController.class)
                        .slash(ticketDto.getTicketId())
                        .withSelfRel()));
    }
}
