package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.FeedbackDao;
import by.training.volkava.helpdesk.model.Feedback;
import by.training.volkava.helpdesk.model.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class FeedbackDaoImpl implements FeedbackDao {
    private SessionFactory sessionFactory;
    private static final String SELECT_FEEDBACK_BY_TICKET = "from Feedback where ticket = :ticket";

    public FeedbackDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Feedback> getFeedbackByTicket(Ticket ticket) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_FEEDBACK_BY_TICKET)
                .setParameter("ticket", ticket)
                .uniqueResultOptional();
    }

    public void save(Feedback feedback) {
        sessionFactory.getCurrentSession().save(feedback);
    }
}
