package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.TicketConverter;
import by.training.volkava.helpdesk.converter.TicketListConverter;
import by.training.volkava.helpdesk.dao.TicketDao;
import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.dto.TicketDtoList;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.exception.TicketException;
import by.training.volkava.helpdesk.model.History;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.EmailService;
import by.training.volkava.helpdesk.service.HistoryService;
import by.training.volkava.helpdesk.service.TicketService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.security.AccessControlException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import javax.transaction.Transactional;

@Service
public class TicketServiceImpl implements TicketService {
    private ApplicationContext applicationContext;
    private static final Logger LOG = LoggerFactory.getLogger(TicketServiceImpl.class.getName());
    private TicketDao ticketDao;
    private TicketConverter ticketConverter;
    private EmailService emailService;
    private HistoryService historyService;
    private TicketListConverter ticketListConverter;

    private Map<TicketState,
            BiConsumer<Ticket, User>> consumerMap = new EnumMap<>(TicketState.class);

    {
        consumerMap.put(TicketState.APPROVED, Ticket::setUserApprover);
        consumerMap.put(TicketState.DECLINED, Ticket::setUserApprover);
        consumerMap.put(TicketState.IN_PROGRESS, Ticket::setUserAssignee);
        consumerMap.put(TicketState.CANCELLED, (ticket, user) -> {
            if (ticket.getState() == TicketState.NEW && user.getRole() == Role.MANAGER) {
                ticket.setUserApprover(user);
            }
            if (user.getRole() == Role.ENGINEER) {
                ticket.setUserAssignee(user);
            }
        });
    }

    public TicketServiceImpl(TicketDao ticketDao,
                             TicketConverter ticketConverter,
                             EmailService emailService,
                             @Lazy HistoryService historyService,
                             @Lazy TicketListConverter ticketListConverter) {
        this.ticketDao = ticketDao;
        this.ticketConverter = ticketConverter;
        this.emailService = emailService;
        this.historyService = historyService;
        this.ticketListConverter = ticketListConverter;
    }

    @Override
    @Transactional
    public TicketDtoList getAllTickets(User currentUser) {
        List<Ticket> list = ticketDao.getAllTickets(currentUser);
        return ticketListConverter.convertToDto(list, currentUser);
    }

    @Override
    @Transactional
    public Ticket getTicketById(Long id, User currentUser) {
        Ticket ticket = ticketDao.getTicketById(id)
                .orElseThrow(() -> {
                    LOG.error("Trying to get ticket id {}  by user {}",
                            id, currentUser);
                    return new TicketException("Trying to get ticket id " + id);
                });
        checkAvalableForUser(ticket, currentUser);
        return ticket;
    }

    @Override
    @Transactional
    @PreAuthorize("hasAnyRole('EMPLOYEE','MANAGER')")
    public Long createTicket(TicketDto ticketDto, User user) {
        Ticket ticket = ticketConverter.convertToEntity(ticketDto);
        ticket.setUserOwner(user);
        ticket.setCreatedOnDate(LocalDate.now());
        ticketDao.saveTicket(ticket);
        if (ticket.getState() == TicketState.NEW) {
            emailService.notifyUsers(ticket, TicketState.NEW);
        }
        History history = historyService.createHistory(user, ticket, "Ticket is created", null );
        historyService.saveHistory(history);
        return ticket.getId();
    }


    @Override
    public void changeState(Long ticketId, User user, TicketState nextState) {
        Ticket ticket =  applicationContext.getBean(TicketService.class).getTicketById(ticketId, user);
        checkDoActionByUser(ticket, user, nextState);
        BiConsumer<Ticket, User> biConsumer = consumerMap.get(nextState);
        if (Objects.nonNull(biConsumer)) {
            biConsumer.accept(ticket, user);
        }
        TicketState oldState = ticket.getState();
        applicationContext.getBean(TicketService.class).changeStateInTicket(ticket, nextState);
        History history = historyService.createHistory(user, ticket, "Ticket Status is changed",
                " from " + oldState + " to " + nextState );
        historyService.saveHistory(history);
        emailService.notifyUsers(ticket, oldState);
    }

    @Override
    @Transactional
    public void changeStateInTicket(Ticket ticket, TicketState nextState) {
        ticket.setState(nextState);
        ticketDao.updateTicket(ticket);
    }

    @Override
    @Transactional
    public TicketDto getTicketDtoById(Long ticketId, User currentUser) {
        Ticket ticket = getTicketById(ticketId, currentUser);
        return ticketConverter.convertToDto(ticket, currentUser);
    }

    @Override
    public void updateTicketFromTicketDto(TicketDto ticketDto, User currentUser, Long ticketId) {
        Ticket ticket = applicationContext.getBean(TicketService.class).getTicketById(ticketId, currentUser);
        Ticket ticketFromDto = ticketConverter.convertToEntity(ticketDto);
        checkAvailableForUpdateTicket(ticket, ticketFromDto);
        TicketState oldState = ticket.getState();
        updateTicketFromDto(ticket, ticketFromDto);
        applicationContext.getBean(TicketService.class).updateTicket(ticket);
        History history = historyService.createHistory(currentUser, ticket, "Ticket is edited", "" );
        historyService.saveHistory(history);
        if (oldState != ticketFromDto.getState()) {
            historyService.createHistory(currentUser, ticket, "Ticket Status is changed",
                    " from " + oldState + " to " + ticket.getState() );
            emailService.notifyUsers(ticket, oldState);
        }
    }

    @Override
    @Transactional
    public void updateTicket(Ticket ticket) {
        ticketDao.updateTicket(ticket);
    }

    private void updateTicketFromDto(Ticket ticket, Ticket ticketFromDto) {
        ticket.setCategory(ticketFromDto.getCategory());
        ticket.setName(ticketFromDto.getName());
        ticket.setDescription(ticketFromDto.getDescription());
        ticket.setUrgency(ticketFromDto.getUrgency());
        ticket.setDesiredResolutionDate(ticketFromDto.getDesiredResolutionDate());
        ticket.setState(ticketFromDto.getState());
    }

    private boolean checkAvailableForUpdateTicket(Ticket ticket,
                                          Ticket ticketFromDto) {
        if (!Arrays.asList(TicketState.DRAFT, TicketState.NEW)
                .contains(ticketFromDto.getState())
                || ticket.getState() != TicketState.DRAFT ) {
            LOG.error("Trying to update ticket {} to {}",
                    ticket.getId(), ticketFromDto.toString());
            throw new TicketException("Trying to update ticket "
                    + ticket.getId() + " to " + ticketFromDto.toString());
        }
        return true;
    }

    private boolean checkAvalableForUser(Ticket ticket, User user) {
        boolean access = user.getRole().checkAvalableForUser(ticket, user);
        if (!access) {
            LOG.error("Trying to get ticket {}  by user {}",
                    ticket.getId(), user);
            throw new AccessControlException("Trying to get ticket "
                    + ticket.getId() + " by user " + user.toString());
        }
        return access;
    }

    private void checkDoActionByUser(Ticket ticket, User user, TicketState nextState) {
        ticket.getState().getActions(user, ticket.getUserOwner()).stream()
                .filter(action -> action.getNextState().equals(nextState))
                .findFirst()
                .orElseThrow(() -> {
                    LOG.error("Trying to change ticket {} to state {}  by user {}",
                            ticket, nextState, user);
                    return new AccessControlException("Trying to change ticket "
                        + ticket + " to state " + nextState + " by user" + user);});
    }

    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
