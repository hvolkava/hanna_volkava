package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;

import java.util.List;
import java.util.Optional;

public interface TicketDao {
    List<Ticket> getAllTickets(User currentUser);

    Optional<Ticket> getTicketById(Long id);

    void saveTicket(Ticket ticket);

    void updateTicket(Ticket ticket);
}