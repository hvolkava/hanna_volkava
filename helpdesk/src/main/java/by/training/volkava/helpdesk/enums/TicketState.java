package by.training.volkava.helpdesk.enums;

import by.training.volkava.helpdesk.model.User;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.List;

public enum TicketState {
    DRAFT("Draft") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            List<TicketActions> list = currentUser.getRole() != Role.ENGINEER
                    ? Arrays.asList(TicketActions.SUBMIT, TicketActions.CANCEL)
                    : Arrays.asList();
            if (currentUser.getRole() == Role.MANAGER) {
                return userOwner.equals(currentUser) ? list : Arrays.asList();
            }
            return list;
        }
    },
    NEW("New") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            List<TicketActions> list = currentUser.getRole() == Role.MANAGER
                    ? Arrays.asList(TicketActions.APPROVE, TicketActions.DECLINE, TicketActions.CANCEL)
                    : Arrays.asList();
            if (currentUser.getRole() == Role.MANAGER) {
                return userOwner.equals(currentUser) ? Arrays.asList() : list;
            }
            return list;
        }
    },
    APPROVED("Approved") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            return currentUser.getRole() == Role.ENGINEER
                    ? Arrays.asList(TicketActions.ASSIGN_TO_ME, TicketActions.CANCEL)
                    : Arrays.asList();
        }
    },
    DECLINED("Declined") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            List<TicketActions> list = currentUser.getRole() != Role.ENGINEER
                    ? Arrays.asList(TicketActions.SUBMIT, TicketActions.CANCEL)
                    : Arrays.asList();
            if (currentUser.getRole() == Role.MANAGER) {
                return userOwner.equals(currentUser) ? list : Arrays.asList();
            }
            return list;
        }
    },
    IN_PROGRESS("In progress") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            return currentUser.getRole() == Role.ENGINEER
                    ? Arrays.asList(TicketActions.DONE)
                    : Arrays.asList();
        }
    },
    DONE("Done") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            return Arrays.asList();
        }
    },
    CANCELLED("Cancelled") {
        @Override
        public List<TicketActions> getActions(User currentUser, User userOwner) {
            return Arrays.asList();
        }
    };

    private String displayName;

    TicketState(String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return displayName;
    }

    public abstract List<TicketActions> getActions(User currentUser, User userOwner);

}
