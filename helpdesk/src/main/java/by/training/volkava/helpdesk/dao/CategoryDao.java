package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.model.Category;

import java.util.List;

public interface CategoryDao {
    List<Category> getAllCategories();
}
