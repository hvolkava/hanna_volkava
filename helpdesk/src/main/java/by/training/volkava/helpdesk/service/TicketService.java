package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.dto.TicketDtoList;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;

public interface TicketService {
    TicketDtoList getAllTickets(User user);

    Ticket getTicketById(Long id, User user);

    Long createTicket(TicketDto ticketDto, User user);

    void changeState(Long ticketId, User user, TicketState nextState);

    void changeStateInTicket(Ticket ticket, TicketState nextState);

    TicketDto getTicketDtoById(Long ticketId, User currentUser);

    void updateTicketFromTicketDto(TicketDto ticketDto, User currentUser, Long ticketId);

    void updateTicket(Ticket ticket);
}