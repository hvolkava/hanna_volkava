package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.dao.UserDao;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.UserService;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public List<User> getUsersByRole(Role role) {
        return userDao.getUsersByRole(role);
    }
}
