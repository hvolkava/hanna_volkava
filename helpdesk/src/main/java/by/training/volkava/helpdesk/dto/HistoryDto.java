package by.training.volkava.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class HistoryDto {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateTime;
    private String username;
    private String action;
    private String description;

    public HistoryDto() {
    }

    public HistoryDto(HistoryDtoBuilder builder) {
        this.dateTime = builder.dateTime;
        this.username = builder.username;
        this.action = builder.action;
        this.description = builder.description;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public HistoryDto setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public HistoryDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getAction() {
        return action;
    }

    public HistoryDto setAction(String action) {
        this.action = action;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public HistoryDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public static class HistoryDtoBuilder {
        private LocalDateTime dateTime;
        private String username;
        private String action;
        private String description;

        public HistoryDtoBuilder setDateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public HistoryDtoBuilder setUsername(String username) {
            this.username = username;
            return this;
        }

        public HistoryDtoBuilder setAction(String action) {
            this.action = action;
            return this;
        }

        public HistoryDtoBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public HistoryDto build() {
            return new HistoryDto(this);
        }
    }




}
