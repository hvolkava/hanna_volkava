package by.training.volkava.helpdesk.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.dto.TicketDtoList;
import by.training.volkava.helpdesk.dto.validate.NewTicket;
import by.training.volkava.helpdesk.dto.validate.UpdateTicket;
import by.training.volkava.helpdesk.dto.validate.UpdateTicketState;
import by.training.volkava.helpdesk.enums.Urgency;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;

import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.TicketService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/tickets")
public class TicketController {
    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<TicketDtoList> getListTickets(Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        return ResponseEntity.ok(ticketService.getAllTickets(currentUser));
    }

    @GetMapping(value = "/{ticketId}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<TicketDto> getTicketById(@PathVariable(value = "ticketId")
                                                               Long ticketId,
                                                   Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        return ResponseEntity.ok(ticketService.getTicketDtoById(ticketId, currentUser));
    }

    @PostMapping
    public ResponseEntity<TicketDto> createTicket(@Validated(NewTicket.class)
                                                      @RequestBody TicketDto ticketDto,
                                                  Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        final URI uri =
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(ticketService.createTicket(ticketDto, currentUser))
                        .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(value = "/{ticketId}")
    public ResponseEntity<TicketDto> updateTicket(@PathVariable(value = "ticketId") Long ticketId,
                                                  @Validated(UpdateTicket.class)
                                                  @RequestBody TicketDto ticketDto,
                                                  Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        ticketService.updateTicketFromTicketDto(ticketDto, currentUser, ticketId);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(value = "/{ticketId}")
    public ResponseEntity<Void> changeStateInTicket(
            @PathVariable(value = "ticketId") Long ticketId,
            @Validated(UpdateTicketState.class) @RequestBody  TicketDto ticketDto,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        ticketService.changeState(ticketId, currentUser, ticketDto.getState());
        return ResponseEntity.ok().build();
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE, value = "/urgencies")
    public ResponseEntity<Urgency[]> getListTicketsUrgencies() {
        return ResponseEntity.ok(Urgency.values());
    }
}
