package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.dto.AttachmentDto;
import by.training.volkava.helpdesk.model.Attachment;
import by.training.volkava.helpdesk.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface AttachmentService {
    Long createAttachment(Long ticketId, MultipartFile file, User currentUser) throws IOException;

    List<AttachmentDto> getAllAttachmentByTicketId(Long ticketId, User currentUser);

    Attachment getAttachment(Long ticketId, Long attachmentId, User currentUser);

    void deleteAttachment(Long ticketId, Long attachmentId, User currentUser);
}
