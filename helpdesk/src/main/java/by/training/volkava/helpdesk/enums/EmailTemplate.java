package by.training.volkava.helpdesk.enums;

import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.UserService;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public enum EmailTemplate {
    NEW_TICKET_FOR_APPROVAL("New ticket for approval",
            "html/new-ticket-for-approval") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            return userService.getUsersByRole(Role.MANAGER)
                    .stream()
                    .collect(Collectors.toSet());
        }
    },
    TICKET_WAS_APPROVED("Ticket was approved",
            "html/ticket-was-approved") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            Set<User> users = userService.getUsersByRole(Role.ENGINEER)
                    .stream()
                    .collect(Collectors.toSet());
            users.add(ticket.getUserOwner());
            return users;
        }
    },
    TICKET_WAS_DECLINED("Ticket was declined",
            "html/ticket-was-declined") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            Set<User> users = new HashSet<>();
            users.add(ticket.getUserOwner());
            return users;
        }
    },
    TICKET_WAS_CANCELLED_BY_MANAGER("Ticket was cancelled",
            "html/ticket-was-cancelled-by-manager") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            Set<User> users = new HashSet<>();
            users.add(ticket.getUserOwner());
            return users;
        }
    },
    TICKET_WAS_CANCELLED_BY_ENGINEE("Ticket was cancelled",
            "html/ticket-was-cancelled-by-engineer") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            Set<User> users = new HashSet<>();
            users.add(ticket.getUserOwner());
            return users;
        }
    },
    TICKET_WAS_DONE("Ticket was done", "html/ticket-was-done") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            Set<User> users = new HashSet<>();
            users.add(ticket.getUserOwner());
            return users;
        }
    },
    FEEDBACK_WAS_PROVIDED("Ticket was provided",
            "html/feedback-was-provided") {
        @Override
        public Set<User> getUsersForNotify(Ticket ticket, UserService userService) {
            Set<User> users = new HashSet<>();
            users.add(ticket.getUserAssignee());
            return users;
        }
    };

    private String subject;
    private String emailTemplate;

    EmailTemplate(String subject, String emailTemplate) {
        this.subject = subject;
        this.emailTemplate = emailTemplate;
    }

    public String getSubject() {
        return subject;
    }

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public abstract Set<User> getUsersForNotify(Ticket ticket, UserService userService);
}
