package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.dto.FeedbackDto;
import by.training.volkava.helpdesk.model.Feedback;
import by.training.volkava.helpdesk.model.User;

public interface FeedbackService {
    Long createFeedback(Long ticketId, FeedbackDto feedbackDto, User currentUser);

    FeedbackDto getFeedbackByTicket(Long ticketId, User currentUser);
}
