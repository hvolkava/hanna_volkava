package by.training.volkava.helpdesk.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"by.training.volkava.helpdesk.service", "by.training.volkava.helpdesk.dao",
                "by.training.volkava.helpdesk.converter"})
public class DataServiceConfig {

}
