package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.dto.CommentDto;
import by.training.volkava.helpdesk.model.Comment;
import by.training.volkava.helpdesk.model.Ticket;

import java.util.List;

public interface CommentDao {
    void save(Comment comment);

    List<Comment> getAllCommentsByTicket(Ticket ticket);
}
