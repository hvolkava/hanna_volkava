package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.CategoryDao;
import by.training.volkava.helpdesk.model.Category;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDaoImpl implements CategoryDao {
    private static final String SELECT_ALL_CATEGORIES = "from Category";
    private SessionFactory sessionFactory;

    public CategoryDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAllCategories() {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_CATEGORIES)
                .list();
    }
}
