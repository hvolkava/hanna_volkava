package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.model.Feedback;
import by.training.volkava.helpdesk.model.Ticket;

import java.util.Optional;

public interface FeedbackDao {
    Optional<Feedback> getFeedbackByTicket(Ticket ticket);

    void save(Feedback feedback);
}
