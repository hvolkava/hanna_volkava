package by.training.volkava.helpdesk.model;

import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @Basic
    private LocalDateTime date;

    private String action;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    private String description;

    public History() {
    }

    public History(HistoryBuilder builder) {
        this.id = builder.id;
        this.ticket = builder.ticket;
        this.date = builder.date;
        this.action = builder.action;
        this.user = builder.user;
        this.description = builder.description;
    }

    public Long getId() {
        return id;
    }

    public History setId(Long id) {
        this.id = id;
        return this;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public History setTicket(Ticket ticket) {
        this.ticket = ticket;
        return this;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public History setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public String getAction() {
        return action;
    }

    public History setAction(String action) {
        this.action = action;
        return this;
    }

    public User getUser() {
        return user;
    }

    public History setUser(User user) {
        this.user = user;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public History setDescription(String description) {
        this.description = description;
        return this;
    }

    public static class HistoryBuilder {
        private Long id;
        private Ticket ticket;
        private LocalDateTime date;
        private String action;
        private User user;
        private String description;

        public HistoryBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public HistoryBuilder setTicket(Ticket ticket) {
            this.ticket = ticket;
            return this;
        }

        public HistoryBuilder setDate(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public HistoryBuilder setAction(String action) {
            this.action = action;
            return this;
        }

        public HistoryBuilder setUser(User user) {
            this.user = user;
            return this;
        }

        public HistoryBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public History build() {
            return new History(this);
        }
    }
}
