package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.enums.EmailTemplate;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.EmailService;
import by.training.volkava.helpdesk.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Service
@PropertySource({"classpath:application.properties"})
public class EmailServiceImpl implements EmailService {
    private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class.getName());
    private Map<TicketState, Map<TicketState, EmailTemplate>> notifyMap = new EnumMap<>(TicketState.class);
    {
        Map<TicketState, EmailTemplate> toNewStateMap = new EnumMap<>(TicketState.class);
        toNewStateMap.put(TicketState.NEW, EmailTemplate.NEW_TICKET_FOR_APPROVAL);

        notifyMap.put(TicketState.DRAFT, toNewStateMap);
        notifyMap.put(TicketState.DECLINED, toNewStateMap);

        Map<TicketState, EmailTemplate> fromNewStateMap = new EnumMap<>(TicketState.class);
        fromNewStateMap.put(TicketState.NEW, EmailTemplate.NEW_TICKET_FOR_APPROVAL);
        fromNewStateMap.put(TicketState.APPROVED, EmailTemplate.TICKET_WAS_APPROVED);
        fromNewStateMap.put(TicketState.DECLINED, EmailTemplate.TICKET_WAS_DECLINED);
        fromNewStateMap.put(TicketState.CANCELLED, EmailTemplate.TICKET_WAS_CANCELLED_BY_MANAGER);

        notifyMap.put(TicketState.NEW, fromNewStateMap);

        Map<TicketState, EmailTemplate> fromApprovedStateMap = new EnumMap<>(TicketState.class);
        fromApprovedStateMap.put(TicketState.CANCELLED,EmailTemplate.TICKET_WAS_CANCELLED_BY_ENGINEE);

        notifyMap.put(TicketState.APPROVED, fromApprovedStateMap);

        Map<TicketState, EmailTemplate> fromInProgressStateMap = new EnumMap<>(TicketState.class);
        fromInProgressStateMap.put(TicketState.DONE, EmailTemplate.TICKET_WAS_DONE);

        notifyMap.put(TicketState.IN_PROGRESS, fromInProgressStateMap);

        Map<TicketState, EmailTemplate> fromDoneStateMap = new EnumMap<>(TicketState.class);
        fromDoneStateMap.put(TicketState.DONE, EmailTemplate.FEEDBACK_WAS_PROVIDED);

        notifyMap.put(TicketState.DONE, fromDoneStateMap);
    }
    @Value("${base.url}")
    private String baseUrl;

    private JavaMailSender mailSender;
    private TemplateEngine htmlTemplateEngine;
    private UserService userService;

    public EmailServiceImpl(JavaMailSender mailSender,
                            @Qualifier("emailTemplateEngine") TemplateEngine htmlTemplateEngine,
                            UserService userService) {
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
        this.userService = userService;
    }

    @Async("taskExecutor")
    public void notifyUsers(Ticket ticket, TicketState oldState) {
        EmailTemplate template = findTemplate(ticket, oldState);
        Set<User> users = template.getUsersForNotify(ticket, userService);
        users.forEach((recipient) -> {
            try {
                this.sendSimpleMail(ticket, recipient, template);
            } catch (MessagingException | InterruptedException exc) {
                LOG.error(exc.getMessage());
            }
        });
    }

    private EmailTemplate findTemplate(Ticket ticket, TicketState oldState) {
        Map<TicketState, EmailTemplate> toStateMap = notifyMap.get(oldState);
        if (Objects.isNull(toStateMap)) {
            LOG.error("When ticket {} change state to {} couldn't find template for email notify", ticket, ticket.getState());
            throw new IllegalArgumentException("When ticket " + ticket + " change state to "
                    + ticket.getState() + " couldn't find template for email notify");
        }
        EmailTemplate template = toStateMap.get(ticket.getState());
        if (Objects.isNull(template)) {
            LOG.error("When ticket {} change state to {} couldn't find template for email notify", ticket, ticket.getState());
            throw new IllegalArgumentException("When ticket " + ticket + " change state to "
                    + ticket.getState() + " couldn't find template for email notify");
        }
        return template;
    }

    public void sendSimpleMail(Ticket ticket, User recipient, EmailTemplate template) throws MessagingException, InterruptedException {
        final Context ctx = new Context();
        ctx.setVariable("ticket", ticket);
        ctx.setVariable("recipient", recipient);
        ctx.setVariable("baseUrl", baseUrl);
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
        message.setSubject(template.getSubject());
        message.setTo(recipient.getEmail());
        final String htmlContent = this.htmlTemplateEngine.process(template.getEmailTemplate(), ctx);
        message.setText(htmlContent, true );
        this.mailSender.send(mimeMessage);
    }
}