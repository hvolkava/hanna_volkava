package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.model.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserDao {
    Optional<User> getUserByEmail(String email);

    List<User> getUsersByRole(Role role);
}
