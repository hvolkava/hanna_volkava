package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.enums.EmailTemplate;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;

import javax.mail.MessagingException;

public interface EmailService {
    void notifyUsers(Ticket ticket, TicketState oldState);
    void sendSimpleMail(Ticket ticket, User recipient, EmailTemplate template)
            throws MessagingException, InterruptedException;
}
