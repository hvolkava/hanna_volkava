package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.FeedbackConverter;
import by.training.volkava.helpdesk.dao.FeedbackDao;
import by.training.volkava.helpdesk.dto.FeedbackDto;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.exception.TicketException;
import by.training.volkava.helpdesk.model.Feedback;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.EmailService;
import by.training.volkava.helpdesk.service.FeedbackService;
import by.training.volkava.helpdesk.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import javax.transaction.Transactional;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    private static final Logger LOG = LoggerFactory.getLogger(FeedbackServiceImpl.class.getName());
    private FeedbackDao feedbackDao;
    private FeedbackConverter feedbackConverter;
    private TicketService ticketService;
    private EmailService emailService;

    public FeedbackServiceImpl(FeedbackDao feedbackDao,
                               FeedbackConverter feedbackConverter,
                               TicketService ticketService,
                               EmailService emailService) {
        this.feedbackDao = feedbackDao;
        this.feedbackConverter = feedbackConverter;
        this.ticketService = ticketService;
        this.emailService = emailService;
    }

    @Override
    @Transactional
    @PreAuthorize("hasAnyRole('EMPLOYEE','MANAGER')")
    public Long createFeedback(Long ticketId,
                               FeedbackDto feedbackDto,
                               User currentUser) {
        Feedback feedback = feedbackConverter.converToEntity(feedbackDto);
        Ticket ticket = ticketService.getTicketById(ticketId, currentUser);
        feedback.setTicket(ticket);
        feedback.setUser(currentUser);
        feedback.setDate(LocalDateTime.now());
        if (ticket.getUserOwner().equals(currentUser)) {
            feedbackDao.save(feedback);
        } else {
            LOG.error("User {} trying create feedback for ticket id {}", currentUser, ticketId);
            throw new TicketException("User " + currentUser.toString()
                    + " trying create feedback for ticket id " + ticketId);
        }
        emailService.notifyUsers(ticket, TicketState.DONE);
        return feedback.getId();
    }

    @Override
    @Transactional
    public FeedbackDto getFeedbackByTicket(Long ticketId,
                                           User currentUser) {
        Ticket ticket = ticketService.getTicketById(ticketId, currentUser);
        if (ticket.getUserOwner().equals(currentUser)
                || ticket.getUserAssignee().equals(currentUser)
                || ticket.getUserApprover().equals(currentUser)) {
            return feedbackConverter.convertToDto(Optional.ofNullable(ticket.getFeedback())
                    .orElseThrow(() -> {
                        LOG.error("User {} trying get feedback for ticket id {}", currentUser, ticketId);
                        return new TicketException("Trying to get feedback ticket id "
                                + ticketId);
                    }));
        } else {
            LOG.error("User {} trying get feedback for ticket id {}", currentUser, ticketId);
            throw new TicketException("User " + currentUser.toString()
                    + " trying get feedback for ticket id " + ticketId);
        }
    }
}
