package by.training.volkava.helpdesk.dto;

import by.training.volkava.helpdesk.dto.validate.NewTicket;
import by.training.volkava.helpdesk.dto.validate.UpdateTicket;
import by.training.volkava.helpdesk.dto.validate.UpdateTicketState;
import by.training.volkava.helpdesk.enums.TicketActions;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.enums.Urgency;
import by.training.volkava.helpdesk.model.Feedback;
import by.training.volkava.helpdesk.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketDto extends ResourceSupport {

    private Long ticketId;

    @NotNull(groups = {NewTicket.class, UpdateTicket.class})
    @Pattern(groups = {NewTicket.class, UpdateTicket.class},
            regexp = "^[\\p{Lower}\\p{Digit}\\p{Blank}\\p{Punct}]+$",
            message = "{ticket.name.valid}")
    private String name;

    @FutureOrPresent(groups = {NewTicket.class, UpdateTicket.class},
            message = "{ticket.desiredResolutionDate.valid}")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate desiredDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate createdOnDate;

    @NotNull(groups = {NewTicket.class, UpdateTicket.class, UpdateTicketState.class})
    private TicketState state;

    @NotNull(groups = {NewTicket.class, UpdateTicket.class})
    private Urgency urgency;

    private List<TicketActions> actionList;

    private Boolean isMineTicket;

    @NotNull(groups = {NewTicket.class, UpdateTicket.class})
    private CategoryDto category;

    @Size(max = 500)
    @Pattern(groups = {NewTicket.class, UpdateTicket.class},
            regexp = "^[\\p{Graph}\\p{Blank}]*$",
            message = "{ticket.description.valid}")
    private String description;

    @AssertTrue(groups = {NewTicket.class, UpdateTicket.class}, message = "{ticket.state.create}")
    @JsonIgnore
    public boolean isValidState() {
        return state == TicketState.DRAFT || state == TicketState.NEW;
    }

    @JsonIgnore
    private String ownerName;
    @JsonIgnore
    private Feedback feedback;

    public Long getTicketId() {
        return ticketId;
    }

    public TicketDto setTicketId(Long ticketId) {
        this.ticketId = ticketId;
        return this;
    }

    public LocalDate getCreatedOnDate() {
        return createdOnDate;
    }

    public TicketDto setCreatedOnDate(LocalDate createdOnDate) {
        this.createdOnDate = createdOnDate;
        return this;
    }

    public User getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(User userOwner) {
        this.userOwner = userOwner;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getApproverName() {
        return approverName;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    private String approverName;

    private String assigneeName;

    private User userOwner;

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setApproverName(String approverName) {
        approverName = approverName;
    }

    public void setAssigneeName(String assigneeName) {
        assigneeName = assigneeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public TicketDto setCategory(CategoryDto category) {
        this.category = category;
        return this;
    }

    public TicketDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TicketState getState() {
        return state;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public LocalDate getDesiredDate() {
        return desiredDate;
    }

    public void setDesiredDate(LocalDate desiredDate) {
        this.desiredDate = desiredDate;
    }

    public List<TicketActions> getActionList() {
        return actionList;
    }

    public void setActionList(List<TicketActions> actionList) {
        this.actionList = actionList;
    }

    public Boolean getIsMineTicket() {
        return isMineTicket;
    }

    public TicketDto setIsMineTicket(Boolean mineTicket) {
        isMineTicket = mineTicket;
        return this;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public TicketDto(TicketDtoBuilder builder) {
        this.ticketId = builder.id;
        this.name = builder.name;
        this.desiredDate = builder.desiredDate;
        this.state = builder.state;
        this.urgency = builder.urgency;
        this.actionList = builder.actionList;
        this.isMineTicket = builder.isMineTicket;
        this.createdOnDate = builder.createdOn;
        this.approverName = builder.approverName;
        this.ownerName = builder.ownerName;
        this.assigneeName = builder.assigneeName;
        this.category = builder.category;
        this.description = builder.description;
        this.userOwner = builder.userOwner;
        this.feedback = builder.feedback;
    }

    public static class TicketDtoBuilder {
        private Long id;
        private LocalDate createdOn;
        private String name;
        private LocalDate desiredDate;
        private TicketState state;
        private Urgency urgency;
        private List<TicketActions> actionList;
        private Boolean isMineTicket;
        private String ownerName;
        private String approverName;
        private String assigneeName;
        private CategoryDto category;
        private String description;
        private User userOwner;
        private Feedback feedback;

        public TicketDtoBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public TicketDtoBuilder setCategory(CategoryDto category) {
            this.category = category;
            return this;
        }

        public TicketDtoBuilder setOwnerName(String ownerName) {
            this.ownerName = ownerName;
            return this;
        }

        public TicketDtoBuilder setApproverName(String approverName) {
            this.approverName = approverName;
            return this;
        }

        public TicketDtoBuilder setAssigneeName(String assigneeName) {
            this.assigneeName = assigneeName;
            return this;
        }

        public TicketDtoBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public TicketDtoBuilder setCreatedOn(LocalDate createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public TicketDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public TicketDtoBuilder setDesiredDate(LocalDate desiredDate) {
            this.desiredDate = desiredDate;
            return this;
        }

        public TicketDtoBuilder setState(TicketState state) {
            this.state = state;
            return this;
        }

        public TicketDtoBuilder setUrgency(Urgency urgency) {
            this.urgency = urgency;
            return this;
        }

        public TicketDtoBuilder setActionList(List<TicketActions> actionList) {
            this.actionList = actionList;
            return this;
        }

        public TicketDtoBuilder setIsMineTicket(Boolean isMineTicket) {
            this.isMineTicket = isMineTicket;
            return this;
        }

        public TicketDtoBuilder setUserOwner(User userOwner) {
            this.userOwner = userOwner;
            return this;
        }

        public TicketDtoBuilder setFeedback(Feedback feedback) {
            this.feedback = feedback;
            return this;
        }

        public TicketDto build() {
            return new TicketDto(this);
        }
    }
}
