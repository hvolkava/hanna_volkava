package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.HistoryConverter;
import by.training.volkava.helpdesk.dao.HistoryDao;
import by.training.volkava.helpdesk.dto.HistoryDto;
import by.training.volkava.helpdesk.model.History;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.HistoryService;
import by.training.volkava.helpdesk.service.TicketService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

@Service
public class HistoryServiceImpl implements HistoryService {
    private HistoryConverter historyConverter;
    private HistoryDao historyDao;
    private TicketService ticketService;

    public HistoryServiceImpl(HistoryConverter historyConverter,
                              HistoryDao historyDao,
                              TicketService ticketService) {
        this.historyConverter = historyConverter;
        this.historyDao = historyDao;
        this.ticketService = ticketService;
    }

    @Override
    @Transactional
    public List<HistoryDto> getAllHistoriesByTicketId(Long ticketId, User currentUser) {
        Ticket ticket = ticketService.getTicketById(ticketId, currentUser);
        List<History> listHistories = historyDao.getAllCommentsByTicket(ticket);
        return listHistories.stream()
                .map(history -> historyConverter.convertToDto(history))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void saveHistory(History history) {
        historyDao.saveHistory(history);
    }

    @Override
    public History createHistory(User user, Ticket ticket, String action, String addition) {
        return new History.HistoryBuilder()
                .setAction(action)
                .setDescription(Objects.nonNull(addition) ? action + addition : action)
                .setUser(user)
                .setDate(LocalDateTime.now())
                .setTicket(ticket)
                .build();
    }
}
