package by.training.volkava.helpdesk.enums;

import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiPredicate;

public enum Role {
    EMPLOYEE {
        @Override
        public boolean checkAvalableForUser(Ticket ticket, User user) {
            BiPredicate<Ticket, User> predicateForEmployee = (ticket1, currentUser) ->
                    ticket1.getUserOwner().equals(currentUser);
            return predicateForEmployee.test(ticket, user);
        }
    },
    MANAGER {
        @Override
        public boolean checkAvalableForUser(Ticket currentTicket, User currentUser) {
            BiPredicate<Ticket, User> predicateForManager = (ticket, user) -> {
                return ticket.getUserOwner().equals(user)
                        || (ticket.getUserOwner().getRole() == Role.EMPLOYEE
                        && ticket.getState() == TicketState.NEW)
                        || (ticket.getUserOwner().getRole() == Role.MANAGER
                        && ticket.getState() == TicketState.NEW
                        && !ticket.getUserOwner().equals(user))
                        || (Objects.nonNull(ticket.getUserApprover())
                        && ticket.getUserApprover().equals(user)
                        && (Arrays.asList(TicketState.APPROVED, TicketState.CANCELLED,
                        TicketState.DECLINED, TicketState.IN_PROGRESS, TicketState.DONE)
                        .contains(ticket.getState())));
            };
            return predicateForManager.test(currentTicket, currentUser);
        }
    },
    ENGINEER {
        @Override
        public boolean checkAvalableForUser(Ticket currentTicket, User currentUser) {
            BiPredicate<Ticket, User> predicateForEnginee = (ticket, user) -> {
                return ticket.getUserOwner().equals(user)
                        || (Arrays.asList(Role.EMPLOYEE, Role.MANAGER)
                        .contains(ticket.getUserOwner().getRole())
                        && ticket.getState() == TicketState.APPROVED)
                        || (ticket.getUserAssignee().equals(user)
                        && (ticket.getState() == TicketState.IN_PROGRESS
                        || ticket.getState() == TicketState.DONE));
            };
            return predicateForEnginee.test(currentTicket, currentUser);
        }
    };

    public abstract boolean checkAvalableForUser(Ticket ticket, User user);
}
