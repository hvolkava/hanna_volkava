package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.HistoryDao;
import by.training.volkava.helpdesk.model.History;
import by.training.volkava.helpdesk.model.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HistoryDaoImpl implements HistoryDao {
    private SessionFactory sessionFactory;
    private static final String SELECT_ALL_HISTORIES_BY_TICKET =
            "from History where ticket = :ticket";

    public HistoryDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<History> getAllCommentsByTicket(Ticket ticket) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_HISTORIES_BY_TICKET)
                .setParameter("ticket", ticket)
                .list();
    }

    @Override
    public void saveHistory(History history) {
        sessionFactory.getCurrentSession().save(history);
    }
}
