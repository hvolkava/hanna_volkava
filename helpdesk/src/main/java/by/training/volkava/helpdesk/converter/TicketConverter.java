package by.training.volkava.helpdesk.converter;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import by.training.volkava.helpdesk.controller.FeedbackController;
import by.training.volkava.helpdesk.controller.TicketController;
import by.training.volkava.helpdesk.dto.TicketDto;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.enums.TicketState;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiPredicate;

@Component
public class TicketConverter {
    private CategoryConverter categoryConverter;

    public TicketConverter(CategoryConverter categoryConverter) {
        this.categoryConverter = categoryConverter;
    }

    private final BiPredicate<Ticket, User> predicateForEmployee = (ticket, user) ->
            ticket.getUserOwner().equals(user);
    private final BiPredicate<Ticket, User> predicateForManager = (ticket, user) ->
            (Objects.nonNull(ticket.getUserApprover()) && ticket.getUserApprover().equals(user) );
    private final BiPredicate<Ticket, User> predicateForEnginee = (ticket, user) ->
            (Objects.nonNull(ticket.getUserAssignee()) && ticket.getUserAssignee().equals(user) );
    private Map<Role, BiPredicate<Ticket, User>> isMineMap = new EnumMap<>(Role.class);

    {
        isMineMap.put(Role.EMPLOYEE, predicateForEmployee);
        isMineMap.put(Role.MANAGER, predicateForManager);
        isMineMap.put(Role.ENGINEER, predicateForEnginee);
    }

    public TicketDto convertToDto(Ticket ticket, User currentUser) {
        TicketDto ticketDto = new TicketDto.TicketDtoBuilder()
                .setId(ticket.getId())
                .setName(ticket.getName())
                .setDesiredDate(ticket.getDesiredResolutionDate())
                .setState(ticket.getState())
                .setUrgency(ticket.getUrgency())
                .setDescription(ticket.getDescription())
                .setActionList(ticket.getState().getActions(currentUser, ticket.getUserOwner()))
                .setCreatedOn(ticket.getCreatedOnDate())
                .setCategory(categoryConverter.convertToDto(ticket.getCategory()))
                .setOwnerName(ticket.getUserOwner().getFullName())
                .setApproverName(Objects.nonNull(ticket.getUserApprover())
                        ? ticket.getUserApprover().getFullName() : null)
                .setAssigneeName(Objects.nonNull(ticket.getUserAssignee())
                        ? ticket.getUserAssignee().getFullName() : null)
                .setUserOwner(ticket.getUserOwner())
                .setFeedback(ticket.getFeedback())
                .build();
        setIsMineTicketIndication(ticket, ticketDto, currentUser);
        addLinksForTicketDto(ticketDto, currentUser);
        return ticketDto;
    }


    public Ticket convertToEntity(TicketDto ticketDto) {
        return new Ticket.TicketBuilder()
                .setName(ticketDto.getName())
                .setUrgency(ticketDto.getUrgency())
                .setCategory(categoryConverter.convertToEntity(ticketDto.getCategory()))
                .setDesiredResolutionDate(ticketDto.getDesiredDate())
                .setState(ticketDto.getState())
                .setDescription(ticketDto.getDescription())
                .build();
    }

    private void setIsMineTicketIndication(Ticket ticket, TicketDto ticketDto, User currentUser) {
        BiPredicate<Ticket, User> biPredicate = isMineMap.get(currentUser.getRole());
        if (Objects.nonNull(biPredicate)) {
            ticketDto.setIsMineTicket(biPredicate.test(ticket, currentUser));
        }
    }

    private void addLinksForTicketDto(TicketDto ticketDto, User currentUser) {
        ticketDto.add(linkTo(TicketController.class).slash(ticketDto.getTicketId()).withSelfRel());
        if (ticketDto.getState() == TicketState.DRAFT) {
            ticketDto.add(linkTo(TicketController.class)
                    .slash(ticketDto.getTicketId())
                    .withRel("editTicket"));
        }
        if (ticketDto.getState() == TicketState.DONE
                && ticketDto.getUserOwner().equals(currentUser)
                && Objects.isNull(ticketDto.getFeedback())) {
            ticketDto.add(linkTo(methodOn(FeedbackController.class)
                    .getFeedback(ticketDto.getTicketId(), null))
                    .withRel("leaveFeedback"));
        }
        if (ticketDto.getState() == TicketState.DONE && Objects.nonNull(ticketDto.getFeedback())
                && (ticketDto.getUserOwner().equals(currentUser)
                    || currentUser.getRole() == Role.ENGINEER)) {
            ticketDto.add(linkTo(methodOn(FeedbackController.class)
                    .getFeedback(ticketDto.getTicketId(), null))
                    .withRel("viewFeedback"));
        }
    }
}
