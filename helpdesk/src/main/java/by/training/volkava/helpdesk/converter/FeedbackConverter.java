package by.training.volkava.helpdesk.converter;

import by.training.volkava.helpdesk.dto.FeedbackDto;
import by.training.volkava.helpdesk.model.Feedback;
import org.springframework.stereotype.Component;

@Component
public class FeedbackConverter {
    public Feedback converToEntity(FeedbackDto feedbackDto) {
        return new Feedback.FeedbackBuilder()
                .setRate(feedbackDto.getRate())
                .setText(feedbackDto.getText())
                .build();
    }

    public FeedbackDto convertToDto(Feedback feedback) {
        return new FeedbackDto(feedback.getRate(), feedback.getText());
    }
}
