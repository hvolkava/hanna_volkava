package by.training.volkava.helpdesk.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TicketActions {
    SUBMIT("Submit", TicketState.NEW),
    CANCEL("Cancel", TicketState.CANCELLED),
    APPROVE("Approve", TicketState.APPROVED),
    DECLINE("Decline", TicketState.DECLINED),
    ASSIGN_TO_ME("Assign to me", TicketState.IN_PROGRESS),
    DONE("Done", TicketState.DONE);

    private String displayName;
    private TicketState nextState;

    TicketActions(String displayName, TicketState nextState) {
        this.displayName = displayName;
        this.nextState = nextState;
    }

    public String getDisplayName() {
        return displayName;
    }

    public TicketState getNextState() {
        return nextState;
    }
}
