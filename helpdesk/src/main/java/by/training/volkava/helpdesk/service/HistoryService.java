package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.dto.HistoryDto;
import by.training.volkava.helpdesk.model.History;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;

import java.util.List;

public interface HistoryService {

    List<HistoryDto> getAllHistoriesByTicketId(Long ticketId, User currentUser);

    void saveHistory(History history);

    History createHistory(User user, Ticket ticket, String action, String addicional);
}
