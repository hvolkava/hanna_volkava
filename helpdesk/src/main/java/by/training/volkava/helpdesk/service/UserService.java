package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.model.User;

import java.util.List;

public interface UserService {
    List<User> getUsersByRole(Role role);
}
