package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.AttachmentConverter;
import by.training.volkava.helpdesk.dao.AttachmentDao;
import by.training.volkava.helpdesk.dto.AttachmentDto;
import by.training.volkava.helpdesk.exception.AttachmentException;
import by.training.volkava.helpdesk.model.Attachment;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.AttachmentService;
import by.training.volkava.helpdesk.service.HistoryService;
import by.training.volkava.helpdesk.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

@Service
public class AttachmentServiceImpl implements AttachmentService {
    private static final Logger LOG = LoggerFactory.getLogger(AttachmentServiceImpl.class.getName());
    private AttachmentConverter attachmentConverter;
    private AttachmentDao attachmentDao;
    private TicketService ticketService;
    private HistoryService historyService;

    public AttachmentServiceImpl(AttachmentConverter attachmentConverter,
                                 AttachmentDao attachmentDao,
                                 TicketService ticketService,
                                 HistoryService historyService) {
        this.attachmentConverter = attachmentConverter;
        this.attachmentDao = attachmentDao;
        this.ticketService = ticketService;
        this.historyService = historyService;
    }

    @Transactional
    @Override
    public Long createAttachment(Long ticketId,
                                 MultipartFile file,
                                 User currentUser) throws IOException {
        Ticket ticket = ticketService.getTicketById(ticketId, currentUser);
        Attachment attachment = attachmentConverter.convertToEntity(file, ticket);
        Pattern pattern = Pattern.compile("(.pdf|.doc|.docx|.png|.jpeg|.jpg)$", Pattern.CASE_INSENSITIVE);
        if (pattern.matcher(attachment.getName()).find()) {
            attachmentDao.createAttachment(attachment);
            historyService.createHistory(currentUser, ticket, "File is attached", ": " + attachment.getName());
            return attachment.getId();
        } else {
            LOG.error("Service couldn't save attachments with this extention {}", attachment.getName());
            throw new AttachmentException("Service couldn't save attachments with this extention " + attachment.getName());
        }
    }

    @Transactional
    @Override
    public List<AttachmentDto> getAllAttachmentByTicketId(Long ticketId, User currentUser) {
        Ticket ticket = ticketService.getTicketById(ticketId, currentUser);
        return attachmentDao.getAllAttachmentByTicket(ticket).stream()
                .map(attach -> attachmentConverter.convertToDto(attach))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Attachment getAttachment(Long ticketId, Long attachmentId, User currentUser) {
        Attachment attachment = attachmentDao.getAttachmentByAttachId(attachmentId)
                .orElseThrow(() -> {
                    LOG.error("Attachment id {} not found  - Ticket id {} by user {}",
                            attachmentId, ticketId, currentUser);
                    return new AttachmentException("Attachment not found");
                });
        if (attachment.getTicket().getId() == ticketId) {
            return attachment;
        } else {
            LOG.error("Ticket id {} doesn't match attachment id {} by user {}",
                    ticketId, attachment.getId(), currentUser);
            throw new AttachmentException("Ticket id doesn't match attachment id");
        }
    }

    @Override
    @Transactional
    public void deleteAttachment(Long ticketId, Long attachmentId, User currentUser) {
        Attachment attachment = attachmentDao.getAttachmentByAttachId(attachmentId)
                .orElseThrow(() ->{
                    LOG.error("Attachment id {} not found  - Ticket id {} by user {}",
                            attachmentId, ticketId, currentUser);
                    return new AttachmentException("Attachment not found");
                });
        if (attachment.getTicket().getId() == ticketId) {
            attachmentDao.deleteAttachment(attachment);
        } else {
            LOG.error("Ticket id {} doesn't match attachment id {} by user {}",
                    ticketId, attachment.getId(), currentUser);
            throw new AttachmentException("Ticket id doesn't match attachment id");
        }
        historyService.createHistory(currentUser, attachment.getTicket(),
                "File is removed", ": " + attachment.getName());
    }
}
