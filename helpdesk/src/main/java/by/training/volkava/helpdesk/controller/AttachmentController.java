package by.training.volkava.helpdesk.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import by.training.volkava.helpdesk.dto.AttachmentDto;
import by.training.volkava.helpdesk.model.Attachment;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.AttachmentService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/tickets/{ticketId}/attachments")
public class AttachmentController {
    private AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @PostMapping
    public ResponseEntity<Attachment> createAttachment(
            @PathVariable(value = "ticketId") Long ticketId,
            @RequestParam("file") MultipartFile file,
            Authentication auth) throws IOException {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        final URI uri =
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(attachmentService
                                .createAttachment(ticketId, file, currentUser))
                        .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    public ResponseEntity<List<AttachmentDto>> getAllAttachmentBiTicketId(
            @PathVariable(value = "ticketId") Long ticketId,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        List<AttachmentDto> attachmentDtoList = attachmentService
                .getAllAttachmentByTicketId(ticketId, currentUser);
        attachmentDtoList.forEach(
                (attachmentDto) -> attachmentDto.add(
                        linkTo(methodOn(AttachmentController.class)
                        .downloadAttachmentById(ticketId, attachmentDto.getAttachmentId(), auth))
                        .withSelfRel()));
        return ResponseEntity.ok(attachmentDtoList);

    }

    @GetMapping
    @RequestMapping(value = "/{attachmentId}")
    public ResponseEntity<byte[]> downloadAttachmentById(
            @PathVariable(value = "ticketId") Long ticketId,
            @PathVariable(value = "attachmentId") Long attachmentId,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        Attachment attachment = attachmentService.getAttachment(ticketId,
                attachmentId, currentUser);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="
                        + attachment.getName())
                .contentLength(attachment.getBlob().length)
                .body(attachment.getBlob());

    }

    @DeleteMapping(value = "/{attachmentId}")
    public ResponseEntity<Void> deleteAttachmentById(
            @PathVariable(value = "ticketId") Long ticketId,
            @PathVariable(value = "attachmentId") Long attachmentId,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        attachmentService.deleteAttachment(ticketId,
                attachmentId, currentUser);
        return ResponseEntity.noContent().build();

    }
}
