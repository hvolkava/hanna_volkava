package by.training.volkava.helpdesk.converter;

import by.training.volkava.helpdesk.dto.AttachmentDto;
import by.training.volkava.helpdesk.model.Attachment;
import by.training.volkava.helpdesk.model.Ticket;
import by.training.volkava.helpdesk.model.User;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class AttachmentConverter {
    public Attachment convertToEntity(MultipartFile file,
                                      Ticket ticket) throws IOException {
        Attachment attachment = new Attachment();
        attachment.setTicket(ticket);
        attachment.setBlob(file.getBytes());
        attachment.setName(file.getOriginalFilename());
        return attachment;
    }

    public AttachmentDto convertToDto(Attachment attachment) {
        AttachmentDto attachmentDto = new AttachmentDto();
        attachmentDto.setName(attachment.getName());
        attachmentDto.setAttachmentId(attachment.getId());
        return attachmentDto;
    }
}
