package by.training.volkava.helpdesk.dao;

import by.training.volkava.helpdesk.model.History;
import by.training.volkava.helpdesk.model.Ticket;

import java.util.List;

public interface HistoryDao {
    List<History> getAllCommentsByTicket(Ticket ticket);

    void saveHistory(History history);
}
