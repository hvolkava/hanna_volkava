package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.AttachmentDao;
import by.training.volkava.helpdesk.model.Attachment;
import by.training.volkava.helpdesk.model.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AttachmentDaoImpl implements AttachmentDao {
    private SessionFactory sessionFactory;
    private static final String SELECT_ALL_ATTACHMENTS_BY_TICKET =
            "from Attachment where ticket = :ticket";
    private static final String SELECT_ATTACHMENT_BY_ATTACH_ID =
            "from Attachment where id=:id";

    public AttachmentDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createAttachment(Attachment attachment) {
        sessionFactory.getCurrentSession().save(attachment);
    }

    @Override
    public List<Attachment> getAllAttachmentByTicket(Ticket ticket) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_ATTACHMENTS_BY_TICKET)
                .setParameter("ticket", ticket)
                .list();
    }

    @Override
    public Optional<Attachment> getAttachmentByAttachId(Long attachmentId) {
        return Optional.ofNullable((Attachment) sessionFactory.getCurrentSession()
                .createQuery(SELECT_ATTACHMENT_BY_ATTACH_ID)
                .setParameter("id", attachmentId)
                .uniqueResult());
    }

    @Override
    public void deleteAttachment(Attachment attachment) {
        sessionFactory.getCurrentSession().delete(attachment);
    }
}
