package by.training.volkava.helpdesk.controller;

import by.training.volkava.helpdesk.dto.HistoryDto;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.HistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/tickets/{ticketId}/histories")
public class HistoryController {
    private HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping
    public ResponseEntity<List<HistoryDto>> getAllHistoriesByTicketId(
            @PathVariable(value = "ticketId") Long ticketId,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        return ResponseEntity.ok(historyService.getAllHistoriesByTicketId(ticketId, currentUser));
    }

}
