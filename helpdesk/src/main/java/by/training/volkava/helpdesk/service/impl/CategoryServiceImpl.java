package by.training.volkava.helpdesk.service.impl;

import by.training.volkava.helpdesk.converter.CategoryConverter;
import by.training.volkava.helpdesk.dao.CategoryDao;
import by.training.volkava.helpdesk.dto.CategoryDto;
import by.training.volkava.helpdesk.model.Category;
import by.training.volkava.helpdesk.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryDao categoryDao;
    private CategoryConverter categoryConverter;

    public CategoryServiceImpl(CategoryDao categoryDao, CategoryConverter categoryConverter) {
        this.categoryDao = categoryDao;
        this.categoryConverter = categoryConverter;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryDto> getAllCategories() {
        List<Category> listCategories = categoryDao.getAllCategories();
        return listCategories.stream()
                .map(category -> categoryConverter.convertToDto(category))
                .collect(Collectors.toList());
    }
}
