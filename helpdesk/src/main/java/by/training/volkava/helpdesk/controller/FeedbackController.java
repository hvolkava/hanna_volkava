package by.training.volkava.helpdesk.controller;

import by.training.volkava.helpdesk.dto.FeedbackDto;
import by.training.volkava.helpdesk.model.CustomUserPrincipal;
import by.training.volkava.helpdesk.model.User;
import by.training.volkava.helpdesk.service.FeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/tickets/{ticketId}/feedbacks")
public class FeedbackController {
    private FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PostMapping
    public ResponseEntity<FeedbackDto> createFeedback(
            @PathVariable(value = "ticketId") Long ticketId,
            @Valid @RequestBody FeedbackDto feedbackDto,
            Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        final URI uri =
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(feedbackService
                                .createFeedback(ticketId, feedbackDto, currentUser))
                        .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    public ResponseEntity<FeedbackDto> getFeedback(@PathVariable(value = "ticketId") Long ticketId,
                                                     Authentication auth) {
        User currentUser = ((CustomUserPrincipal) auth.getPrincipal()).getUser();
        return ResponseEntity.ok(feedbackService.getFeedbackByTicket(ticketId, currentUser));
    }
}
