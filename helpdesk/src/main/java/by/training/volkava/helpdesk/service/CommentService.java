package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.dto.CommentDto;
import by.training.volkava.helpdesk.model.User;

import java.util.List;

public interface CommentService {
    Long createComment(Long ticketId, CommentDto commentDto, User currentUser);

    List<CommentDto> getAllCommentsByTicketId(Long ticketId, User currentUser);
}