package by.training.volkava.helpdesk.service;

import by.training.volkava.helpdesk.dto.CategoryDto;

import java.util.List;

public interface CategoryService {
    List<CategoryDto> getAllCategories();
}
