package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.CommentDao;
import by.training.volkava.helpdesk.dto.CommentDto;
import by.training.volkava.helpdesk.model.Comment;
import by.training.volkava.helpdesk.model.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentDaoImpl implements CommentDao {
    private SessionFactory sessionFactory;

    public CommentDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private static final String SELECT_ALL_COMMENTS_BY_TICKET =
            "from Comment where ticket = :ticket";

    @Override
    public void save(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }

    @Override
    public List<Comment> getAllCommentsByTicket(Ticket ticket) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_COMMENTS_BY_TICKET)
                .setParameter("ticket", ticket)
                .list();
    }
}
