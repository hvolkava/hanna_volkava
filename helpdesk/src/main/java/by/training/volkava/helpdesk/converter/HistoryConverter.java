package by.training.volkava.helpdesk.converter;

import by.training.volkava.helpdesk.dto.HistoryDto;
import by.training.volkava.helpdesk.model.History;
import org.springframework.stereotype.Component;

@Component
public class HistoryConverter {

    public HistoryDto convertToDto(History history) {
        return new HistoryDto.HistoryDtoBuilder()
                .setAction(history.getAction())
                .setDateTime(history.getDate())
                .setDescription(history.getDescription())
                .setUsername(history.getUser().getFullName())
                .build();
    }
}
