package by.training.volkava.helpdesk.dto;

import org.springframework.hateoas.ResourceSupport;

public class AttachmentDto  extends ResourceSupport {
    private Long attachmentId;
    private String name;

    public AttachmentDto() {
    }

    public String getName() {
        return name;
    }

    public AttachmentDto setName(String name) {
        this.name = name;
        return this;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }
}
