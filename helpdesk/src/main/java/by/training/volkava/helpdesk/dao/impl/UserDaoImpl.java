package by.training.volkava.helpdesk.dao.impl;

import by.training.volkava.helpdesk.dao.UserDao;
import by.training.volkava.helpdesk.enums.Role;
import by.training.volkava.helpdesk.model.User;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class.getName());
    private static final String SELECT_BY_EMAIL_HQL = "from User WHERE email = :email";
    private static final String SELECT_BY_ROLE_HQL = "from User WHERE role = :role";
    private SessionFactory sessionFactory;

    public UserDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Optional<User> getUserByEmail(String email) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_BY_EMAIL_HQL)
                .setParameter("email", email)
                .uniqueResultOptional();
    }

    @Override
    public List<User> getUsersByRole(Role role) {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_BY_ROLE_HQL)
                .setParameter("role", role)
                .list();
    }
}
