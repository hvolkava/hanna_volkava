package by.training.volkava.helpdesk.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Urgency {
    CRITICAL(1, "Critical"),
    HIGH(2, "High"),
    AVERAGE(3, "Average"),
    LOW(4, "Low");

    Urgency(int order, String displayName) {
        this.order = order;
        this.displayName = displayName;
    }

    private int order;
    private String displayName;

    public int getOrder() {
        return order;
    }

    public String getDisplayName() {
        return displayName;
    }
}
