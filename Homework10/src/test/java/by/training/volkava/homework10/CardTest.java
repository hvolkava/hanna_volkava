package by.training.volkava.homework10;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for Card class.
 */
public class CardTest {

    private Card card;

    @Before
    public void setUp() throws CardException {
        card = new Card("John Test", 500);
    }

    @Test
    public void testGetAccountBalance() throws CardException {
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 500;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test
    public void testReplenishAccountBalance_WithCorrectAmount() throws CardException {
        card.replenishAccountBalance(100);
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 600;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test
    public void testWithdrawAccountBalance_WithCorrectAmount() throws CardException {
        card.withdrawAccountBalance(100);
        double actualBalance = card.getAccountBalance();
        double expectedBalance = 400;
        double precision = 0.00001;
        Assert.assertEquals(expectedBalance, actualBalance, precision);
    }

    @Test(expected = CardException.class)
    public void testReplenishAccountBalance_WithNegativeAmount() throws CardException {
        card.replenishAccountBalance(-100);
    }

    @Test(expected = CardException.class)
    public void testWithdrawAccountBalance_WithNegativeAmount() throws CardException {
        card.withdrawAccountBalance(-100);
    }

    @Test(expected = CardException.class)
    public void testCardConstructor_WithoutNameHolder() throws CardException {
        Card card = new Card("", 500);
    }

    @Test(expected = CardException.class)
    public void testCardConstructor_WithNegativeAccountBalance() throws CardException {
        Card card = new Card("John Test", -500);
    }

}