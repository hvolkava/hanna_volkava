package by.training.volkava.homework10;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for Atm class.
 */
public class AtmTest {
    private Atm atm;

    @Before
    public void setUp() throws Exception {
        atm = new Atm();
    }

    @Test
    public void testReplenishAccount_WithCorrectAmount() throws CardException {
        Card card = new Card("John Test");
        boolean actual = atm.replenishAccount(card, 500);
        Assert.assertTrue(actual);
    }

    @Test
    public void testReplenishAccount_WithNegativeAmount() throws CardException {
        Card card = new Card("John Test");
        boolean actual = atm.replenishAccount(card, -500);
        Assert.assertFalse(actual);
    }

    @Test
    public void testWithdrawalFromAccount_WithCorrectAmount() throws CardException {
        Card card = new Card("John Test", 500);
        boolean actual = atm.withdrawalFromAccount(card, 500);
        Assert.assertTrue(actual);
    }
}