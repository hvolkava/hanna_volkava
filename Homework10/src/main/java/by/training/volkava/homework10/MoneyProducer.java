package by.training.volkava.homework10;

import static by.training.volkava.homework10.Main.isLimitReached;
import static by.training.volkava.homework10.Main.tryToSleep;

import java.util.concurrent.ThreadLocalRandom;

/**
 * MoneyProducer class is designed to replenish the amount (5-10) to the card.
 *
 * @author Hanna Volkava
 */
public class MoneyProducer extends Atm implements Runnable {
    private Card card;
    private String nameAtm;

    public MoneyProducer(Card card, String nameAtm) {
        this.card = card;
        this.nameAtm = nameAtm;
    }

    @Override
    public void run() {
        synchronized (card) {
            if (isLimitReached(card)) {
                tryToSleep();
                return;
            }
            int amount = ThreadLocalRandom.current().nextInt(5, 11);
            System.out.print(nameAtm + " to card replenish " + amount);
            replenishAccount(card, amount);
            System.out.println(".\tAccount balance after: " + card.getAccountBalance());
        }
    }
}