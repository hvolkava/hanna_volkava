package by.training.volkava.homework10;

/**
 * Class exception for Card class.
 *
 * @author Hanna Volkava
 */
public class CardException extends Exception {
    public CardException(String message) {
        super(message);
    }
}