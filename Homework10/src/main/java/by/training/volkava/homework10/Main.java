package by.training.volkava.homework10;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Main class showing work with multithreading on the example of the task Consumers and Producers.
 *
 * @author Hanna Volkava
 */
public class Main {
    /**
     * Application start point. Creates a card with 500 and some ATMs both types.
     * If at some point there is no money left in the account or the amount exceeds 1 000
     * the program should display a corresponding message and complete the work.
     *
     * @param args arguments of command line
     * @throws CardException occurs when you incorrect card handling
     */
    public static void main(String[] args) throws CardException {
        Card card = new Card("Tom Soyer", 500);
        ExecutorService executorService = createExecutorService(card);
        while (true) {
            synchronized (card) {
                if (isLimitReached(card)) {
                    executorService.shutdown();
                    System.out.println("Account balance less than zero or more than 1000 ");
                    break;
                }
            }
        }
        System.out.println("EXIT");
    }

    public static boolean isLimitReached(Card card) {
        double accountBalance = card.getAccountBalance();
        return accountBalance < 0 || accountBalance > 1000;
    }

    /**
     * Method for trying thread to on 1 second.
     */
    public static void tryToSleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Method must create a some numbers (3-5) ATMs of MoneyConsumer and MoneyProducer types,
     * each of which uses the same card and run them on execution, each in a separate thread.
     *
     * @param card card
     * @return instance of ScheduledExecutorService
     */
    private static ExecutorService createExecutorService(Card card) {
        Random random = new Random();
        int numberProducers = random.nextInt(4) + 2;
        int numberConsumers = random.nextInt(4) + 2;
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(
                numberProducers + numberConsumers);
        for (int i = 0; i < numberProducers; i++) {
            Runnable atm = new MoneyProducer(card, "AtmProd" + (i + 1));
            executorService.scheduleWithFixedDelay(atm, 0,
                    random.nextInt(3) + 3, TimeUnit.SECONDS);
        }
        for (int i = 0; i < numberConsumers; i++) {
            Runnable atm = new MoneyConsumer(card, "AtmCons" + (i + 1));
            executorService.scheduleWithFixedDelay(atm, 0,
                    random.nextInt(3) + 3, TimeUnit.SECONDS);
        }
        return executorService;
    }
}
