package by.training.volkava.homework06;

import java.util.Map;
import java.util.Scanner;

/**
 * Program is designed to find the frequency of words.
 *
 * @author Hanna Volkava
 */
public class Main {
    /**
     * Application start point that takes accepts an English text at the input,
     * processes it and displays all the words that appear in the text (without duplicates)
     * by grouping them by alphabetical order by the first letter.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            System.out.println("Enter your text:");
            String userText = scan.nextLine();
            Map<String, Integer> map = FrequencyOfWords.countFrequencyOfWords(userText);
            if (map.size() > 0) {
                System.out.println(FrequencyOfWords.getStringMap(map));
            } else {
                System.out.println("There are no words in your text");
            }
        } catch (Exception exception) {
            System.out.println(exception);
        }
    }
}
