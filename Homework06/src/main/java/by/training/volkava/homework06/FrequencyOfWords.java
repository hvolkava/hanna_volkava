package by.training.volkava.homework06;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * FrequencyOfWords class is designed to find the frequency of words.
 *
 * @author Hanna Volkava
 */
public class FrequencyOfWords {
    public static final String SPLITTER = "[,;:.!?\\s()]+";

    /**
     * Method count frequency of words in input text.
     *
     * @param text input text
     * @return map contains unique words as key and frequency of use as value
     */
    public static Map<String, Integer> countFrequencyOfWords(String text) {
        if (Objects.isNull(text)) {
            throw new IllegalArgumentException("Text must be not null");
        }
        String[] words = text.split(SPLITTER);
        Map<String, Integer> map = new LinkedHashMap<>();
        Arrays.stream(words)
                .filter(word -> word.length() > 0)
                .map(String::toLowerCase)
                .forEach(uniqueWord -> map.merge(uniqueWord, 1, Integer::sum));
        return map;
    }

    /**
     * Method sorted by first letter key of map and forms a string for output.
     *
     * @param map containing unique words as key and frequency of use as value
     * @return string for output map
     */
    public static String getStringMap(Map<String, Integer> map) {
        StringBuilder result = new StringBuilder();
        List<Map.Entry<String, Integer>> sortedList = map.entrySet().stream()
                .sorted(Comparator.comparing(word -> word.getKey().charAt(0)))
                .collect(toList());
        char capitalLetter = ' ';
        for (Map.Entry<String, Integer> entry : sortedList) {
            char firstLetter = entry.getKey().toUpperCase().charAt(0);
            if (capitalLetter != firstLetter) {
                result.append(firstLetter).append(": ");
                capitalLetter = firstLetter;
            } else {
                result.append("   ");
            }
            result.append(entry.getKey()).append(' ').append(entry.getValue()).append("\n");
        }
        return result.toString();
    }
}
