package by.training.volkava.homework06;

import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class FrequencyOfWordsTest {

    @Test
    public void testGetStringMap() {
        Map<String, Integer> map = new LinkedHashMap<>();
        map.put("one", 3);
        map.put("but", 2);
        map.put("begin", 1);
        String expected = "B: but 2\n" +
                "   begin 1\n" +
                "O: one 3\n";
        String actual = FrequencyOfWords.getStringMap(map);
        assertEquals(expected, actual);
    }
    @Test
    public void testCountFrequencyOfWords_WithText() {
        String text = "one but one begin one but";
        Map<String, Integer> expected = new LinkedHashMap<>();
        expected.put("one", 3);
        expected.put("but", 2);
        expected.put("begin", 1);
        Map<String, Integer> actual = FrequencyOfWords.countFrequencyOfWords(text);
        assertEquals(expected, actual);
    }
    @Test
    public void testCountFrequencyOfWords_WithEmpty() {
        String text = "";
        Map<String, Integer> map = FrequencyOfWords.countFrequencyOfWords(text);
        assertEquals(0, map.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCountFrequencyOfWords_WithNull() {
        String text = null;
        Map<String, Integer> map = FrequencyOfWords.countFrequencyOfWords(text);
    }
}