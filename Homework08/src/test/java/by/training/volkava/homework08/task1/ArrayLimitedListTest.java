package by.training.volkava.homework08.task1;

import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;

/**
 * Test suite for ArrayLimitedList class.
 */
public class ArrayLimitedListTest {

    @Test
    public void testAdd_InCorrectCase() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(5);
        Assert.assertTrue(list.add(1));
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testAdd_WithOverflow() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(1);
        list.add(1);
        list.add(1);
    }

    @Test
    public void testAdd_WithIndexInCorrectCase() throws ArrayLimitedListException {
        LimitedList<Byte> list = new ArrayLimitedList<>(3);
        list.add((byte) 1);
        Assert.assertTrue(list.add(0, (byte) 2));
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testAdd_WithIndexInIllegalAccess() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(5);
        list.add(4, 1);
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testAdd_WithIndexOverflow() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(1);
        list.add(0, 1);
        list.add(0, 1);
    }

    @Test
    public void testRemove_InCorrectCase() throws ArrayLimitedListException {
        LimitedList<String> list = new ArrayLimitedList<>(3);
        list.add(0, "3");
        list.add(0, "2");
        list.add(0, "1");
        Assert.assertTrue(list.remove("1"));
    }

    @Test
    public void testRemove_WithoutExistObject() throws ArrayLimitedListException {
        LimitedList<String> list = new ArrayLimitedList<>(3);
        list.add(0, "3");
        list.add(0, "2");
        list.add(0, "1");
        Assert.assertFalse(list.remove("4"));
    }

    @Test
    public void testRemove_WithIndexInCorrectCase() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(3);
        list.add(0, 3);
        list.add(0, 2);
        list.add(0, 1);
        Assert.assertEquals(java.util.Optional.of(1).get(), list.remove(0));

    }

    @Test(expected = ArrayLimitedListException.class)
    public void testRemove_WithIndexInIllegalAccess() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(5);
        list.add(0, 1);
        list.remove(1);
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testRemove_InIllegalAccess() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(5);
        list.remove(0);
    }

    @Test
    public void testIsEmpty_WithEmptyCase() throws ArrayLimitedListException {
        LimitedList<Byte> list = new ArrayLimitedList<>(0);
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void testIsEmpty_WithoutEmptyCase() throws ArrayLimitedListException {
        LimitedList<Byte> list = new ArrayLimitedList<>(Arrays.asList(new Byte[]{1, 2, 3}));
        Assert.assertFalse(list.isEmpty());
    }

    @Test
    public void testSize() throws ArrayLimitedListException {
        LimitedList<Byte> list = new ArrayLimitedList<>(Arrays.asList(new Byte[]{1, 2, 3}));
        Assert.assertEquals(3, list.size());
    }

    @Test
    public void testContains_WithExistObject() throws ArrayLimitedListException {
        LimitedList<Byte> list = new ArrayLimitedList<>(Arrays.asList(new Byte[]{1, 2, 3}));
        Assert.assertTrue(list.contains((byte) 2));
    }

    @Test
    public void testContains_WithoutExistObject() throws ArrayLimitedListException {
        LimitedList<Byte> list = new ArrayLimitedList<>(Arrays.asList(new Byte[]{1, 2, 3}));
        Assert.assertFalse(list.contains((byte) 5));
    }

    @Test
    public void testClear() throws ArrayLimitedListException {
        LimitedList<Long> list = new ArrayLimitedList<>(Arrays.asList(new Long[]{1L}));
        list.clear();
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testSet_WithCorrectCase() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(Arrays.asList(new Integer[]{1, 2, 3}));
        Assert.assertTrue(list.set(1, null));
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testSet_WithIllegalAccess() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(3);
        list.set(3, null);
    }

    @Test
    public void testIndexOf_WithExistObject() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(Arrays.asList(new Integer[]{1, 2, 3, 4, 5}));
        Assert.assertEquals(1, list.indexOf(2));
    }

    @Test
    public void testIndexOf_WithoutExixtObject() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(Arrays.asList(new Integer[]{1, 2, 3, 4, 5}));
        Assert.assertEquals(-1, list.indexOf(7));
    }

    @Test
    public void testIndexOf_WithNullObject() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(Arrays.asList(new Integer[]{0, 2, 3, 4, 5}));
        list.set(2, null);
        Assert.assertEquals(2, list.indexOf(null));
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testArrayLimitedListConstructor_WithNegativeDimention() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(-5);
    }

    @Test(expected = ArrayLimitedListException.class)
    public void testArrayLimitedListConstructor_WithNullCollection() throws ArrayLimitedListException {
        LimitedList<Integer> list = new ArrayLimitedList<>(null);
    }
}