package by.training.volkava.homework08.task2;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class WorkerTest {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_WithoutName() {
        Worker worker = new Worker("");
    }

    @Test(expected = NullPointerException.class)
    public void testConstructor_WithNullName() {
        Worker worker = new Worker(null);
    }

    @Test
    public void testGetProfSkills_WithEqualsSkills() {
        Worker worker = new Worker("Tom", ProfessionalSkills.BUILDER, ProfessionalSkills.BUILDER);
        Assert.assertEquals(1, worker.getProfSkills().size());
    }

    @Test
    public void testEquals_WithDifferentId() {
        Worker worker1 = new Worker("Tom", ProfessionalSkills.BUILDER);
        Worker worker2 = new Worker("Tom", ProfessionalSkills.BUILDER);
        Assert.assertFalse(worker1.equals(worker2));
    }

    @Test
    public void testEquals_WithEqualsId() {
        Worker worker1 = new Worker("Tom", ProfessionalSkills.BUILDER);
        Worker worker2 = worker1;
        Assert.assertTrue(worker1.equals(worker2));
    }
}