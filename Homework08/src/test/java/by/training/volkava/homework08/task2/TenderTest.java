package by.training.volkava.homework08.task2;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

public class TenderTest {
    private Tender tender;

    @Test
    public void testFindExecuters_WithNotMeetingRequirement() {
        Worker worker1 = new Worker("Anne", ProfessionalSkills.ACCOUNTANT,
                ProfessionalSkills.HR_SPECIALIST);
        Worker worker2 = new Worker("John", ProfessionalSkills.BUILDER,
                ProfessionalSkills.BRICKLAYER);
        Worker worker3 = new Worker("Tom", ProfessionalSkills.BUILDER,
                ProfessionalSkills.WELDER);
        Team team1 = new Team("Team1", worker1, worker2, worker3);

        Worker worker4 = new Worker("Anne", ProfessionalSkills.ACCOUNTANT,
                ProfessionalSkills.HR_SPECIALIST, ProfessionalSkills.BUILDER);
        Worker worker5 = new Worker("John", ProfessionalSkills.BRICKLAYER);
        Worker worker6 = new Worker("Tom", ProfessionalSkills.BUILDER,
                ProfessionalSkills.WELDER, ProfessionalSkills.BRICKLAYER);
        Team team2 = new Team("Winner", worker4, worker5, worker6);

        Worker worker7 = new Worker("Tom", ProfessionalSkills.PROGRAMMER);
        Worker worker8 = new Worker("Tom", ProfessionalSkills.ELECTRICIAN);
        Team team3 = new Team("Freelance", worker7, worker8, worker8);

        Map<ProfessionalSkills, Integer> requirement = new EnumMap<>(ProfessionalSkills.class);
        requirement.put(ProfessionalSkills.BUILDER,3);
        requirement.put(ProfessionalSkills.HR_SPECIALIST, 1);
        requirement.put(ProfessionalSkills.BRICKLAYER, 2);

        tender = new Tender(requirement);
        tender.submitFinancialProposal(team1, 2000);
        tender.submitFinancialProposal(team1, 2000);
        tender.submitFinancialProposal(team2, 4000);
        tender.submitFinancialProposal(team3, 1000);

        Optional<Team> potentialMap = tender.findExecutor();
        Assert.assertFalse(potentialMap.isPresent());
    }

    @Test
    public void testFindExecuters_WithMeetingRequirement() {
        Worker worker1 = new Worker("Anne", ProfessionalSkills.ACCOUNTANT,
                ProfessionalSkills.HR_SPECIALIST);
        Worker worker2 = new Worker("John", ProfessionalSkills.BUILDER,
                ProfessionalSkills.BRICKLAYER);
        Worker worker3 = new Worker("Tom", ProfessionalSkills.BUILDER,
                ProfessionalSkills.WELDER);
        Team team1 = new Team("Team1", worker1, worker2, worker3);

        Worker worker4 = new Worker("Anne", ProfessionalSkills.ACCOUNTANT,
                ProfessionalSkills.HR_SPECIALIST, ProfessionalSkills.BUILDER);
        Worker worker5 = new Worker("John", ProfessionalSkills.BUILDER,
                ProfessionalSkills.BRICKLAYER);
        Worker worker6 = new Worker("Tom", ProfessionalSkills.BUILDER,
                ProfessionalSkills.WELDER, ProfessionalSkills.BRICKLAYER);
        Team team2 = new Team("Winner", worker4, worker5, worker6);

        Worker worker7 = new Worker("Tom", ProfessionalSkills.PROGRAMMER);
        Worker worker8 = new Worker("Tom", ProfessionalSkills.ELECTRICIAN);
        Team team3 = new Team("Freelance", worker7, worker8, worker8);

        Map<ProfessionalSkills, Integer> requirement = new EnumMap<>(ProfessionalSkills.class);
        requirement.put(ProfessionalSkills.BUILDER,2);
        requirement.put(ProfessionalSkills.HR_SPECIALIST, 1);
        requirement.put(ProfessionalSkills.BRICKLAYER, 1);

        tender = new Tender(requirement);
        tender.submitFinancialProposal(team1, 2000);
        tender.submitFinancialProposal(team2, 4000);
        tender.submitFinancialProposal(team3, 1000);

        Optional<Team> potentialMap = tender.findExecutor();
        Assert.assertEquals(team1.getId(), potentialMap.get().getId());
    }

    @Test(expected = NullPointerException.class)
    public void testConstructor_WithNull() {
        Tender tend = new Tender(null);
    }
}
