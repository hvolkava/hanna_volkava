package by.training.volkava.homework08.task2;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;

/**
 * Class Worker represents an employee with a set of specific skills.
 *
 * @author Hanna Volkava
 */
public class Worker {
    private static int nextId;
    private int id = assignId();
    private String name;
    private EnumSet<ProfessionalSkills> profSkills = EnumSet.noneOf(ProfessionalSkills.class);

    /**
     * Constructor creates an instance of worker with the given name and skills.
     *
     * @param name       worker name
     * @param profSkills a set of specific skills
     */
    public Worker(String name, ProfessionalSkills... profSkills) {
        setName(name);
        Collections.addAll(this.profSkills, profSkills);
    }

    /**
     * Sets a new worker name.
     *
     * @param name new worker name
     */
    public void setName(String name) {
        if (Objects.requireNonNull(name, "Name must be not null").isEmpty()) {
            throw new IllegalArgumentException("Name must be not empty");
        }
        this.name = name;
    }

    /**
     * Method for obtaining professional skills of worker.
     *
     * @return set of professional skills worker
     */

    public EnumSet<ProfessionalSkills> getProfSkills() {
        return profSkills;
    }

    private static int assignId() {
        int workerId = nextId;
        nextId++;
        return workerId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Worker other = (Worker) obj;
        return id == other.id;
    }
}
