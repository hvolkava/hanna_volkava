package by.training.volkava.homework08.task1;

/**
 * The LimitedList interface describes an ordered collection in which
 * the location of an item matters.
 *
 * @param <E> the type of item in list
 * @author Hanna Volkava
 */
interface LimitedList<E> {
    /**
     * Adds an object to an array.
     *
     * @param object object that we add
     * @return true if list changed
     * @throws ArrayLimitedListException if couldn't add object in array
     */
    boolean add(E object) throws ArrayLimitedListException;

    /**
     * Add object on a specified position in the list.
     *
     * @param index  index list
     * @param object object we will add
     * @return true if list changed
     * @throws ArrayLimitedListException if index not check range in list and list is full
     */
    boolean add(int index, E object) throws ArrayLimitedListException;

    /**
     * Removes an object from the collection that is equal to element of list.
     *
     * @param object object we will remove
     * @return true if the collection has changed
     */
    boolean remove(E object);

    /**
     * Remove an item at a specified position in the list.
     *
     * @param index index list
     * @return true if list changed
     * @throws ArrayLimitedListException if index not check range in list
     */
    E remove(int index) throws ArrayLimitedListException;

    /**
     * Checks if list is empty.
     *
     * @return true if list does not contain any of the elements.
     */
    boolean isEmpty();

    /**
     * Returns the number of items currently stored in list.
     *
     * @return size list
     */
    int size();

    /**
     * Checks if there is an object in list.
     *
     * @param object object we will check
     * @return true if object contains in list, false otherwise
     */
    boolean contains(E object);

    /**
     * Removes all items from list.
     */
    void clear();

    /**
     * Replaces an item at a specified position in the list with a new item.
     *
     * @param index  index list
     * @param object object we will set
     * @return true if list changed
     * @throws ArrayLimitedListException if index not check range in list
     */
    boolean set(int index, E object) throws ArrayLimitedListException;

    /**
     * Returns the position of the first occurrence of the item in the list.
     *
     * @param object object we are looking for
     * @return the position of the first occurrence of the item in the list
     *         or a value of 1 if the item is not found
     */
    int indexOf(E object);
}
