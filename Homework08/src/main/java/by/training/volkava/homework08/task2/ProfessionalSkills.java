package by.training.volkava.homework08.task2;

public enum ProfessionalSkills {
    ACCOUNTANT, BUILDER, MANAGER, WELDER, PROGRAMMER, HR_SPECIALIST, BRICKLAYER, ELECTRICIAN
}
