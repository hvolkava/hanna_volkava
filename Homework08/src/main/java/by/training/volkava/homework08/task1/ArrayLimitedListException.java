package by.training.volkava.homework08.task1;

public class ArrayLimitedListException extends Exception {
    public ArrayLimitedListException(String message) {
        super(message);
    }
}
