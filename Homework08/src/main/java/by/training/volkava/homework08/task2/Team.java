package by.training.volkava.homework08.task2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The class is a team of builders with a certain set of worker.
 *
 * @author Hanna Volkava
 */
public class Team {
    private Set<Worker> workersSet = new HashSet<>();
    private static int nextId;
    private int id = assignId();
    private String nameTeam;

    /**
     * Constructor creates an instance of team with the given name and workers.
     *
     * @param nameTeam name teem
     * @param workers  workers of team
     */

    public Team(String nameTeam, Worker... workers) {
        this.nameTeam = nameTeam;
        Collections.addAll(this.workersSet, workers);
    }

    public int getId() {
        return id;
    }

    /**
     * Method for obtaining all possible professional skills in numbers.
     *
     * @return mapping professional skills with quantity
     */

    public Map<ProfessionalSkills, Long> allSkillsMap() {
        return workersSet.stream()
                .flatMap(w -> w.getProfSkills().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private static int assignId() {
        int teamId = nextId;
        nextId++;
        return teamId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Team other = (Team) obj;
        return id == other.id;
    }
}
