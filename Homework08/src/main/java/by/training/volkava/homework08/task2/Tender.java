package by.training.volkava.homework08.task2;

import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * It is a tender to find a executor to meet the requirements.
 *
 * @author Hanna Wolkova
 */
public class Tender {
    private Map<ProfessionalSkills, Integer> requirement;
    private Map<Team, Double> requestMap = new HashMap<>();

    /**
     * Constructor create tender with requirement.
     *
     * @param requirement requirement to teams of builders
     */
    public Tender(Map<ProfessionalSkills, Integer> requirement) {
        if (Objects.isNull(requirement)) {
            throw new NullPointerException("Requirement must be not null");
        }
        this.requirement = new EnumMap<>(requirement);
    }

    public void submitFinancialProposal(Team team, double projectAmount) {
        requestMap.put(team, projectAmount);
    }

    /**
     * Show result of tender.
     */
    public void getResult() {
        Optional<Team> executor = findExecutor();
        if (executor.isPresent()) {
            System.out.println("Tender will be execute team with id " + executor.get().getId());
        } else {
            System.out.println("Tender has been closed");
        }
    }

    /**
     * Search for team to perform work meeting tender requirements
     * with the minimum cost of work.
     *
     * @return possible team if it exists
     */
    public Optional<Team> findExecutor() {
        return requestMap.keySet().stream()
                .filter(team -> satisfyRequirements.test(team))
                .collect(Collectors.toMap(Function.identity(), team -> (requestMap.get(team))))
                .entrySet().stream()
                .min(Comparator.comparingDouble(Map.Entry::getValue))
                .map(Map.Entry::getKey);
    }

    private Predicate<Team> satisfyRequirements = team -> {
        Map<ProfessionalSkills, Long> skillsTeam = team.allSkillsMap();
        boolean result = true;
        for (Map.Entry<ProfessionalSkills, Integer> entryRequirement :
                requirement.entrySet()) {
            if (!skillsTeam.containsKey(entryRequirement.getKey())) {
                result = false;
                break;
            }
            if (skillsTeam.get(entryRequirement.getKey())
                    < (entryRequirement.getValue())) {
                result = false;
                break;
            }
        }
        return result;
    };
}
