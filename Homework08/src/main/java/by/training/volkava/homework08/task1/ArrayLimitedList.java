package by.training.volkava.homework08.task1;

import java.util.Collection;
import java.util.Objects;

/**
 * Class ArrayLimitedList is LimitedList interface implementation
 * with a limited number of elements.
 *
 * @param <E> the type of item in list
 * @author Hanna Volkava
 */
public class ArrayLimitedList<E> implements LimitedList<E> {
    private Object[] array;
    private int size;

    /**
     * Constructor an empty list with the initial dimension.
     *
     * @param dimension dimension of list
     * @throws ArrayLimitedListException if dimension is negative
     */
    public ArrayLimitedList(int dimension) throws ArrayLimitedListException {
        if (dimension >= 0) {
            this.array = new Object[dimension];
        } else {
            throw new ArrayLimitedListException("Size of array can't be negative");
        }
    }

    /**
     * Constructor list on input collection.
     *
     * @param collection input collection
     * @throws ArrayLimitedListException when input collection is null
     */
    public ArrayLimitedList(Collection<? extends E> collection) throws ArrayLimitedListException {
        if (Objects.isNull(collection)) {
            throw new ArrayLimitedListException("Input collection is null");
        }
        array = collection.toArray();
        size = collection.size();
    }

    @Override
    public boolean add(E object) throws ArrayLimitedListException {
        if (isIncompliteArray()) {
            array[size++] = object;
            return true;
        } else {
            throw new ArrayLimitedListException("You can't add element "
                    + "in list because it is full");
        }
    }

    @Override
    public boolean add(int index, E object) throws ArrayLimitedListException {
        if (!(index >= 0 && index <= size)) {
            throw new ArrayLimitedListException("You cannot access element index of " + index);
        }
        if (isIncompliteArray()) {
            size++;
            for (int i = size - 1; i > index; i--) {
                array[i] = array[i - 1];
            }
            array[index] = object;
            return true;
        } else {
            throw new ArrayLimitedListException("You can't add element "
                    + " in list because it is full");
        }
    }

    @Override
    public boolean remove(E object) {
        for (int i = 0; i < size; i++) {
            if (object.equals(array[i])) {
                for (int j = i + 1; j < size; j++) {
                    array[j - 1] = array[j];
                }
                array[--size] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public E remove(int index) throws ArrayLimitedListException {
        rangeCheck(index);
        E result = (E) array[index];
        for (int i = index + 1; i < size; i++) {
            array[i - 1] = array[i];
        }
        array[size - 1] = null;
        size--;
        return result;

    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(E object) {
        for (Object element : array) {
            if (element.equals(object)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    /**
     * Replaces an item at a specified position in the list with a new item.
     *
     * @param index  index list
     * @param object object we will set
     * @return true if list changed
     * @throws ArrayLimitedListException if index not check range in list
     */
    @Override
    public boolean set(int index, E object) throws ArrayLimitedListException {
        rangeCheck(index);
        array[index] = object;
        return true;
    }

    @Override
    public int indexOf(Object object) {
        if (Objects.isNull(object)) {
            for (int i = 0; i < size; i++) {
                if (Objects.isNull(array[i])) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (object.equals(array[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    private boolean isIncompliteArray() {
        return size < array.length;
    }

    private void rangeCheck(int index) throws ArrayLimitedListException {
        if (index < 0 || index >= size) {
            throw new ArrayLimitedListException("You cannot access element index of " + index);
        }
    }
}
