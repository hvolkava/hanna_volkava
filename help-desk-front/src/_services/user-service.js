import authHeader from '../_helpers/auth-header';
import { saveAs } from 'file-saver';
import packageJson from '../../package.json';

export default class UserService {
    
    state = {
        _apiBase: packageJson.config.baseUrl
    }

    getResource = async (url, option) => {
        const res = await fetch(`${this.state._apiBase}${url}`, option);    
        if (!res.ok) {
          throw new Error(`Could not fetch ${url}, received ${res.status}`)
        }
        return await res.json();
    };

    deleteAttachment = async (url) => {
        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);
        const requestOptions = {
            method: "DELETE",
            headers: headers
        };
        const res = await fetch(`${url}`, requestOptions);  
        console.log("delete attachment ticket response", res)      
        if ( res.ok && res.status === 204) {
            return Promise.resolve();
        };
        const bodyErrors = await res.json();
        return Promise.reject( bodyErrors );
    }

    downloadAttachment = (url) => {
        const requestOptions = {
            headers: authHeader()
        };
        fetch(url, requestOptions)
                    .then(response => {
                        console.log(response);console.log(response.headers.get('Content-Disposition'));
                        const filename =  response.headers.get('Content-Disposition').split('filename=')[1];
                        response.blob().then(blob => saveAs(blob, filename));
                    });
      }

    getAllAttachmentsByTicketId = async (id) => {
        const requestOptions = {
            headers: authHeader()
        };
        const res = await this.getResource(`/tickets/${id}/attachments`, requestOptions);
        console.log(res);
        return res;
    };

    getAllHistoriesByTicketId = async (id) => {
        const requestOptions = {
            headers: authHeader()
        };
        const res = await this.getResource(`/tickets/${id}/histories`, requestOptions);
        console.log(res);
        return res;
    };

    getAllCommentsByTicketId = async (id) => {
        const requestOptions = {
            headers: authHeader()
        };
        const res = await this.getResource(`/tickets/${id}/comments`, requestOptions);
        console.log(res);
        return res;
    };

    getTicket = (id) => {
        const requestOptions = {
            headers: authHeader()
        };
        return fetch(`${this.state._apiBase}/tickets/${id}`, requestOptions)
        .then(res=>res.json());
        //return await res.json();
    };

    getFeedback = (url) => {
        const requestOptions = {
            headers: authHeader()
        };
        return fetch(`${url}`, requestOptions)
        .then(res=>{console.log(res); return res.json()});

    }

    changeState = async ( url, body) => {

        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);
        headers.append("Content-type", "application/json");
        const requestOptions = {
            method: "PATCH",
            headers: headers,
            body: body
        };
        console.log(requestOptions);
        const res = await fetch(url, requestOptions);

        if ( res.ok && res.status === 200) {
            console.log(res, res.headers);
            return Promise.resolve(res.headers);
        };
        console.log("Ticket can not change state", res);
        return Promise.reject();

    }

    createFeedback = (url, body) => {
        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);        
        headers.append("Content-type", "application/json");

        console.log(body);
        console.log(url);
        const requestOptions = {
            method: "POST",
            headers: headers,
            body: body
        };
        return fetch(`${url}`, requestOptions)
        .then(res=>{
        console.log("response from create feedback",res);
        if ( res.ok && res.status === 201) {
            return Promise.resolve();
        };
        console.log("Feedback not save, something wrong", res);
        return Promise.reject()})
    }

    createAttachment = (file, url) => {
        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);
        headers.append('Content-Length', file.size);
        const formData = new FormData();
        
        formData.append("file", file);
        const requestOptions = {
            method: "POST",
            headers: headers,
            body: formData
        };
        fetch(`${url}/attachments`, requestOptions)
        .then(res=>{
        console.log(res, res.headers);
        if ( res.ok && res.status === 201) {
            return Promise.resolve(res.headers);
        };
        console.log("Attachment not save, something wrong", res);
        return Promise.reject();
        })        
    }

    createComment = async (url, text) => {
        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);
        headers.append("Content-type", "application/json");
        const requestOptions = {
            method: "POST",
            headers: headers,
            body: JSON.stringify({comment: text})
        };
        const res = await fetch(`${url}/comments`, requestOptions);
        console.log(res, res.headers);
        if ( res.ok && res.status === 201) {
            return Promise.resolve(res.headers);
        };
        const errorM = await res.json();
        console.log("Comment not save, something wrong", errorM, res);
        return Promise.reject(errorM);
    }

    //TODO without async
    createTicket = async (body) => {
        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);
        headers.append("Content-type", "application/json");
        const requestOptions = {
            method: "POST",
            headers: headers,
            body: body
        };
        const res = await fetch(`${this.state._apiBase}${'/tickets'}`, requestOptions);
        
        if ( res.ok && res.status === 201) {
            return Promise.resolve(res.headers);
        };
        const bodyErrors = await res.json();
        return Promise.reject( bodyErrors );
    };

    updateTicket = async (body, url) => {
        let headers = new Headers();
        headers.append('Authorization', authHeader()['Authorization']);
        headers.append("Content-type", "application/json");
        const requestOptions = {
            method: "PUT",
            headers: headers,
            body: body
        };
        const res = await fetch(`${url}`, requestOptions);  
        console.log("update ticket response", res)      
        if ( res.ok && res.status === 200) {
            return Promise.resolve();
        };
        const bodyErrors = await res.json();
        return Promise.reject( bodyErrors );
    };
   
    getAllUrgencies = async () => {
        const requestOptions = {
            headers: authHeader()
        };
        const res = await this.getResource(`/tickets/urgencies`, requestOptions);
        console.log(res);
        return res;
    };

    getAllCategories = async () => {
        const requestOptions = {
            headers: authHeader()
        };
        const res = await this.getResource(`/categories`, requestOptions);
        console.log(res);
        return res;
    };

    getAllTickets = async () => {
        const requestOptions = {
            headers: authHeader()
        };
        const res = await this.getResource(`/tickets`, requestOptions);
        console.log(res);
        return res;
    };


    logout = async () => {
        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        const response = await fetch(`${this.state._apiBase}/logout`, requestOptions);
        console.log(response);
        localStorage.removeItem('user');
    };

    login = async (username, password) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: new URLSearchParams({ username, password })
        };
        const response = await fetch(`${this.state._apiBase}/login`, requestOptions);
        console.log(packageJson);
        if (response.ok) {
            const authdata = window.btoa(username + ':' + password);
            localStorage.setItem('user', authdata);
            console.log(response);
            console.log(authHeader());
            return Promise.resolve();
        }
        else {
            throw Error(`Request rejected with status ${response.status}`);
        };        
    };  

};


