import React, { Component } from 'react';

export default class AppHeader extends Component {

    render() {
        return (
        <nav className="navbar bg-primary justify-content-end d-flex">
            <a className="navbar-brand text-white mr-auto" href="#">HelpDesk</a>
            <button type="button"
                className="btn btn-link text-white navbar-item "
                onClick={this.props.onLogout}>Logout</button>
        </nav>);
    };
};