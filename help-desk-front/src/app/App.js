import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, withRouter } from 'react-router-dom';

import './App.css';
import LoginPage from '../login-page';
import TicketListPage from '../ticket-list-page';
import TicketEditPage from '../ticket-edit-page';
import UserService from '../_services';
import TicketOverview from '../ticket-overview-page';

class App extends Component {

  state = {
    isLoggedIn: false,
    userService: new UserService(),
    error: false
  };

  onLogin = (usermame, password) => {
    this.state.userService
    .login(usermame, password)
    .then(res=>{console.log(res);this.setState({isLoggedIn: true, error:false});this.props.history.push("/")})
    .catch(err =>{console.error('Caught error when trying log in: ', err);this.setState({error: true})});    
  };

  onLogout = () => {
    this.setState({
      isLoggedIn: false
    });
    this.state.userService.logout();
  };
 
  render() {

    const { isLoggedIn, error, userService } = this.state;
    return ( <div className="container">
      <Switch>                
        <Route
          path="/login"
          render={() => (<>
            
            <LoginPage
              isLoggedIn={isLoggedIn}
              onLogin={this.onLogin}
              err={error}/></>
          )}
        />
        <Route
          path="/" exact
          render={() => (
            <TicketListPage isLoggedIn={isLoggedIn} onLogout={this.onLogout} userService={userService} />
          )}
        />
        <Route
          path="/ticketCreate"
          render={() => (
            <TicketEditPage isLoggedIn={isLoggedIn} onLogout={this.onLogout} userService={userService} />
          )}
        />
        <Route
          path="/ticketOverview/:id" exact
          render={({ match }) => (
            <TicketOverview 
                      isLoggedIn={isLoggedIn} 
                      onLogout={this.onLogout} 
                      userService={userService} 
                      ticketId={match.params.id} 
                      history={this.props.history}/>
          ) }
        />
        <Route
          path="/ticketEdition/:id" exact
          render={({ match }) => (
            <TicketEditPage 
              isLoggedIn={isLoggedIn} 
              onLogout={this.onLogout} 
              userService={userService}
              ticketId={match.params.id}
              isEdition={true} />
          ) }
        />

        <Route render={() => <h2>Page not found</h2>} />
      </Switch>      </div>  
    );
  };
};

export default withRouter(App);