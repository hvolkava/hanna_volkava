import { Link } from "react-router-dom";
//import matchSorter from 'match-sorter';
//import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import React, { Component } from 'react';
import { SplitButton, Dropdown } from 'react-bootstrap';
import UserService from '../_services';
import Items from './items-action';

export const columns = [{
    Header: 'Id',
    accessor: 'id',
    sortable: true
  }, {
    Header: 'Name',
    accessor: 'name',
    sortable: true
  }, {
    Header: 'Desired Date',
    accessor: 'desiredDate',
    sortable: true
  }, {
    Header: 'Urgency',
    accessor: 'urgency',
    Cell: row => row.value!=undefined?<span>{row.value.displayName}</span>:<></>,
    sortable: true,
    sortMethod: (a, b) => {
      if (a==undefined) return -1;
      if (b == undefined) return 1;
      if (a.order === b.order) {
        return 0;
      }
      return a.order > b.order ? 1 : -1;
    }
  }, {
    Header: 'Status',
    accessor: 'state',
    sortable: true
  }, {
    id: 'actionList',
    Header: 'Action',
    accessor: (ticket) => {
      console.log(ticket);
      return ( ticket.actionList.length > 0 ? <SplitButton title="Choose action" variant='success' size="sm" >
        <Items ticket={ticket} update={this.update}/>
      </SplitButton> : <></> )
    },
    style: {overflow: 'visible' },
    sortable: false
    
  }
]
  