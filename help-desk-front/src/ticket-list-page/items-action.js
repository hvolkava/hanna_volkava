import React, { Component } from 'react';
import { SplitButton, Dropdown } from 'react-bootstrap';
import UserService from '../_services';

export default class ItemsAction extends Component {
    render() {
      const { ticket } = this.props
      console.log("Action list", ticket.actionList);
      return ticket.actionList.map( (action) =>
        <Dropdown.Item 
          onClick={()=>{
            console.log(ticket.links[0].href, action);
            const service = new UserService();
            service.changeState(ticket.links[0].href, JSON.stringify({ "state": action.nextState }))
            .then(this.props.update);
          }}
          eventKey={action.displayName}
          key={action.displayName}>
            {action.displayName}
        </Dropdown.Item>);
    }
};