import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import AppHeader from '../app-header';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import './ticket-list-page.css';
import { SplitButton } from 'react-bootstrap';
import Items from './items-action';

export default class TicketListPage extends Component {

    state = {
        tickets: [],
        search: '',
        filterMyTickets: false,
        isCreateTicket: true
    };

    update = () => {
        this.props.userService.getAllTickets()
        .then(json => {
            const isCreateTicket = json.links.filter(item=>item.rel=='createTicket').length > 0 ? true : false;
            this.setState({tickets: json.tickets, isCreateTicket: isCreateTicket})})
        .catch(error => console.error('Error:', error));
    };

    componentDidMount() {
        console.log(this.props);
        this.update();
    };

    filterTickets(tickets, filter) {
        if (!filter) {
            return tickets;
        } else {
            return tickets.filter(ticket => ticket.isMineTicket);
        } 
    };

    searchTickets(tickets, search) {
        if (search.length === 0) {
            return tickets;
        }
        return tickets.filter((ticket) => {
            return String(ticket.id).indexOf(search) > -1 ||
                ticket.name.indexOf(search) > -1 ||
                ticket.desiredDate.indexOf(search) > -1 ||
                ticket.urgency.displayName.indexOf(search) > -1 ||
                ticket.state.indexOf(search) > -1;
        });
    };

    render () {

        const { isLoggedIn, onLogout } = this.props;
        const { tickets, search, filterMyTickets, isCreateTicket  } = this.state;
        const visibleTickets = this.searchTickets(this.filterTickets(tickets, filterMyTickets), search);
        const col = [{
            Header: 'Id',
            accessor: 'ticketId',
            sortable: true,
            className: "col-1 text-center",
            headerClassName: 'col-1'
        }, {
            id: 'name',
            Header: 'Name',
            accessor: (ticket) => {return <Link to={`/ticketOverview/${ticket.ticketId}`}>{ticket.name}</Link>},
            sortable: true,
            className: "col-3",
            headerClassName: 'col-3'
        }, {
            Header: 'Desired Date',
            accessor: 'desiredDate',
            sortable: true,
            className: "col-2 text-center",
            headerClassName: 'col-2'
        }, {
            id: 'urgency',
            Header: 'Urgency',
            accessor: 'urgency',
            Cell: row => <span>{row.value.displayName}</span>,
            sortable: true,
            sortMethod: (a, b) => {
                if (a.order === b.order) return 0;
                return a.order > b.order ? 1 : -1;
            }
        }, {
            id: 'status',
            Header: 'Status',
            accessor: 'state',
            sortable: true
        }, {
            id: 'actionList',
            Header: 'Action',
            accessor: (ticket) => {
                console.log(ticket);
                return ( ticket.actionList.length > 0 ? 
                    <SplitButton title="Choose action" variant='success' size="sm" >
                        <Items ticket={ticket} update={this.update}/>
                    </SplitButton> : <></> )
            },
            style: {overflow: 'visible' },
            sortable: false      
        } ];

        const caption = !filterMyTickets ? "All Tickets" : "My Tickets";

        if (isLoggedIn) {    
            return (
                <div className="container justify-content-center">
                    <AppHeader onLogout={onLogout} />
                    <h3 className="display-10 m-4">{caption}</h3>
                    <div className="d-flex mb-4">
                    {isCreateTicket? <button className="btn btn-primary col-4" onClick={()=>{this.props.history.push("/ticketCreate")}} >Create New Ticket</button> : <></>}
                    </div>
                    <button 
                        className={"btn col-6 mb-4 " + (filterMyTickets?"btn-outline-secondary":"btn-info")}
                        onClick={e => this.setState({filterMyTickets: false})} >All Tickets</button>
                    <button 
                        className={"btn col-6 mb-4 " + (!filterMyTickets?"btn-outline-secondary":"btn-info")}
                        onClick={e => this.setState({filterMyTickets: true})} >My Tickets</button>          
                    <input type="text"
                        className="form-control col-4 mb-4"
                        value={this.state.search}
                        onChange={e => {
                            let value = e.target.value;
                            value = value.replace(/[^\x20-\x7F]+/ig, "")
                            this.setState({search: value})}
                        }
                    />
                    <ReactTable
                        update={this.update}
                        data={visibleTickets}
                        columns={col}
                        defaultSorted={[
                        {
                            id: 'urgency',
                            asc: true
                        },
                        {
                            id: 'desiredDate',
                            desc: true
                        }] }            
                        className="-striped -highlight"    />
                </div>
            );
        }
        return <Redirect to="/login" />;
    };
};