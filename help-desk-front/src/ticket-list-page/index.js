import TicketListPage from './ticket-list-page';
import {withRouter} from 'react-router-dom';

export default withRouter(TicketListPage);