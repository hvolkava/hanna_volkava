import React, { Component } from 'react';
import { saveAs } from 'file-saver';

export default class FilesList extends Component {
    render() {
        const listItem = (fileList) => {
            const files = Array.from(fileList);
            return files.map( (file, i) => 
            <li               
                key={i}>
                <span className="btn-link"
                    onClick={(e)=>{
                        e.preventDefault();
                        const blob = new Blob([file], {type: file.type});
                        saveAs(blob, file.name);}} >
                    {file.name}</span>
                    <button onClick={(e)=>{
                        e.preventDefault()
                        this.props.onDeleteFiles(i)}} >Delete</button>
            </li>)};

      const { files } = this.props
      console.log("files list", files);
      return <ul className="col-9 " > {listItem(files)} </ul>; 
    }
};