import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import moment from 'moment';
import { Redirect } from 'react-router-dom';
import AppHeader from '../app-header';
import AttachmentsList from './attachments-list';
import FilesList from './files-list';
 
import "react-datepicker/dist/react-datepicker.css";

export default class TicketEditPage extends Component {
    state = {
        categories: [],
        category: '',
        name: '',
        description: '',
        urgencies: [],
        urgency: '',
        desiredDate: '',
        files: [],
        attachmentsForDelete: [],
        attachmentsOnServer: [],
        comment: '',
        error: '',
        isEdition: false,
        ticket: {}
    };

    componentDidMount = () => {
        const { ticketId, userService } = this.props;

        Promise.all([this.props.userService.getAllCategories()
                        .then(json => {
                            const options = json.map(function(cat) { return {value: cat.id, label:cat.name}});
                            this.setState({categories: options})
                        })
                        .catch(error => console.error('Error:', error))   ,        
                        this.props.userService.getAllUrgencies()
                        .then(json => {
                            const options = json.map(function(urg){ return {value: urg.order, label: urg.displayName}});
                            this.setState({urgencies: options})
                        })
                        .catch(error => console.error('Error:', error))])
        .then(()=>{
            if (this.props.isEdition) {
                this.setState({isEdition: true})
                userService.getTicket(ticketId)
                .then(ticket=> {this.setState({ ticket: ticket});
                    const indexCategory = this.state.categories.findIndex(cat => cat.value ===ticket['category']['id']);
                    const indexUrgency = this.state.urgencies.findIndex(urg => urg.value ===ticket['urgency']['order']);
                    
                    this.setState({ urgency: this.state.urgencies[indexUrgency], category: this.state.categories[indexCategory], 
                    name: ticket.name, description: ticket.description, desiredDate: moment(ticket.desiredDate, 'DD/MM/YYYY').toDate()})
                    userService.getAllAttachmentsByTicketId(ticketId)
                    .then(attachments => {
                        this.setState({ attachmentsOnServer: attachments });
                        console.log(this.state.attachments);
                    });
                });
            }
        });
    };

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    };

    handleFileChange = (e) => {
        console.log(e.target.files);
        const files = this.state.files.concat(Array.from(e.target.files));
        this.setState({ files: files });
    }

    handleCategoryChange = (category) => {
        this.setState({ category });
    };

    handleUrgencyChange = (urgency) => {
        this.setState({ urgency });
    };

    handleDesiredDateChange = (date) => {
       
        this.setState({ desiredDate: date });
    };

    createTicket = (state) => {
        const date = moment(this.state.desiredDate).format('DD/MM/YYYY');
        const ticket = {
            category: {id: this.state.category.value},
            name: this.state.name,
            description: this.state.description,
            urgency: this.state.urgency.label.toUpperCase(),
            desiredDate: date,
            state: state
        };

        this.props.userService.createTicket(JSON.stringify(ticket))
        .then(headers => {
            const url = headers.get('Location');
            if (this.state.comment) {
                this.props.userService.createComment(url, this.state.comment);
            }
            if (this.state.files.length>0) {
                const promises = [];
                this.state.files.map(file => { 
                    promises.push(this.props.userService.createAttachment(file,url)) });
                Promise.all(promises)
                .then((res)=>{console.log("attachment res",res)})
                .catch((error)=>{console.log("attachment error",error);});
            }            
            this.props.history.push("/");
        })
        .catch(json => {console.log(JSON.stringify(json)); alert(json.errors);});
    }

    updateTicket = (state) => {
        const isEditTicket = this.state.ticket.name != this.state.name ||
            this.state.ticket.category.id != this.state.category.value ||
            this.state.ticket.description != this.state.description ||
            this.state.ticket.urgency.displayName.toUpperCase() != this.state.urgency.label.toUpperCase() ||
            this.state.ticket.desiredDate != moment(this.state.desiredDate).format('DD/MM/YYYY') ||
            state != undefined;
            console.log("isEditTicket", isEditTicket);
        let ticket = {
            category: {id: this.state.category.value},
            name: this.state.name,
            description: this.state.description,
            urgency: this.state.urgency.label.toUpperCase(),
            desiredDate: moment(this.state.desiredDate).format('DD/MM/YYYY'),
            state: this.state.ticket.state
        };

        if (state != undefined) ticket = { ...ticket, state};
        const url = this.state.ticket.links[0].href;

        if (isEditTicket) {
            this.props.userService.updateTicket(JSON.stringify(ticket), url)
            .then(() => {
                Promise.all([this.createCommentProm(url), this.createAttachmentsProm(url), this.deleteAttachmentProm()])
                .then(() => this.props.history.push("/ticketOverview/" + this.state.ticket.ticketId))
                .catch(err => {console.log(JSON.stringify(err)); alert("rep "+err.errors);});
            })
            .catch(json => {console.log(JSON.stringify(json)); alert("rep "+json.errors);});
        } else {
            Promise.all([this.createCommentProm(url), this.createAttachmentsProm(url), this.deleteAttachmentProm()])
                .then(() => this.props.history.push("/ticketOverview/" + this.state.ticket.ticketId))
                .catch(err => {console.log(JSON.stringify(err)); alert("rep "+err.errors);});
        }
    }

    createCommentProm = (url) => {
        if (this.state.comment) {
            return this.props.userService.createComment(url, this.state.comment);
        }
        return Promise.resolve();
    }

    deleteAttachmentProm = () => {
        if (this.state.attachmentsForDelete.length > 0) {
            const promises = [];
            this.state.attachmentsForDelete.map(attach => {
                promises.push(this.props.userService.deleteAttachment(attach.links[0].href))
            });
            return Promise.all(promises);
        }
        return Promise.resolve(); 
    }
    createAttachmentsProm = (url) => {
        if (this.state.files.length > 0) {
            const promises = [];
            this.state.files.map(file => { 
                    promises.push(this.props.userService.createAttachment(file,url)) });
            return Promise.all(promises);
        }
        return Promise.resolve(); 
    }

    handleDraftSubmit = (e) => {
        e.preventDefault();
        if (this.state.isEdition) {
            this.updateTicket();
        } else {
            const state = "Draft";
            this.createTicket(state);
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();     
        if (this.state.isEdition) {
            let state;
            if (this.state.ticket.state === "Draft") {
                state = "New";
            }
            this.updateTicket(state);
        } else {
            const state = "New";
            this.createTicket(state);
        }
    };

    onDelete = (id) => {  
        this.setState((state) => {
            const idx = state.attachmentsOnServer.findIndex((item) => item.attachmentId === id);
            const attachmentsForDelete = [ ...state.attachmentsForDelete, state.attachmentsOnServer[idx] ];
            const attachmentsOnServer = [ ...state.attachmentsOnServer.slice(0, idx), ...state.attachmentsOnServer.slice(idx + 1) ];
            return { attachmentsOnServer, attachmentsForDelete };
        });
    }

    onDeleteFiles = (idx) => {  
        this.setState((state) => {
            const files = [ ...state.files.slice(0, idx), ...state.files.slice(idx + 1) ];
            return { files };
        });
    }

    render() {
        const { isLoggedIn, onLogout, userService } = this.props;
        const { category, name, description, urgency, desiredDate, comment, error, categories, urgencies, isEdition, ticket } = this.state;
        const validForSubmit = category === "" || name === "" || desiredDate ==="" || urgency ==="" ? true : false;
        const validForDraft = validForSubmit || (isEdition == true && ticket.state !="Draft") ? true : false;
        const header = isEdition ? "Edit Ticket" : "Create New Ticket";

        console.log(validForDraft,validForSubmit,ticket.state)
        
        if (isLoggedIn) { 
            return (            
                <div className="container justify-content-center">
                    <AppHeader onLogout={onLogout} /> 
                    {!isEdition ?
                        <button className="btn btn-primary mt-4" onClick={()=>{this.props.history.push("/")}} >Ticket List</button> :
                        <button className="btn btn-primary mt-4" onClick={()=>{this.props.history.push("/ticketOverview/" + this.state.ticket.ticketId)}} >Ticket Overview</button>
                    }
                    <h3 className="display-10 m-4">{header}</h3>
                    <form name="form" >
                        <div className="row mb-4">
                            <label htmlFor="category" className="col-3 col-form-label text-right">Category*</label>
                            <Select className="col-4" name="category"
                                options={categories}
                                value={category}                                
                                onChange={this.handleCategoryChange}
                            />
                        </div>

                        <div className="row mb-4">
                            <label htmlFor="name" className="col-3 col-form-label text-right">Name*</label>
                            <div className="col-9">
                                <input type="text" className="form-control" 
                                    name="name" value={name} onChange={e => {
                                        let value = e.target.value;
                                        value = value.replace(/[^\x20-\x40\x5B-\x7F]+/g, "")
                                        if (value.length <= 100) this.setState({[e.target.name]: value})}
                                    } />
                            </div>
                        </div>

                        <div className="row mb-4">
                            <label htmlFor="decsription" className="col-3 col-form-label text-right">Description</label>
                            <div className="col-9">
                                <input type="text" className="form-control" 
                                    name="description" value={description} onChange={e => {
                                        let value = e.target.value;
                                        value = value.replace(/[^\x20-\x7F]+/ig, "")
                                        if (value.length <= 500) this.setState({[e.target.name]: value})}
                                    } />
                            </div>
                        </div>

                        <div className="row mb-4">
                            <label htmlFor="urgency" className="col-3 col-form-label text-right">Urgency*</label>
                            <Select className="col-4" name="urgency"
                                options={urgencies}
                                value={urgency}
                                onChange={this.handleUrgencyChange}
                            />
                        </div>

                        <div className="row mb-4">
                            <label htmlFor="desiredDate" className="col-3 col-form-label text-right">Desired resolution date*</label>
                            <div className="col-9">
                                <DatePicker className="form-control"
                                    minDate={new Date()}
                                    dateFormat='dd/MM/yyyy'
                                    selected={this.state.desiredDate}
                                    onChange={this.handleDesiredDateChange}
                                />
                            </div>
                        </div>

                        <div className="row mb-4">
                            <label htmlFor="files" className="col-3 col-form-label text-right">Attachments</label>
                            <div className="col-9 p-0" >
                                
                                
                                <input type="file" className="col-9 custom-file "  id="files"
                                    name="files[]"  onChange={this.handleFileChange} multiple 
                                    accept=".doc,.docx,.pdf,.jpg,.jpeg,.png" />
                                {isEdition ? <AttachmentsList
                                                userService={userService}
                                                attachments={this.state.attachmentsOnServer}
                                                onDelete={this.onDelete}/>: <></>}
                                {this.state.files.length > 0 ? <FilesList
                                                userService={userService}
                                                files={this.state.files}
                                                onDeleteFiles={this.onDeleteFiles}/>: <></>}
                            </div>                            
                        </div>

                        <div className="row mb-4">
                            <label htmlFor="comment" className="col-3 col-form-label text-right">Comment</label>
                            <div className="col-9">
                                <input type="text" className="form-control" 
                                    name="comment" value={comment} onChange={e => {
                                        let value = e.target.value;
                                        value = value.replace(/[^\x20-\x7F]+/ig, "")
                                        if (value.length <= 500) this.setState({[e.target.name]: value})}
                                    } />
                            </div>
                        </div>
                        
                        <div className="d-flex justify-content-end">
                            <button className="btn btn-secondary " onClick={this.handleDraftSubmit}  disabled={validForDraft} >Save as Draft</button>
                            <button className="btn btn-primary " onClick={this.handleSubmit} disabled={validForSubmit}>Submit</button>
                        </div>
                        
                    </form>
                </div>
            );
        } else {
            return <Redirect to="/login" />;
        }
    };
};