import React, { Component } from 'react';

export default class AttachmentsList extends Component {
    render() {
        const listItem = (attachments) => {return attachments.map( (attachment, i) =>
            <li               
                key={i}>
                <span className="btn-link"
                    onClick={()=>{this.props.userService.downloadAttachment(attachment.links[0].href)}} >
                    {attachment.name}</span>
                    <button onClick={(e)=>{
                        e.preventDefault()
                        this.props.onDelete(attachment['attachmentId'])}} >Delete</button>
            </li>
        )}

      const { attachments } = this.props
      console.log("attachments list", attachments);
      return <ul className="col-9 " > {listItem(attachments)} </ul>
    }
};