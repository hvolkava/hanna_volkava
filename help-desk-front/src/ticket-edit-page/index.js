import TicketEditPage from './ticket-edit-page';
import { withRouter } from 'react-router-dom';

export default withRouter(TicketEditPage);