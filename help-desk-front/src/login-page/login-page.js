import React, { Component } from 'react';
import UserService from '../_services';

export default class LoginPage extends Component {
    state = {
        username: '',
        password: '',
        error: '',
        userService: new UserService()
    };

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { username, password } = this.state;
        this.props.onLogin(username, password);   
    };

    render() {
        const { username, password, error } = this.state;
        const { err } = this.props;
        const validForLogin = (username.match(/^([\w\W]+)@([\w\W]+\.)+([\w]{2,})$/i) && username.length <= 100) &&
         (password.match(/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W]).*$/) && password.length >= 6 && password.length <= 20);
        return (
            <div className="d-flex flex-row vh-100 align-content-center flex-wrap">            
                <h2 className="col-12 text-center">Login to the Help Desk</h2>
                <form name="form" onSubmit={this.handleSubmit} className="col-12 align-content-center">
                    <div className="d-flex m-2">
                        <label htmlFor="username" className="col-3 col-form-label text-right">User Name:</label>
                        <div className="col-9">
                            <input type="text" className="form-control" 
                                name="username" value={username} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="d-flex m-2">
                        <label htmlFor="password" className="col-3 col-form-label text-right">Password:</label>
                        <div className="col-9">
                            <input type="password" className="form-control" 
                                name="password" value={password} onChange={this.handleChange} />
                            {(error||err) &&
                                <div className={'mt-2 alert alert-danger '}>Please make sure you are using a valid email or password</div>
                            }
                        </div>
                    </div>
                    <div className="d-flex justify-content-end">
                        <button className="btn btn-primary mr-4 " disabled={!validForLogin} >Login</button>
                    </div>
                    
                    
                </form>
            </div>
        );
    };
};