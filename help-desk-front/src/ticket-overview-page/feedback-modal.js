import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import Rating from 'react-rating';
import './feedback-modal.css';

export default class FeedbackModal extends React.Component {
    state = {
        rate: 0,
        text: '', 
        isCreate: true
    }

    update = () => {
        const { ticket, userService, isCreate } = this.props;
        this.setState({ isCreate });

        if (!isCreate) {
            if ( this.props.ticket.links) {
                const url = this.props.ticket.links.filter(item=>item.rel=='viewFeedback')[0].href;
                userService.getFeedback(url)
                .then(feedback=> {
                    this.setState({ rate: feedback.rate, text: feedback.text });
                });     
            }
        }
    };

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    handleChangeRate = (rating) => {
        this.setState({ rate: rating });
    } 

    handleSubmit = (e) => {
        e.preventDefault();
        const url = this.props.ticket.links.filter(item=>item.rel=='leaveFeedback')[0].href;
        const body = JSON.stringify({rate: this.state.rate, text: this.state.text});
        this.props.userService.createFeedback(url, body)
        .then(res=>{  this.props.onSubmit();
                      this.props.onHide();
                    })
        .catch(err => console.log(err));
    }

    render() {
      return (
        <Modal
            show={this.props.show}
            onHide={this.props.onHide}
            onShow={this.update}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Ticket({this.props.ticket.ticketId}) - {this.props.ticket.name}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>Please, rate your satisfaction with solution:</p>
                <Rating
                    emptySymbol="fa fa-star-o fa-4x"
                    fullSymbol={["fa fa-star fa-4x color-yellow"]}
                    initialRating={this.state.rate}
                    onChange={this.handleChangeRate}
                    readonly={!this.props.isCreate} />

                <textarea 
                    className="form-control"
                    name="text" rows="6"
                    value={this.state.text}
                    onChange={this.handleChange}
                    disabled={!this.props.isCreate}></textarea>
            </Modal.Body>
            { this.props.isCreate ? 
                <Modal.Footer>                
                    <Button onClick={this.handleSubmit} disabled={this.state.rate==0}>Submit</Button>
                </Modal.Footer> : <></> }
        </Modal>
      );
    }
  }