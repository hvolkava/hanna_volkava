import React, { Component } from 'react';

export default class ListAttachments extends Component {
    render() {
        const listItem = (attachments) => {return attachments.map( (attachment, i) =>
            <li className="btn-link"
                onClick={()=>{this.props.userService.downloadAttachment(attachment.links[0].href)}} 
                key={i}>
                {attachment.name}
            </li>)};

      const { attachments } = this.props
      console.log("attachments list", attachments);
      return <ul> {listItem(attachments)} </ul>
    }
};