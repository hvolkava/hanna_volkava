import React from 'react';
import moment from 'moment';

const columnsHistories = [
    {
        id: 'dateTime',
        Header: 'Date',
        accessor: (row) => moment(row.dateTime, 'DD/MM/YYYY HH:mm:ss').format('MMM DD, YYYY HH:mm:ss'),
        sortMethod: (a, b) => {
            if (moment(a, 'MMM DD, YYYY HH:mm:ss') === moment(b, 'MMM DD, YYYY HH:mm:ss')) return 0;
            return moment(a, 'MMM DD, YYYY HH:mm:ss') > moment(b, 'MMM DD, YYYY HH:mm:ss') ? 1 : -1;
        },
        className: "col-2 text-center",
        headerClassName: 'col-2'
    }, {
        Header: 'User',
        accessor: 'username',
        className: "col-4",
        headerClassName: 'col-4'
    }, {
        Header: 'Action',
        accessor: 'action',
        className: "col-2",
        headerClassName: 'col-2'
    }, {
        Header: 'Description',
        accessor: 'description',
        className: "col-4",
        headerClassName: 'col-4'
    }
];
export default columnsHistories;
  