import React, { Component } from 'react';
import { Redirect, Link, withRouter } from 'react-router-dom';
import AppHeader from '../app-header';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import columnsComments from './columns-comments';
import columnsHistories from './columns-histories';
import ListAttachments from './list-attacments';
import FeedbackModal from './feedback-modal';

class TicketOverview extends Component {

    state = {
        comments: [],
        histories: [],
        attachments: [],
        ticket: {},
        dataSource: 'histories',
        defaultPageSize: 5, 
        isEditTicket: false,
        leaveFeedback: false,
        viewFeedback: false,
        modalShow: false, 
        comment: ''
    };

    componentDidMount() {
        const { ticketId, userService } = this.props;
        userService.getTicket(ticketId)
        .then(ticket=> {
            console.log("Ticket", ticket);
            const isEditTicket = ticket.links.filter(item=>item.rel=='editTicket').length > 0 ? true : false;
            const leaveFeedback = ticket.links.filter(item=>item.rel=='leaveFeedback').length > 0 ? true : false;
            const viewFeedback = ticket.links.filter(item=>item.rel=='viewFeedback').length > 0 ? true : false;
            this.setState({ ticket, isEditTicket, leaveFeedback, viewFeedback })});
        userService.getAllCommentsByTicketId(ticketId)
        .then(comments => {
            this.setState({ comments: comments });
            console.log(this.state.comments);
        });

        userService.getAllHistoriesByTicketId(ticketId)
        .then(histories => {
            this.setState({ histories: histories });
            console.log(this.state.histories);
        });

        userService.getAllAttachmentsByTicketId(ticketId)
        .then(attachments => {
            this.setState({ attachments: attachments });
            console.log(this.state.attachments);
        });              
    };

    updateRowSize = (size) => {
        console.log(size);
        this.setState({ defaultPageSize: size });
        console.log(this.state.defaultPageSize);
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    };

    updateAction = () => {
        const ticket = this.state.ticket;
        const idx = ticket.links.findIndex((item) => item.rel === 'leaveFeedback');
        ticket.links[idx].rel = 'viewFeedback';
        this.setState({ ticket: ticket, leaveFeedback: false, viewFeedback:true });
    }

    createComment = (ticket) => {
        const url = ticket.links.filter(item=>item.rel=='self')[0].href;
        if (this.state.comment) {
            this.props.userService.createComment(url, this.state.comment)
            .then(()=>{
                this.props.userService.getAllCommentsByTicketId(ticket.ticketId)
                .then(comments => {
                    this.setState({ comments: comments });
                    console.log(this.state.comments);
                });
                this.setState({ comment: "" });
            });
        }
    }

    render () {

        const { isLoggedIn, onLogout, history, userService } = this.props;
        const { dataSource, comments, histories, ticket, attachments, defaultPageSize, 
            isEditTicket, leaveFeedback, viewFeedback, comment } = this.state;
        let modalClose = () => this.setState({ modalShow: false });

        const renderCategory = (ticket) => {
            return Object.keys(ticket).filter((key)=>{return key === "category"}).map((category, i) => {
                return (<span key={i}>{ticket[category].name}</span>)
            })
        }

        const renderUrgency = (ticket) => {
            return Object.keys(ticket).filter((key)=>{return key === "urgency"}).map((urgency, i) => {
                return (<span key={i}>{ticket[urgency].displayName}</span>)
            })
        }

        const data = dataSource === 'comments' ? comments : histories;
        const columns = dataSource === 'comments' ? columnsComments : columnsHistories;
        const isWork = defaultPageSize < data.length ? true : false;

        if (isLoggedIn) {    
            return <div className="container ">
                    <AppHeader onLogout={onLogout} />
                    <div className="d-flex align-items-center mt-4">
                        <button className="btn btn-primary " onClick={()=>{this.props.history.push("/")}} >Ticket List</button>
                        <h3 className="display-10 ml-4 mr-auto">{ticket.name}</h3>
                        { isEditTicket ? <button className="btn btn-primary " onClick={() => {this.props.history.push(`/ticketEdition/${this.props.ticketId}`)}} >Edit</button> : <></> }
                        { leaveFeedback ? <button className="btn btn-primary " onClick={() => this.setState({ modalShow: true })} >Leave Feedback</button> : <></> }
                        { viewFeedback ? <button className="btn btn-primary " onClick={() => this.setState({ modalShow: true })} >View Feedback</button> : <></> }

                    </div>
                    
                    <div className="row">
                        <label htmlFor="createdOnDate" className="col-3 col-form-label text-right">Created on:</label>
                        <label id="createdOnDate" className="col-3 col-form-label mr-auto">{ticket.createdOnDate}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="state" className="col-3 col-form-label text-right">Status:</label>
                        <label id="state" className="col-3 col-form-label">{ticket.state}</label>

                        <label htmlFor="category" className=" col-form-label text-right">Category:</label>
                        <label id="category" className="col-3 col-form-label">{renderCategory(ticket)}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="urgency" className="col-3 col-form-label text-right">Urgency:</label>
                        <label id="urgency" className="col-9 col-form-label">{renderUrgency(ticket)}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="desiredDate" className="col-3 col-form-label text-right">Desired resolution date:</label>
                        <label id="desiredDate" className="col-9 col-form-label">{ticket.desiredDate}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="ownerName" className="col-3 col-form-label text-right">Owner:</label>
                        <label id="ownerName" className="col-9 col-form-label">{ticket.ownerName}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="approverName" className="col-3 col-form-label text-right">Approver:</label>
                        <label id="approverName" className="col-9 col-form-label">{ticket.approverName}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="assigneeName" className="col-3 col-form-label text-right">Assignee:</label>
                        <label id="assigneeName" className="col-9 col-form-label">{ticket.assigneeName}</label>
                    </div>
                    <div className="row">
                        <label htmlFor="attachments" className="col-3 col-form-label text-right">Attachments:</label>
                        <label id="attachments" className="col-9 col-form-label">
                            <ListAttachments attachments={attachments} userService={userService}/>
                        </label>
                    </div>
                    <div className="row mb-4">
                        <label htmlFor="description" className="col-3 col-form-label text-right">Description:</label>
                        <label id="description" className="col-9 col-form-label">{ticket.description}</label>
                    </div>
                    <button 
                        className={"btn col-6 " + (dataSource != 'histories' ?"btn-outline-secondary":"btn-info")}
                        onClick={() => this.setState({dataSource: 'histories', defaultPageSize: 5})} >History</button>
                    <button 
                        className={"btn col-6 " + (dataSource != 'comments' ? "btn-outline-secondary":"btn-info")}
                        onClick={() => this.setState({dataSource: 'comments', defaultPageSize: 5})} >Comments</button>
                    
                    <button type="button" disabled={!isWork}
                            className="btn btn-link "
                            onClick={()=>this.setState({ defaultPageSize: data.length })}>Show all</button> 
                    <ReactTable
                        data={data}
                        columns = {columns}
                        pageSize = {defaultPageSize}
                        sortable ={false}
                        className="-striped -highlight"
                        showPagination={false}
                        showPageSizeOptions={false} 
                        defaultSorted={[{ id: 'dateTime', desc: true }]}   />

                    <div className="d-flex m-4">
                        <label htmlFor="comment" className="col-2 col-form-label text-right">Comment</label>
                        
                            <input type="text" className="form-control col-7 mr-2" 
                                name="comment" value={comment} onChange={this.handleChange} />
                            <button type="button" disabled={!comment}
                                className="btn btn-primary"
                                onClick={()=>this.createComment(ticket)}>Add comment</button>
                        
                    </div>
                    <FeedbackModal 
                        show={this.state.modalShow}
                        onHide={modalClose} 
                        ticket={this.state.ticket} 
                        isCreate={leaveFeedback}
                        userService={userService} 
                        onSubmit={this.updateAction} />                    
                </div>
        }   
        return <Redirect to="/login" />;
    };
};

export default (TicketOverview);