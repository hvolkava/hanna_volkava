export default function authHeader(other) {
    // return authorization header with basic auth credentials
    let user = localStorage.getItem('user');

    if (user) {
        if (other!=undefined) {
            return { 'Authorization': 'Basic ' + user, other };
        } else {
            return { 'Authorization': 'Basic ' + user };
        }
    } else {
        return {};
    }
}