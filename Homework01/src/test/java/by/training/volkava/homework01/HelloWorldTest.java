package by.training.volkava.homework01;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Test HelloWorld class.
 *
 * @author Hanna_Volkava
 */
public class HelloWorldTest {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(null);
    }

    @Test
    public void testMain() {
        String expected = "Hello, World!";
        HelloWorld.main(new String[0]);
        String actual = outContent.toString();
        assertEquals(expected, actual);
    }
}