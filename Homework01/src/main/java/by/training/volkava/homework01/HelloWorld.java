package by.training.volkava.homework01;

/**
 * Main application class  that displays in the console "Hello, World!".
 *
 * @author Hanna_Volkava
 */
public class HelloWorld {

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        System.out.print("Hello, World!");
    }
}
