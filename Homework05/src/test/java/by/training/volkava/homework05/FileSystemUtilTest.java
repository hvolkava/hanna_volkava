package by.training.volkava.homework05;

import org.junit.Test;

public class FileSystemUtilTest {

    @Test(expected = FileSystemException.class)
    public void parseString_WithFile() throws FileSystemException {
        FileSystem fileSystem = new File("Test.exe");
        FileSystemUtil.parseString(fileSystem, "Folder");
    }
    @Test(expected = FileSystemException.class)
    public void parseString_WithNull() throws FileSystemException {
        FileSystem fileSystem = null;
        FileSystemUtil.parseString(fileSystem, "Folder");
    }
}