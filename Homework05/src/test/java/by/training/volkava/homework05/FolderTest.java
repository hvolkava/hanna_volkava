package by.training.volkava.homework05;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FolderTest {

    @Test
    public void testGetChildObjects_WithoutChild() throws FileSystemException {
        Folder folder = new Folder("Folder");
        List<FileSystem> list = folder.getChildObjects();
        Assert.assertEquals(0, list.size());
    }
    @Test
    public void testAdd_WithExistObject() throws FileSystemException {
        Folder parentFolder = new Folder("Test");
        Folder childFolder = new Folder("Child");
        parentFolder.add(childFolder);
        Assert.assertFalse(parentFolder.add(childFolder));
    }
    @Test
    public void testAdd_WithNotExistObject() throws FileSystemException {
        Folder parentFolder = new Folder("Test");
        Folder childFolder = new Folder("Child");
        Assert.assertTrue(parentFolder.add(childFolder));
    }
    @Test
    public void testGetFolderByName_WithExistFolder() throws FileSystemException {
        Folder parentFolder = new Folder("Test");
        Folder expectedChildFolder = new Folder("Child");
        parentFolder.add(expectedChildFolder);
        FileSystem actualFolder = parentFolder.getFolderByName("Child");
        Assert.assertEquals(expectedChildFolder, actualFolder);
    }
    @Test
    public void testGetFolderByName_WithoutExistFolder() throws FileSystemException {
        Folder expectedFolder = new Folder("Test");
        FileSystem actualFolder = expectedFolder.getFolderByName("Folder");
        Assert.assertEquals(expectedFolder, actualFolder);
    }
    @Test
    public void testGetName() throws FileSystemException {
        Folder folder = new Folder("Test");
        String expected = "Test";
        Assert.assertEquals(expected, folder.getName());
    }

    @Test(expected = FileSystemException.class)
    public void testFolderConstructor_WithEmptyName() throws FileSystemException {
        Folder folder = new Folder("");
    }

    @Test(expected = FileSystemException.class)
    public void testFolderConstructor_WithNull() throws FileSystemException {
        String name = null;
        Folder folder = new Folder(name);
    }
}