package by.training.volkava.homework05;

import java.util.List;

/**
 * Class FileSystemOut designed to print the file system structure.
 *
 * @author Hanna_Volkava
 */
public class FileSystemOut {
    /**
     * Method to represent structure in strings.
     *
     * @param object object file system
     * @return string of structure
     * @throws FileSystemException if object is null
     */
    public static String printTree(FileSystem object) throws FileSystemException {
        if (object != null) {
            return print(object, 0);
        } else {
            throw new FileSystemException("Object file system must be not null");
        }
    }

    private static String print(FileSystem object, int nestingLevel) {
        StringBuilder result = new StringBuilder();
        // we don't show our main file system which put the folder from string
        if (nestingLevel != 0) {
            for (int i = 1; i < nestingLevel; i++) {
                result.append("  ");
            }
            result.append(object.getName());
        }
        if (object instanceof Folder) {
            if (nestingLevel != 0) {
                result.append("/\n");
            }
            List<FileSystem> childObjects = ((Folder) object).getChildObjects();
            for (FileSystem childObject : childObjects) {
                result.append(print(childObject, nestingLevel + 1));
            }
        } else {
            result.append("\n");
        }
        return result.toString();
    }
}
