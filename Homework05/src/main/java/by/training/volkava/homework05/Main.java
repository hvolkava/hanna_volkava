package by.training.volkava.homework05;

import java.util.Scanner;

/**
 * Main application class for Homework05.
 * Program that emulates the file system model, its hierarchical structure.
 *
 * @author Hanna Volkava
 */
public class Main {
    /**
     * Application start point. The program accepts as input a string
     * representing the path to the file / directory.
     * The user can display the resulting directory structure at any time.
     * The user at any time can complete the work with the program.
     *
     * @param args command line arguments
     * @throws FileSystemException occurs when something wrong
     */
    public static void main(String[] args) throws FileSystemException {
        FileSystem myFileSystem = new Folder("MyFileSystem");
        try (Scanner scan = new Scanner(System.in)) {
            while (true) {
                System.out.print("$>");
                String userString = scan.next();
                if (userString.trim().equalsIgnoreCase("print")) {
                    System.out.println(FileSystemOut.printTree(myFileSystem));
                } else if (userString.trim().equalsIgnoreCase("exit")) {
                    break;
                } else {
                    FileSystemUtil.parseString(myFileSystem, userString);
                }
            }
        } catch (Exception exception) {
            System.out.println(exception);
        }
    }
}
