package by.training.volkava.homework05;

/**
 * Interface FileSystem is designed for build file system tree.
 * "Component" in pattern "Composite"
 *
 * @author Hanna Volkava
 */
public interface FileSystem {
    String getName();
}
