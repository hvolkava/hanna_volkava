package by.training.volkava.homework05;

public class FileSystemException extends Exception {
    public FileSystemException(String message) {
        super(message);
    }
}
