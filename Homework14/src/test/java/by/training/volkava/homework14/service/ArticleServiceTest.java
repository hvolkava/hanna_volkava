package by.training.volkava.homework14.service;

import by.training.volkava.homework14.connection.HttpConnection;
import by.training.volkava.homework14.model.Article;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ArticleServiceTest {
    @Mock
    private HttpConnection connection;

    @InjectMocks
    private ArticleService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreatePost_InRightCase() {
        Article article = new Article(10, 5, "sunt", "quia et suscipit");
        Mockito.when(connection.doPostRequest(article)).thenReturn(Optional.of(article));
        Assert.assertTrue(service.createPost(article));
    }

    @Test
    public void testCreatePost_WhenConnectionReturnEmpty() {
        Article article = new Article(10, 5, "sunt", "quia et suscipit");
        Mockito.when(connection.doPostRequest(article)).thenReturn(Optional.empty());
        Assert.assertFalse(service.createPost(article));
    }

    @Test
    public void testCreatePost_WithNullArticle() {
        Assert.assertFalse(service.createPost(null));
    }

    @Test
    public void testGetArticleById_InRightCase() {
        Article article = new Article(10, 5, "sunt", "quia et suscipit");
        Mockito.when(connection.doGetRequest(10)).thenReturn(Optional.of(article));
        Assert.assertTrue(service.getArticleById(10));
    }
}
