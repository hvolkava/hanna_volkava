package by.training.volkava.homework14.connection.impl;

import by.training.volkava.homework14.model.Article;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ConnectionViaHttpClientTest {
    private String stringUrl = "https://fake-service/posts";

    @Mock
    private CloseableHttpClient mockHttpClient;
    @Mock
    private CloseableHttpResponse mockResponse;
    @Mock
    private StatusLine mocStatusLine;
    @Mock
    private HttpEntity mockHttpEntity;
    @InjectMocks
    private ConnectionViaHttpClient mockConn = new ConnectionViaHttpClient(stringUrl);

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDoPostRequest_InCorrectCase() throws IOException {
        when(mockHttpClient.execute(any(HttpPost.class))).thenReturn(mockResponse);
        when(mockResponse.getStatusLine()).thenReturn(mocStatusLine);
        when(mocStatusLine.getStatusCode()).thenReturn(201);
        when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
        String response = getJsonForCreate();
        when(mockHttpEntity.getContent()).thenReturn(new ByteArrayInputStream(response.getBytes()));
        Article article = new Article(101, 1, "nesciunt quas odio", "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\n" +
                "voluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque");
        Optional<Article> createdArticle = mockConn.doPostRequest(article);
        Assert.assertEquals(article, createdArticle.get());
    }

    @Test
    public void testDoPostRequest_WithNullArticle() {
        Optional<Article> createdArticle = mockConn.doPostRequest(null);
        Assert.assertFalse(createdArticle.isPresent());
    }

    @Test
    public void testDoPostRequest_WhenWrongUrl() throws IOException {
        when(mockHttpClient.execute(any(HttpPost.class))).thenReturn(mockResponse);
        when(mockResponse.getStatusLine()).thenReturn(mocStatusLine);
        when(mocStatusLine.getStatusCode()).thenReturn(404);
        Article article = new Article(10, 1, "optio molestias id quia eum", "quo e\n");
        Optional<Article> createdArticle = mockConn.doPostRequest(article);
        Assert.assertFalse(createdArticle.isPresent());
    }

    @Test
    public void testDoGetRequest_InCorrectCase() throws IOException {
        when(mockHttpClient.execute(any(HttpUriRequest.class))).thenReturn(mockResponse);
        when(mockResponse.getStatusLine()).thenReturn(mocStatusLine);
        when(mocStatusLine.getStatusCode()).thenReturn(200);
        when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
        String response = getJsonForId10();
        when(mockHttpEntity.getContent()).thenReturn(new ByteArrayInputStream(response.getBytes()));
        Article article = new Article(10, 1, "optio molestias id quia eum",
                "quo et expedita modi cum officia vel magni\n" +
                        "doloribus qui repudiandae\n" +
                        "vero nisi sit\n" +
                        "quos veniam quod sed accusamus veritatis error");
        Optional<Article> createdArticle = mockConn.doGetRequest(10);
        Assert.assertEquals(article, createdArticle.get());

    }

    @Test
    public void testDoGetRequest_mockConnWhenWrongUrl() throws IOException {
        when(mockHttpClient.execute(any(HttpUriRequest.class))).thenReturn(mockResponse);
        when(mockResponse.getStatusLine()).thenReturn(mocStatusLine);
        when(mocStatusLine.getStatusCode()).thenReturn(404);
        Optional<Article> createdArticle = mockConn.doGetRequest(10);
        Assert.assertFalse(createdArticle.isPresent());
    }
    private String getJsonForCreate() {
        return "{\n" +
                "  \"userId\": 1,\n" +
                "  \"id\": 101,\n" +
                "  \"title\": \"nesciunt quas odio\",\n" +
                "  \"body\": \"repudiandae veniam quaerat sunt sed\\nalias aut fugiat sit autem sed est\\n" +
                "voluptatem omnis possimus esse voluptatibus quis\\nest aut tenetur dolor neque\"\n" +
                "}";
    }
    private String getJsonForId10() {
        return "{\n" +
                "  \"userId\": 1,\n" +
                "  \"id\": 10,\n" +
                "  \"title\": \"optio molestias id quia eum\",\n" +
                "  \"body\": \"quo et expedita modi cum officia vel magni\\ndoloribus qui repudiandae\\nvero nisi sit\\nquos veniam quod sed accusamus veritatis error\"\n" +
                "}";
    }
}
