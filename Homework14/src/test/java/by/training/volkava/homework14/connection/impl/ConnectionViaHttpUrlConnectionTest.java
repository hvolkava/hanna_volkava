package by.training.volkava.homework14.connection.impl;

import by.training.volkava.homework14.model.Article;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class ConnectionViaHttpUrlConnectionTest {
    private String serviceUrl = "https://fake-service";
    @Mock
    private HttpURLConnection mockConnection;
    private ConnectionViaHttpUrlConnection connection;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        connection = new ConnectionViaHttpUrlConnection(serviceUrl) {
            protected HttpURLConnection openConnection(URL requestUrl) {
                return mockConnection;
            }
        };
    }

    @Test
    public void testDoPostRequest_InRightCase() throws Exception {
        when(mockConnection.getResponseCode()).thenReturn(201);
        String response = getJsonForCreate();
        OutputStream os = mock(OutputStream.class);
        when(mockConnection.getOutputStream()).thenReturn(os);
        when(mockConnection.getInputStream()).thenReturn(new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8)));
        Article article = new Article(101, 1, "nesciunt quas odio", "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\n" +
                "voluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque");
        Optional<Article> createdArticle = connection.doPostRequest(article);
        Assert.assertEquals(article, createdArticle.get());
    }

    @Test
    public void testDoPostRequest_WithNullArticle() {
        connection = new ConnectionViaHttpUrlConnection(serviceUrl);
        Optional<Article> createdArticle = connection.doPostRequest(null);
        Assert.assertFalse(createdArticle.isPresent());
    }

    @Test
    public void testDoPostRequest_WithWrongUrl() throws IOException {
        when(mockConnection.getResponseCode()).thenReturn(404);
        OutputStream os = mock(OutputStream.class);
        when(mockConnection.getOutputStream()).thenReturn(os);
        Article article = new Article(101, 5, "title", "body");
        Optional<Article> createdArticle = connection.doPostRequest(article);
        Assert.assertFalse(createdArticle.isPresent());
    }

    @Test
    public void testDoGetRequest_InRightCase() throws IOException {
        when(mockConnection.getResponseCode()).thenReturn(200);
        String response = getJsonForId10();
        when(mockConnection.getInputStream()).thenReturn(new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8)));
        Article article = new Article(10, 1, "optio molestias id quia eum",
                "quo et expedita modi cum officia vel magni\n" +
                        "doloribus qui repudiandae\n" +
                        "vero nisi sit\n" +
                        "quos veniam quod sed accusamus veritatis error");
        Optional<Article> createdArticle = connection.doGetRequest(10);
        Assert.assertEquals(article, createdArticle.get());
    }

    @Test
    public void testDoGetRequest_WhenWrongUrl() throws IOException {
        when(mockConnection.getResponseCode()).thenReturn(404);
        Optional<Article> createdArticle = connection.doGetRequest(10);
        Assert.assertFalse(createdArticle.isPresent());
    }

    private String getJsonForCreate() {
        return "{\n" +
                "  \"userId\": 1,\n" +
                "  \"id\": 101,\n" +
                "  \"title\": \"nesciunt quas odio\",\n" +
                "  \"body\": \"repudiandae veniam quaerat sunt sed\\nalias aut fugiat sit autem sed est\\n" +
                "voluptatem omnis possimus esse voluptatibus quis\\nest aut tenetur dolor neque\"\n" +
                "}";
    }

    private String getJsonForId10() {
        return "{\n" +
                "  \"userId\": 1,\n" +
                "  \"id\": 10,\n" +
                "  \"title\": \"optio molestias id quia eum\",\n" +
                "  \"body\": \"quo et expedita modi cum officia vel magni\\ndoloribus qui repudiandae\\nvero nisi sit\\nquos veniam quod sed accusamus veritatis error\"\n" +
                "}";
    }
}
