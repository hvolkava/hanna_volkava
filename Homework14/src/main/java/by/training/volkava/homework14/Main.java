package by.training.volkava.homework14;

import by.training.volkava.homework14.connection.HttpConnection;
import by.training.volkava.homework14.connection.impl.ConnectionViaHttpClient;
import by.training.volkava.homework14.connection.impl.ConnectionViaHttpUrlConnection;
import by.training.volkava.homework14.model.Article;
import by.training.volkava.homework14.service.ArticleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * The program must accept the article ID as input and output information for it
 * or save information about the new article.
 *
 * @author Hanna Volkava
 */
public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class.getName());

    /**
     * Method create connection depends on argument of command line and get or create new article.
     *
     * @param args arguments of command line
     */
    public static void main(String[] args) {
        LOG.debug("Start application with args: {}", Arrays.toString(args));
        String serviceUrl = "https://jsonplaceholder.typicode.com/posts";
        HttpConnection connection = createConnection(args, serviceUrl);
        ArticleService articleService = new ArticleService(connection);
        try {
            doUserAction(args, articleService);
        } catch (IllegalArgumentException exception) {
            LOG.error(exception.toString(), exception);
        }
        LOG.debug("Exit from application");
    }

    /**
     * Method for chosen which connection we will be used.
     * By default we used ConnectionViaHttpUrlConnection
     *
     * @param args       arguments of command line
     * @param serviceUrl url service
     * @return created connection
     */

    private static HttpConnection createConnection(String[] args, String serviceUrl) {
        if (Arrays.asList(args).contains("task2")) {
            return new ConnectionViaHttpClient(serviceUrl);
        }
        return new ConnectionViaHttpUrlConnection(serviceUrl);
    }

    private static void doUserAction(String[] args, ArticleService service) {
        if (args.length == 0 || args.length > 3) {
            throw new IllegalArgumentException("Check command line arguments");
        }
        switch (args[0].toUpperCase()) {
            case "GET":
                service.getArticleById(parseInt(args[1]));
                break;
            case "POST":
                Article createdArticle = new Article(101, 3,
                        "title-example", "body-example");
                service.createPost(createdArticle);
                break;
            default:
                LOG.error("Exception thrown: wrong argument");
                throw new IllegalArgumentException("You have provided an incorrect "
                        + "first argument " + args[0]);
        }
    }

    /**
     * Method for parse int argument.
     *
     * @param arg actual argument
     * @return parsed int argument
     * @throws IllegalArgumentException occurs when String arg cannot convert to int
     */
    private static int parseInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException exception) {
            LOG.error("Exception thrown: wrong argument");
            throw new IllegalArgumentException("Incorrect argument " + arg, exception);
        }
    }
}
