package by.training.volkava.homework14.utils;

import by.training.volkava.homework14.model.Article;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Objects;

public class ArticleParser {
    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * The method converts the string JSON into the object type of Article.
     *
     * @param stringJson string JSON
     * @return object type of Article
     * @throws IOException occurs when mapper cannot do this conversion
     */
    public static Article convertJsonToObject(String stringJson)
            throws IOException {
        if (Objects.isNull(stringJson) || stringJson.isEmpty()) {
            throw new IllegalArgumentException("JSON empty or null");
        }
        return mapper.readValue(stringJson.getBytes(), Article.class);
    }

    /**
     * The method converts object type of Article into string of JSON.
     *
     * @param article object
     * @return string of JSON
     * @throws JsonProcessingException  occurs when mapper cannot do this conversion
     * @throws IllegalArgumentException when given article is null
     */
    public static String convertObjectToJson(Article article) throws JsonProcessingException {
        if (Objects.isNull(article)) {
            throw new IllegalArgumentException("Article is null");
        }
        return mapper.writeValueAsString(article);
    }
}
