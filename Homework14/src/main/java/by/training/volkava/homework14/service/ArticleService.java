package by.training.volkava.homework14.service;

import by.training.volkava.homework14.connection.HttpConnection;
import by.training.volkava.homework14.model.Article;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;

/**
 * The ArticleService allows you to work with articles.
 *
 * @author Hanna Volkava
 */
public class ArticleService {
    private static final Logger LOG = LoggerFactory.getLogger(ArticleService.class.getName());
    private HttpConnection connection;

    public ArticleService(HttpConnection connection) {
        this.connection = connection;
    }

    /**
     * Save information about the new article.
     *
     * @param article article what we want to save
     * @return saved article in wrapper Optional
     */
    public boolean createPost(Article article) {
        if (Objects.isNull(article)) {
            return false;
        }
        Optional<Article> createdArticle = connection.doPostRequest(article);
        createdArticle.ifPresent(post -> {
            StringBuilder sb = (new StringBuilder(post.toString()))
                    .insert(post.toString().indexOf(":"), " has been created");
            LOG.info(sb.toString());
        });
        return createdArticle.isPresent();
    }

    /**
     * Get article by Id.
     *
     * @param id Id article
     * @return article with needed Id in wrapper Optional
     */
    public boolean getArticleById(int id) {
        Optional<Article> article = connection.doGetRequest(id);
        article.ifPresent(post -> LOG.info(post.toString()));
        return article.isPresent();
    }
}
