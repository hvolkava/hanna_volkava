package by.training.volkava.homework14.connection.impl;

import by.training.volkava.homework14.connection.HttpConnection;
import by.training.volkava.homework14.model.Article;
import by.training.volkava.homework14.utils.ArticleParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

public class ConnectionViaHttpUrlConnection implements HttpConnection {
    private static final Logger LOG = LoggerFactory.getLogger(
            ConnectionViaHttpUrlConnection.class.getName());
    private String serviceUrl;

    /**
     * Constructor creates instance of connection bound in a specific url service.
     *
     * @param serviceUrl url service
     */
    public ConnectionViaHttpUrlConnection(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    /**
     * The method sends a POST request to url service with the passed article.
     *
     * @param article article
     * @return saved article in wrapper Optional
     */
    public Optional<Article> doPostRequest(Article article) {
        try {
            URL requestUrl = new URL(serviceUrl);
            HttpURLConnection connection = openConnection(requestUrl);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            try (OutputStream os = connection.getOutputStream()) {
                String stringJson = ArticleParser.convertObjectToJson(article);
                os.write(stringJson.getBytes());
            }
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_CREATED) {
                return Optional.ofNullable(getArticleFromResponse(connection));
            } else {
                LOG.error("Response code not equals 201");
                throw new IOException("Post Request failed with error code " + responseCode);
            }
        } catch (IOException | IllegalArgumentException exception) {
            LOG.error(exception.toString(), exception);
        }
        return Optional.empty();
    }

    /**
     * The method sends a GET request to REST service with the passed Id.
     *
     * @param id id article
     * @return saved article in wrapper Optional
     */
    public Optional<Article> doGetRequest(int id) {
        URL urlForGetRequest;
        try {
            urlForGetRequest = new URL(serviceUrl + '/' + id);
            HttpURLConnection connection = openConnection(urlForGetRequest);
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                return Optional.ofNullable(getArticleFromResponse(connection));
            } else {
                LOG.error("Exception thrown: response code not equals 200");
                throw new IOException("Get Request failed with error code " + responseCode);
            }
        } catch (IOException exception) {
            LOG.error(exception.toString(), exception);
        }
        return Optional.empty();
    }

    private Article getArticleFromResponse(HttpURLConnection connection) throws IOException {
        String stringJson = convertStreamToString(connection.getInputStream());
        return ArticleParser.convertJsonToObject(stringJson);
    }

    private String convertStreamToString(InputStream is) {
        StringBuilder result = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line).append("\n");
            }
        } catch (IOException exception) {
            LOG.error(exception.toString(), exception);
        }
        return result.toString();
    }

    HttpURLConnection openConnection(URL requestUrl) throws IOException {
        return (HttpURLConnection) requestUrl.openConnection();
    }
}
