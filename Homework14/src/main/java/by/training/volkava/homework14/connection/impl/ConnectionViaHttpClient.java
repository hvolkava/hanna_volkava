package by.training.volkava.homework14.connection.impl;

import by.training.volkava.homework14.connection.HttpConnection;
import by.training.volkava.homework14.model.Article;
import by.training.volkava.homework14.utils.ArticleParser;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class ConnectionViaHttpClient implements HttpConnection {
    private static final Logger LOG = LoggerFactory.getLogger(
            ConnectionViaHttpClient.class.getName());
    private String serviceUrl;
    private CloseableHttpClient httpClient;

    /**
     * Constructor creates instance of connection bound in a specific url service.
     *
     * @param serviceUrl url service
     */
    public ConnectionViaHttpClient(String serviceUrl) {
        this.serviceUrl = serviceUrl;
        this.httpClient = HttpClientBuilder.create().build();
    }

    /**
     * The method sends a GET request to REST service with the passed Id.
     *
     * @param id id article
     * @return saved article in wrapper Optional
     */
    public Optional<Article> doGetRequest(int id) {
        try {
            HttpUriRequest getRequest = new HttpGet(serviceUrl + '/' + id);
            getRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
            CloseableHttpResponse httpResponse = httpClient.execute(getRequest);
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (responseCode == HttpStatus.SC_OK) {
                return Optional.ofNullable(getArticleFromResponse(httpResponse));
            } else {
                LOG.error("Exception thrown: response code not equals 200");
                throw new IOException("Get Request failed with error code " + responseCode);
            }
        } catch (IOException exception) {
            LOG.error(exception.toString(), exception);
            return Optional.empty();
        }
    }

    /**
     * The method sends a POST request to url service with the passed article.
     *
     * @param article article
     * @return saved article in wrapper Optional
     */
    public Optional<Article> doPostRequest(Article article) {
        HttpPost postRequest = new HttpPost(serviceUrl);
        try {
            String stringJson = ArticleParser.convertObjectToJson(article);
            postRequest.setEntity(new StringEntity(stringJson));
            postRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            CloseableHttpResponse httpResponse = httpClient.execute(postRequest);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_CREATED) {
                return Optional.ofNullable(getArticleFromResponse(httpResponse));
            } else {
                LOG.error("Response code not equals 201");
                throw new IOException("Post Request failed with error code " + statusCode);
            }
        } catch (IOException | IllegalArgumentException exception) {
            LOG.error(exception.toString(), exception);
        }
        return Optional.empty();
    }

    private Article getArticleFromResponse(CloseableHttpResponse httpResponse) throws IOException {
        String responseJson = EntityUtils.toString(httpResponse.getEntity());
        return ArticleParser.convertJsonToObject(responseJson);
    }
}
