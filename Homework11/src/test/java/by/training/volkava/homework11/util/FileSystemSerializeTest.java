package by.training.volkava.homework11.util;

import by.training.volkava.homework11.model.FileSystem;
import by.training.volkava.homework11.model.FileSystemException;
import by.training.volkava.homework11.model.Folder;
import org.junit.Assert;
import org.junit.Test;
import java.util.Optional;

public class FileSystemSerializeTest {

    @Test
    public void testWriteAndRead_WithCorrectStructure() throws FileSystemException {
        FileSystem expected = new Folder("testFS");
        FileSystemUtil.parseString(expected, "root/folder1/file.txt");
        FileSystemSerialize.write(expected);
        Optional<FileSystem> fileSystem = FileSystemSerialize.read();
        FileSystem actual = fileSystem.get();
        Assert.assertEquals(FileSystemOut.printTree(expected), FileSystemOut.printTree(actual));
    }

    @Test
    public void testRead_NullValue() throws FileSystemException {
        FileSystem expected = null;
        FileSystemSerialize.write(expected);
        Optional<FileSystem> fileSystem = FileSystemSerialize.read();
        Assert.assertFalse(fileSystem.isPresent());
    }
}