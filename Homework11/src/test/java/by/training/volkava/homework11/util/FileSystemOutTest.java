package by.training.volkava.homework11.util;

import by.training.volkava.homework11.model.FileSystem;
import by.training.volkava.homework11.model.FileSystemException;
import by.training.volkava.homework11.model.Folder;
import by.training.volkava.homework11.util.FileSystemOut;
import by.training.volkava.homework11.util.FileSystemUtil;
import org.junit.Assert;
import org.junit.Test;

public class FileSystemOutTest {

    @Test
    public void printTree_WithCorrectValue() throws FileSystemException {
        FileSystem fileSystem = new Folder("testFS");
        FileSystemUtil.parseString(fileSystem, "root/folder1/file.txt");
        FileSystemUtil.parseString(fileSystem, "root/folder1");
        FileSystemUtil.parseString(fileSystem, "root/folder1/folder2/file.txt");
        String excepted = "root/\n" +
                "  folder1/\n" +
                "    file.txt\n" +
                "    folder2/\n" +
                "      file.txt\n";
        Assert.assertEquals(excepted, FileSystemOut.printTree(fileSystem));
    }

    @Test(expected = FileSystemException.class)
    public void printTree_WithNull() throws FileSystemException {
        FileSystem fileSystem = null;
        String actual = FileSystemOut.printTree(fileSystem);
    }
}