package by.training.volkava.homework11.model;

import by.training.volkava.homework11.model.File;
import by.training.volkava.homework11.model.FileSystemException;
import org.junit.Assert;
import org.junit.Test;

public class FileTest {

    @Test
    public void testGetName() throws FileSystemException {
        File file = new File("Test.txt");
        String expected = "Test.txt";
        Assert.assertEquals(expected, file.getName());
    }
    @Test(expected = FileSystemException.class)
    public void testFileConstructor_WithoutExt() throws FileSystemException {
        File file = new File("Test");
    }

    @Test(expected = FileSystemException.class)
    public void testFileConstructor_WithEmptyName() throws FileSystemException {
        File file = new File("");
    }

    @Test(expected = FileSystemException.class)
    public void testFileConstructor_WithNull() throws FileSystemException {
        String name = null;
        File file = new File(name);
    }
}