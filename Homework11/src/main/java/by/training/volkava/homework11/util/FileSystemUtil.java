package by.training.volkava.homework11.util;

import by.training.volkava.homework11.model.File;
import by.training.volkava.homework11.model.FileSystem;
import by.training.volkava.homework11.model.FileSystemException;
import by.training.volkava.homework11.model.Folder;

import java.util.Objects;

/**
 * FileSystemUtil class designed to create a structure from a string.
 *
 * @author Hanna Volkava
 */
public class FileSystemUtil {
    /**
     * Method to create a structure from a string.
     *
     * @param parent     object file system in which create structure
     * @param pathString string of structure
     * @throws FileSystemException occurs when pathString contains some wrong name
     */
    public static void parseString(FileSystem parent, String pathString)
            throws FileSystemException {
        final String separator = "/";
        String[] objects = pathString.split(separator);
        if (Objects.isNull(parent) || !(parent instanceof Folder)) {
            throw new FileSystemException("Object file system must be not null "
                    + "and have type Folder");
        }
        for (int i = 0; i < objects.length; i++) {
            if (i != objects.length - 1) {
                parent = addFolder(parent, objects[i]);
            } else {
                if (File.isFileName(objects[i])) {
                    addFile(parent, objects[i]);
                } else {
                    parent = addFolder(parent, objects[i]);
                }
            }
        }
    }

    private static FileSystem addFolder(FileSystem parent, String nameFolder)
            throws FileSystemException {
        FileSystem newFolder = new Folder(nameFolder);
        ((Folder) parent).add(newFolder);
        return ((Folder) parent).getFolderByName(nameFolder);
    }

    private static void addFile(FileSystem parent, String nameFile) throws FileSystemException {
        FileSystem newFile = new File(nameFile);
        ((Folder) parent).add(newFile);
    }
}
