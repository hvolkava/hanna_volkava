package by.training.volkava.homework11.model;

public class FileSystemException extends Exception {
    public FileSystemException(String message) {
        super(message);
    }
}
