package by.training.volkava.homework11.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class Folder represents an object Folder in model.
 * May contains folders and files
 * "Composite"
 *
 * @author Hanna Volkava
 */
public class Folder implements FileSystem {
    private String name;
    private List<FileSystem> childObjects = new ArrayList<>();

    public Folder(String name) throws FileSystemException {
        setName(name);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) throws FileSystemException {
        if (Objects.nonNull(name) && !name.isEmpty()) {
            this.name = name;
        } else {
            throw new FileSystemException("Folder must be have name");
        }
    }

    public List<FileSystem> getChildObjects() {
        return childObjects;
    }

    /**
     * Method for add object file system in folder.
     *
     * @param object object what we want add
     * @return true if object added, false if such object exist in folder
     */
    public boolean add(FileSystem object) {
        for (FileSystem childObject : childObjects) {
            if (childObject.getName().equalsIgnoreCase(object.getName())
                    && childObject.getClass() == object.getClass()) {
                return false;
            }
        }
        childObjects.add(object);
        return true;
    }

    /**
     * Method for searching folder by name among children.
     *
     * @param nameFolder name folder we want to find
     * @return object folder if it exist and parent folder otherwise
     */
    public FileSystem getFolderByName(String nameFolder) {
        for (FileSystem childObject : childObjects) {
            if (childObject.getName().equalsIgnoreCase(nameFolder)
                    && (childObject instanceof Folder)) {
                return childObject;
            }
        }
        return this;
    }
}
