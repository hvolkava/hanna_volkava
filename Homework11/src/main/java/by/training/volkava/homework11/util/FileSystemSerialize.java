package by.training.volkava.homework11.util;

import by.training.volkava.homework11.model.FileSystem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;

/**
 * The class FileSystemSerialize helps to save the file structure to a file
 * and read it from a file.
 *
 * @author Hanna Volkava
 */
public class FileSystemSerialize {
    private static final String FILE_NAME = "FileSystem.ser";

    /**
     * Method save object of file system in file.
     *
     * @param object object file system
     */
    public static void write(FileSystem object) {
        try (FileOutputStream fs = new FileOutputStream(FILE_NAME);
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(object);
        } catch (Exception exception) {
            System.out.println("Could not write structure to file");
        }
    }

    /**
     * Method read object of file system from file.
     */
    public static Optional<FileSystem> read() {
        FileSystem fileSystem = null;
        try (FileInputStream fis = new FileInputStream(FILE_NAME);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            fileSystem = (FileSystem) ois.readObject();
        } catch (Exception exception) {
            System.out.println("Could not read structure from file");
        }
        return Optional.ofNullable(fileSystem);
    }

}
