<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<h1>404 Page Not Found.../</h1><br/>
<p>Error code: ${pageContext.errorData.statusCode}</p>
<p>Request URL: ${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}</p>
<p><a href="${pageContext.request.contextPath}/login">Start page</a></p>
</body>
</html>
