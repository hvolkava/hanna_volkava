package by.training.volkava.homework18.listener;

import by.training.volkava.homework18.util.DbConnectionManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        (new DbConnectionManager()).createAndFillsTablesInBd();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
