package by.training.volkava.homework18.servlet;

import by.training.volkava.homework18.model.Good;
import by.training.volkava.homework18.model.Order;
import by.training.volkava.homework18.model.User;
import by.training.volkava.homework18.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "checkout", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(CheckoutServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        User user = (User) request.getSession(false).getAttribute("user");
        OrderService orderService = new OrderService();
        Map<Good, Integer> goodsMap = orderService.getOrderByUser(user);
        if (goodsMap.isEmpty()) {

            response.sendRedirect(request.getContextPath() + "/goods");
        }
        Order order = orderService.saveOrderInDd(user, goodsMap);
        request.setAttribute("order", order);
        request.setAttribute("goodsMap", goodsMap);

        getServletContext().getRequestDispatcher("/WEB-INF/jsp/checkout.jsp")
                .forward(request, response);
    }
}
