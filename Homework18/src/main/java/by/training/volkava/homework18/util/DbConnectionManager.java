package by.training.volkava.homework18.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnectionManager {
    private static final Logger LOG = LoggerFactory.getLogger(DbConnectionManager.class.getName());
    private static final String DB_PROPS_PATH = "/db/db.properties";
    private static String dbUrl;
    private static String user;
    private static String password;
    private static String init;
    private static String driver;

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(dbUrl, user, password);
    }

    /**
     * The method creates a database connection and creates a table structure and populates it.
     */
    public void createAndFillsTablesInBd() {
        Properties props = new Properties();
        try (InputStream inputStream = this.getClass().getResourceAsStream(DB_PROPS_PATH)) {
            if (inputStream != null) {
                props.load(inputStream);
            }
        } catch (IOException exception) {
            LOG.error("The problem with reading the settings for the connection", exception);
        }
        dbUrl = props.getProperty("db.url");
        user = props.getProperty("db.user");
        password = props.getProperty("db.password");
        init = props.getProperty("db.initandfill");
        driver = props.getProperty("db.driver");
        try {
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(dbUrl + init, user, password);
            conn.close();
        } catch (ClassNotFoundException | SQLException exception) {
            LOG.error("Problem with the first connection creating the database structure",
                    exception);
        }
    }
}