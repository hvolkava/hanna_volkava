package by.training.volkava.homework18.dao;

import by.training.volkava.homework18.model.User;

public interface UserDao {
    User getUserByLogin(String login);
}
