package by.training.volkava.homework18.dao;

import by.training.volkava.homework18.model.Good;
import by.training.volkava.homework18.model.Order;
import by.training.volkava.homework18.model.User;

import java.util.Map;

public interface OrderDao {
    Order saveOrder(User user, Map<Good, Integer> goodsMap);
}
