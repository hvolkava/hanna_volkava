package by.training.volkava.homework18.service;

import by.training.volkava.homework18.dao.OrderDao;
import by.training.volkava.homework18.dao.OrderGoodDao;
import by.training.volkava.homework18.dao.impl.OrderDaoImpl;
import by.training.volkava.homework18.dao.impl.OrderGoodsDaoImpl;
import by.training.volkava.homework18.model.Good;
import by.training.volkava.homework18.model.Order;
import by.training.volkava.homework18.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
public class OrderService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderService.class.getName());
    private static Map<User, Map<Good, Integer>> ordersMap;

    /**
     * Get get all items fo online-shop.
     *
     * @return set items
     */
    private static Map<User, Map<Good, Integer>> getInstance() {
        if (Objects.isNull(ordersMap)) {
            ordersMap = new HashMap<>();
        }
        return ordersMap;
    }

    /**
     * Gets map order by username.
     *
     * @param user user
     * @return map order
     */
    public Map<Good, Integer> getOrderByUser(User user) {
        if (getInstance().containsKey(user)) {
            return ordersMap.get(user);
        } else {
            return new HashMap<>();
        }
    }

    /**
     * Add item in cart.
     *
     * @param user   user
     * @param goodId id item
     */
    public void addItem(User user, String... goodId) {
        GoodsService goodsService = new GoodsService();

        Map<Good, Integer> goodsMap = goodsService.convertStringIdsToObjects(goodId);
        Map<User, Map<Good, Integer>> ordersMap = getInstance();
        if (getInstance().containsKey(user)) {
            Map<Good, Integer> mapGoods = ordersMap.get(user);
            goodsMap.forEach((key, value) -> mapGoods.merge(key, value, Integer::sum));
        } else {
            ordersMap.put(user, goodsMap);
        }
    }

    /**
     * Method for saving order in database.
     *
     * @param user     user
     * @param goodsMap map of goods
     * @return created order
     */
    public Order saveOrderInDd(User user, Map<Good, Integer> goodsMap) {
        OrderDao orderDao = new OrderDaoImpl();
        Order createdOrder = orderDao.saveOrder(user, goodsMap);
        OrderGoodDao orderGoodDao = new OrderGoodsDaoImpl();
        orderGoodDao.insertOrderGood(createdOrder, goodsMap);
        ordersMap.remove(user);
        if (Objects.nonNull(createdOrder)) {
            LOG.debug(user.toString() + " saved in BD order " + createdOrder.toString());
        }
        return createdOrder;
    }
}
