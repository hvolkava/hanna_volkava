package by.training.volkava.homework18.dao.impl;

import by.training.volkava.homework18.dao.OrderGoodDao;
import by.training.volkava.homework18.model.Good;
import by.training.volkava.homework18.model.Order;
import by.training.volkava.homework18.util.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;

public class OrderGoodsDaoImpl implements OrderGoodDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderGoodsDaoImpl.class.getName());
    private DbConnectionManager dbManager = new DbConnectionManager();
    private static final String INSERT_ORDERGOOD_SQL_STATEMENT =
            "INSERT INTO Order_Good (order_id, good_id, count) values (?, ?, ?)";

    @Override
    public void insertOrderGood(Order order, Map<Good, Integer> goodsMap) {
        try (Connection connection = dbManager.getConnection();
             PreparedStatement st = connection.prepareStatement(INSERT_ORDERGOOD_SQL_STATEMENT)) {
            for (Map.Entry<Good, Integer> entry : goodsMap.entrySet()) {
                st.setInt(1, order.getId());
                st.setInt(2, entry.getKey().getId());
                st.setDouble(3, entry.getValue());
                st.addBatch();
            }
            st.execute();
        } catch (SQLException exception) {
            LOG.error("Problem with executing INSERT", exception);
        }
    }
}
