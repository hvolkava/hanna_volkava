package by.training.volkava.homework18.servlet;

import by.training.volkava.homework18.model.User;
import by.training.volkava.homework18.service.GoodsService;
import by.training.volkava.homework18.service.OrderService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "goods", urlPatterns = {"/goods"})
public class GoodsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        User user = (User) request.getSession(false).getAttribute("user");
        String[] goodIds = request.getParameterValues("goods");
        OrderService orderService = new OrderService();
        orderService.addItem(user, goodIds);
        GoodsService goodsService = new GoodsService();

        request.setAttribute("order", orderService.getOrderByUser(user));
        request.setAttribute("goods", goodsService.getAllGoods());

        getServletContext().getRequestDispatcher("/WEB-INF/jsp/goods.jsp")
                .forward(request, response);
    }
}
