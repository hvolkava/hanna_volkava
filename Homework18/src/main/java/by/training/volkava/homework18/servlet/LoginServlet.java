package by.training.volkava.homework18.servlet;

import by.training.volkava.homework18.model.User;
import by.training.volkava.homework18.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "login", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String username = request.getParameter("username");
        UserService userService = new UserService();
        User user = userService.getUserByLogin(username);

        boolean isAccept = Objects.nonNull(request.getParameter("isAccept"))
                && request.getParameter("isAccept").equals("on");
        request.getSession(true).setAttribute("user", user);
        request.getSession(false).setAttribute("isAccept", isAccept);
        if (isAccept) {
            if (Objects.isNull(user)) {
                LOG.debug("Trying to log in with login " + username);
                request.setAttribute("errorMessage", "Sorry, but you are not registered.");
                request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
            } else {
                LOG.debug(user.toString() + " successfully logged in");
                request.getRequestDispatcher("/goods").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("/WEB-INF/jsp/acceptError.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp")
                .forward(request, response);
    }
}
