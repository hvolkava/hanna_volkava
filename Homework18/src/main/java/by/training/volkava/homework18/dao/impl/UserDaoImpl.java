package by.training.volkava.homework18.dao.impl;

import by.training.volkava.homework18.dao.UserDao;
import by.training.volkava.homework18.model.User;
import by.training.volkava.homework18.util.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class.getName());
    private DbConnectionManager dbManager = new DbConnectionManager();

    private static final String SELECT_BY_LOGIN_SQL_STATEMENT =
            "SELECT * FROM User WHERE login = ?";

    /**
     * Method for find user in DB by his login.
     *
     * @param login login
     * @return user
     */
    public User getUserByLogin(String login) {
        try (Connection connection = dbManager.getConnection();
             PreparedStatement st = connection.prepareStatement(SELECT_BY_LOGIN_SQL_STATEMENT)) {
            st.setString(1, login);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("id"), rs.getString("login"));
                rs.close();
                return user;
            }
        } catch (SQLException exception) {
            LOG.error("Problem with executing SELECT", exception);
        }
        return null;
    }
}
