package by.training.volkava.homework18.dao.impl;

import by.training.volkava.homework18.dao.GoodDao;
import by.training.volkava.homework18.model.Good;
import by.training.volkava.homework18.util.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class GoodDaoImpl implements GoodDao {
    private static final Logger LOG = LoggerFactory.getLogger(GoodDaoImpl.class.getName());
    private DbConnectionManager dbManager = new DbConnectionManager();

    private static final String SELECT_ALL_GOODS_SQL_STATEMENT = "SELECT * FROM Good";
    private static final String SELECT_BY_ID_SQL_STATEMENT = "SELECT * FROM Good WHERE id = ?";

    @Override
    public Set<Good> getAllGood() {
        Set<Good> result = new HashSet<>();
        try (Connection connection = dbManager.getConnection();
             PreparedStatement st = connection.prepareStatement(SELECT_ALL_GOODS_SQL_STATEMENT);
             ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                result.add(new Good(rs.getInt("id"), rs.getString("title"),
                        rs.getDouble("price")));
            }
        } catch (SQLException exception) {
            LOG.error("Problem with executing SELECT", exception);
        }
        return result;
    }

    @Override
    public Good getGoodById(int id) {
        try (Connection connection = dbManager.getConnection();
             PreparedStatement st = connection.prepareStatement(SELECT_BY_ID_SQL_STATEMENT)) {
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Good good = new Good(rs.getInt("id"), rs.getString("title"),
                        rs.getDouble("price"));
                rs.close();
                return good;
            }
        } catch (SQLException exception) {
            LOG.error("Problem with executing SELECT", exception);
        }
        return null;
    }
}
