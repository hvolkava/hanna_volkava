package by.training.volkava.homework18.service;

import by.training.volkava.homework18.dao.UserDao;
import by.training.volkava.homework18.dao.impl.UserDaoImpl;
import by.training.volkava.homework18.model.User;

public class UserService {
    /**
     * Get user by his login.
     * @param login user login
     * @return user
     */
    public User getUserByLogin(String login) {
        UserDao userDao = new UserDaoImpl();
        return userDao.getUserByLogin(login);
    }
}
