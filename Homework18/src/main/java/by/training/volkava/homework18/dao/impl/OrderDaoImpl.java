package by.training.volkava.homework18.dao.impl;

import by.training.volkava.homework18.dao.OrderDao;
import by.training.volkava.homework18.model.Good;
import by.training.volkava.homework18.model.Order;
import by.training.volkava.homework18.model.User;
import by.training.volkava.homework18.util.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class OrderDaoImpl implements OrderDao {
    private static final Logger LOG = LoggerFactory.getLogger(OrderDaoImpl.class.getName());
    private DbConnectionManager dbManager = new DbConnectionManager();
    private static final String INSERT_ORDER_SQL_STATEMENT =
            "INSERT INTO Orders (user_id, total_price) values (?, ?)";

    @Override
    public Order saveOrder(User user, Map<Good, Integer> goodsMap) {
        try (Connection connection = dbManager.getConnection();
             PreparedStatement st = connection.prepareStatement(INSERT_ORDER_SQL_STATEMENT,
                     Statement.RETURN_GENERATED_KEYS)) {
            st.setInt(1, user.getId());
            double totalSum = goodsMap.entrySet().stream()
                    .mapToDouble(entry -> entry.getKey().getPrice() * entry.getValue()).sum();
            st.setDouble(2, totalSum);
            st.executeUpdate();
            ResultSet keys = st.getGeneratedKeys();
            if (keys.next()) {
                int generatedId = keys.getInt(1);
                keys.close();
                return new Order(generatedId, user.getId(), totalSum);
            }
        } catch (SQLException exception) {
            LOG.error("Problem with executing INSERT", exception);
        }
        return null;
    }
}
