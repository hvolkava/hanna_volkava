package by.training.volkava.homework17.servlet;

import by.training.volkava.homework17.model.Item;
import by.training.volkava.homework17.service.OrderService;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "checkout", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String username = (String) request.getSession(false).getAttribute("username");
        OrderService orderService = new OrderService();
        Map<Item, Integer> order = orderService.getOrderByUsername(username);
        if (order.isEmpty()) {
            response.sendRedirect(request.getContextPath() + "/items");
        }
        request.setAttribute("order", order);
        request.setAttribute("totalSum", orderService.getTotalSum(username));

        getServletContext().getRequestDispatcher("/WEB-INF/jsp/checkout.jsp")
                .forward(request, response);
    }
}
