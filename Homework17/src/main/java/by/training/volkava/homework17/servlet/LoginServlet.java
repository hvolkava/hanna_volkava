package by.training.volkava.homework17.servlet;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "login", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String username = request.getParameter("username");
        boolean isAccept = Objects.nonNull(request.getParameter("isAccept"))
                && request.getParameter("isAccept").equals("on");
        request.getSession(true).setAttribute("username", username);
        request.getSession(false).setAttribute("isAccept", isAccept);
        if (isAccept) {
            if (Objects.isNull(username) || username.trim().isEmpty()) {
                response.sendRedirect(request.getContextPath() + "/login");
            } else {
                request.getRequestDispatcher("/items").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("/WEB-INF/jsp/acceptError.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp")
                .forward(request, response);
    }
}
