package by.training.volkava.homework17.service;

import by.training.volkava.homework17.model.Item;
import by.training.volkava.homework17.util.ItemsSingleton;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class ItemsService {
    /**
     * Method converts the id array into set items.
     *
     * @param itemIds array of id
     * @return set items
     */
    public Map<Item, Integer> convertStringIdsToObjects(String... itemIds) {
        Map<Item, Integer> itemsMap = new HashMap<>();
        if (Objects.isNull(itemIds)) {
            return itemsMap;
        }
        for (String id: itemIds) {
            Item item = getItemById(Integer.parseInt(id));
            itemsMap.merge(item, 1, Integer::sum);
        }
        return itemsMap;
    }

    /**
     * Get item by id.
     *
     * @param id id of item
     * @return item
     */
    private Item getItemById(int id) {
        Set<Item> setItems = ItemsSingleton.getInstance();
        return setItems.stream().filter(item -> item.getId() == id).findAny()
                .orElseThrow(() -> new IllegalArgumentException("Couldn't find item "
                        + "with id equals" + id));
    }
}
