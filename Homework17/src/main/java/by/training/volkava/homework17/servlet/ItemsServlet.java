package by.training.volkava.homework17.servlet;

import by.training.volkava.homework17.service.OrderService;
import by.training.volkava.homework17.util.ItemsSingleton;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "items", urlPatterns = {"/items"})
public class ItemsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String username = (String) request.getSession(false).getAttribute("username");
        String[] itemIds = request.getParameterValues("items");
        OrderService orderService = new OrderService();
        orderService.addItem(username, itemIds);
        request.setAttribute("order", orderService.getOrderByUsername(username));
        request.setAttribute("items", ItemsSingleton.getInstance());

        getServletContext().getRequestDispatcher("/WEB-INF/jsp/items.jsp")
                .forward(request, response);
    }
}
