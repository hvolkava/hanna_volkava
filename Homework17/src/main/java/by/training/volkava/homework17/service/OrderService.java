package by.training.volkava.homework17.service;

import by.training.volkava.homework17.model.Item;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * Class helps work with orders.
 *
 * @author Hanna Volkava
 */
public class OrderService {
    private static Map<String, Map<Item, Integer>> ordersMap;

    /**
     * Get get all items fo online-shop.
     *
     * @return set items
     */
    private static Map<String, Map<Item, Integer>> getInstance() {
        if (Objects.isNull(ordersMap)) {
            ordersMap = new HashMap<>();
        }
        return ordersMap;
    }

    /**
     * Gets map order by username.
     *
     * @param username username
     * @return map order
     */
    public Map<Item, Integer> getOrderByUsername(String username) {
        if (getInstance().containsKey(username)) {
            return ordersMap.get(username);
        } else {
            return new HashMap<>();
        }
    }

    /**
     * Add item in cart.
     *
     * @param username username
     * @param itemId   id item
     */
    public void addItem(String username, String... itemId) {
        ItemsService itemsService = new ItemsService();
        Map<Item, Integer> itemsMap = itemsService.convertStringIdsToObjects(itemId);
        Map<String, Map<Item, Integer>> ordersMap = getInstance();
        if (getInstance().containsKey(username)) {
            Map<Item, Integer> mapItems = ordersMap.get(username);
            itemsMap.forEach((key, value) -> mapItems.merge(key, value, Integer::sum));
        } else {
            ordersMap.put(username, itemsMap);
        }
    }

    public double getTotalSum(String username) {
        return getOrderByUsername(username).entrySet().stream()
                .mapToDouble(entry -> entry.getKey().getPrice() * entry.getValue()).sum();
    }
}
