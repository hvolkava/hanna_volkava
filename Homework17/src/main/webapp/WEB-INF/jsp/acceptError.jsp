<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eccept error</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div>
    <h2>Oops!</h2>
    <p>You shouldn't be here</p>
    <p>Please, agree with the terms of service first.</p>
    <p><a href="${pageContext.request.contextPath}/login">Start page</a></p>
</div>
</body>
</html>
