<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Checkout</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div>
    <h2>Dear ${sessionScope.get("username")}, your order:</h2>
    <table>
        <tbody>
        <c:forEach var="entry" items="${order}" varStatus="itemCount">
            <tr>
                <td>${itemCount.count})</td>
                <td><c:out value="${entry.key.name}"/></td>
                <td><c:out value="${entry.key.price}"/> $</td>
                <td><c:out value="${entry.value}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <p> Total: $ <fmt:formatNumber type="number" pattern="#,##0.00" value="${totalSum}" /></p>
</div>
</body>
</html>
