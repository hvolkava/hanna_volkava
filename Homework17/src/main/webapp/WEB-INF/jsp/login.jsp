<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div>
    <h2 style="text-align: center;">Welcome to Online Shop</h2>
    <div>
        <form action="login" method="POST">
            <input type="text" id="username" name="username" placeholder="Enter your name">
            <input type="checkbox" name="isAccept" id="isAccept">
            <label for="isAccept">I agree with the terms of service</label>
            <input type="submit" value='Enter'>
        </form>
    </div>
</div>
</body>
</html>
