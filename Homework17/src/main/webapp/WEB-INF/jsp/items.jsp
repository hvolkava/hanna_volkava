<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Choose items</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>

<div>
    <h2>Hello ${sessionScope.username}!</h2>
    <c:if test="${order.isEmpty() == false}">
        <p>You have already chosen:</p>
        <table>
            <tbody>
            <c:forEach var="entry" items="${order}" varStatus="itemCount">
                <tr>
                    <td>${itemCount.count})</td>
                    <td><c:out value="${entry.key.name}"/></td>
                    <td id="price"><c:out value="${entry.key.price}"/> $</td>
                    <td><c:out value="${entry.value}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${order.isEmpty() == true}">
        <p>Make your order</p>
    </c:if>

    <form id="form" action="items" method='POST'>
        <select name="items" size="1">
            <c:forEach var="item" items="${items}">
                <option value="${item.id}">
                    <c:out value="${item.name}"/> <c:out value="${item.price}"/> $
                </option>
            </c:forEach>
        </select><br>
        <input type="submit" value="Add item"/>
        <input type="submit" value="Submit" formaction="checkout" formmethod="POST">
    </form>
</div>

</body>
</html>
